var skills = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  remote: {
    url: '<?php echo base_url(); ?>/assets/JSON/category' + catVal + '.json',
    filter: function(list) {
      return $.map(list, function(cityname) {
        return { name: cityname }; });
    }
  }
});
skills.initialize();
$('#skills').tagsinput({
    maxTags: 8,
    typeaheadjs: {
        name: 'skills',
        limit: 6,
        displayKey: 'name',
        valueKey: 'name',
        source: skills.ttAdapter()
  }
});