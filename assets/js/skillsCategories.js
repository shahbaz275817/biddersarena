
var country_arr = new Array("Website, IT, Software", "Mobile Phones", "Writing and Content", "Design, Media and Architecture", "Data Entry and Admin", "Engineering and Science", "Sales and Marketing", "Business, Accounting, Human Resources & Legal", "Translation");

// States
var s_a = new Array();
s_a[0]=".NET| Adobe| AJAX| Amazon Web Services| Angular.js| Apache| API| App Developer| Artificial Intelligence| ASP.NET| Azure| Big Data| Blockchain| Blog Install| C Programming| C# Programming| C++ Programming| CakePHP| Cloud Computing| CMS| Cocoa| Codeigniter| Coding| Computer Graphics| Computer Security| CSS3| Database| Digital Marketing| Django| DNS| eCommerce| Email Developer| Face Recognition| Facebook API| Game Design| Game Development| Git| Google| Graphics Programming| Hadoop| HTML| Instagram| Ionic Framework| iOS Development| Java| Javascript| Joomla| jQuery| JSON| Laravel| Link Building| Linkedin| Linux| Mac OS| Magento| Microsoft| Mobile App Testing| MySQL| Network Administration| node.js| Objective C| Open Cart| Payment Gateway Integration| PayPal API| PHP| Plugin| Python| Raspberry Pi| Ruby on Rails| Salesforce App Development| Samsung Accessory SDK| SAP| Script Install| Scripting| SEO| Shopify| Shopping Carts| Social Media Management| Social Networking| Software Architecture| Software Development| Software Testing| SQL| Stripe| Swift| System Admin| Twitter| Typing| Ubuntu| Unity 3D| UNIX| User Interface / IA| VB.NET| Visual Basic| Web Development| Web Hosting| Web Scraping| Web Security| Web Services| Website Management| Website Testing| Windows 8| Windows API| Windows Desktop| Windows Server| WordPress| YouTube";
s_a[1]="Android| Java| J2ME| Kotlin| Geolocation| iPad| iPhone| Mobile App Development| blackberry| Virtualization| Windows Phone";
s_a[2]="Academic Writing| Article Rewriting| Article Writing| Blog| Blog Writing| Book Writing| Business Writing| Cartography & Maps| Communications| Content Writing| Copywriting| Creative Writing| eBooks| Editing| Essay Writing| Fiction| Financial Research| Forum Posting| Newsletters| PDF| Poetry| Powerpoint| Press Releases| Product Descriptions| Proofreading| Proposal/Bid Writing| Publishing| Report Writing| Research| Research Writing| Resumes| Reviews| Screenwriting| SEO Writing| Short Stories| Slogans| Speech Writing| Technical Writing| Translation| Wikipedia";
s_a[3]="2D Animation| 3D Animation| 3D Rendering| Adobe Dreamweaver| Adobe Flash| Adobe InDesign| Adobe LiveCycle Designer| After Effects| Animation| App Designer| Audio Production| Audio Services| Autodesk| bootstrap| Banner Design| Book Artist| Brochure Design| Building Architecture| Business Cards| Corel draw| Concept Art| Corporate Identity| Covers & Packaging| Creative Design| CSS| Design| Fashion Design| Fashion Modeling| Filmmaking| Flash| Furniture Design| Graphic Design| Illustration| Illustrator| Industrial Design| Interior Design| Logo Design| Motion Graphics| Photo Editing| Photography| Photoshop| Shopify Templates| Sound Design| Stationery Design| Video Editing and production|  Website Design";
s_a[4]="Article Submission| Customer Support| Data Analytics| Data Entry| Data Processing| Excel| E-mail handling| Microsoft Office| Technical support| Virtual Assistant| Web Search";
s_a[5]="Algorithm| Arduino| AutoCAD| CAD/CAM| CATIA| Circuit Design| Civil Engineering| Cryptography| Data Mining| Data Science| Electrical Engineering| Electronics| Engineering| GPS| Machine Learning| Matlab and Mathematica| Mechanical Engineering| Nanotechnology| Physics| Product Management| Remote Sensing| Robotics| Scientific Research| Solidworks| Statistical Analysis| Structural Engineering| Telecommunications Engineering";
s_a[6]="Advertising| Affiliate Marketing| Branding| Bulk Marketing| Classifieds Posting| Content Marketing| CRM| Email Marketing| Facebook Marketing| Google Adwords| Internet Marketing| Journalist| Keyword Research| Leads| Marketing| Sales| Search Engine Marketing| Social Media Marketing| Telemarketing";
s_a[7]="Accounting| Attorney| Business Analysis| Business Plans| ERP| Finance| Human Resources| Inventory Management| Legal| Management| Payroll| Project Management| Salesforce.com| SAS| Startups| Tax";
s_a[8]="Arabic| Dutch | English (UK) | English (US) | French | German | Greek | Hindi | Hungariy | Indonesian | Italian | Japanese | Malayalam | Punjabi | Russian | Spanish | Tamil | Telugu | Urdu | Kannad | Marathi | Asaami | Farsi | Bangali | Gujrati";


function populateStates( countryElementId, stateElementId ){
	
	var selectedCountryIndex = document.getElementById( countryElementId ).selectedIndex;

	var stateElement = document.getElementById( stateElementId );
	
	stateElement.length=0;	// Fixed by Julian Woods
	stateElement.options[0] = new Option('Select skill','-1');
	
	var state_arr = s_a[selectedCountryIndex].split("|");
    stateElement.selectedIndex = 0;
	
	for (var i=0; i<state_arr.length; i++) {
		stateElement.options[stateElement.length] = new Option(state_arr[i],state_arr[i]);
	}
}

function populateCountries(countryElementId, stateElementId){
    
	var countryElement = document.getElementById(countryElementId);
	countryElement.length=0;
	for (var i=0; i<country_arr.length; i++) {
		countryElement.options[countryElement.length] = new Option(country_arr[i],i+1);
	}
    countryElement.selectedIndex = 0;
    
    populateStates(countryElementId, stateElementId);
}