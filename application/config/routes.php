<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['myprojects'] = 'project/myProjects';
$route['myprojects/(:any)'] = 'project/viewMyProjects/$1';
$route['projects/(:any)'] = 'project/viewProject/$1';
$route['post-project'] = 'project/postProject';

$route['errors/error_503'] = 'CustomError/error_503';
$route['user/skills'] = "CustomError/error_503";
$route['success'] = 'CustomError/error_503';
$route['404_override'] = 'CustomError';
$route['errors/error'] = 'CustomError';

$route['u/(:any)'] = "profile/viewProfile/$1";
$route['profile'] = "profile";
$route['profile-setting'] = "profile/profileSetting";
$route['user/profile/imageUpload']="profile/imageUpload";
$route['user/profile/uploadcertificates']='profile/uploadCertificates';

$route['forgotpassword'] = 'user/passwordreset';
$route['user/passresetreq'] = "user/passresetreq";
$route['user/resetpassword/resetlink/(:any)'] = "user/verifyresethash/$1";
$route['user/changepassword'] = 'user/forgetPassword';

$route['signup'] = 'user/signup';
$route['register'] = 'user/register';
$route['login'] = 'user/login';
$route['login/(:any)'] = 'user/login/$1';
$route['logout'] = 'user/logout';

$route['verify/(:any)'] = "home/verify/$1";
$route['dashboard'] = "dashboard";
$route['terms'] = "home/termsConditions";
$route['/(:any)'] = "/$1";
$route['default_controller'] = 'home';
$route['home'] = 'home';

$route['translate_uri_dashes'] = FALSE;

