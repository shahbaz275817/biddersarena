<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('homemodel');
		$this->load->library('session');
	}

	public function index()
	{
		if ($this->session->has_userdata('user_id') && $this->session->has_userdata('username') && $this->session->has_userdata('logged_in'))
		{
			$user_id = $this->session->userdata('user_id');

			$result = $this->homemodel->userSubscription($user_id);

			$data['bids_remaining'] = $result[0]->bids_remaining;
			$data['total_bids'] = $result[0]->total_bids;

			$this->load->view('user/dashboard', $data);
		} else {
			$this->load->view('errors/error_503');
		}
	}

	public function showProjects()
	{
		if ($this->session->has_userdata('user_id') && $this->session->has_userdata('username') && $this->session->has_userdata('logged_in')) {

			$username = $this->session->userdata('username');
			$user_id = $this->session->userdata('user_id');

			$result = $this->homemodel->loadProjects($username, $user_id);

			if ($result == NULL) {
				echo 'No skills';
			} else {
				foreach($result as $a) {

					echo '<div class="col-12 mb-3">
						<div class="project row">
							<div class="col-12 col-md-6 text-center text-md-left">
								<p class="projectTitle">'.$a->title.'</p>
								
								<p class="projectDesc">'.$a->description.'</p>
							</div>
							
							<div class="col-12 col-md-6 mt-3 mt-md-0">
								<div class="row">
									<div class="col-12 col-md-8 text-center">
										<div class="row">
											<div class="col-6 col-md-12">
												<p class="projectSubTitle">Budget</p>
												<p style="color: #212121;">&#8377; '.$a->budgetRange.'</p>
											</div>
											<div class="col-6 col-md-12">
												<p class="projectSubTitle">Skills</p>
												<p style="font-size: 0.85rem; word-wrap: break-word; color: #212121;">'.$a->skills_required.'</p>
											</div>
										</div>
									</div>
									<div class="col-12 col-md-4 bid mt-3 mt-md-0 text-center text-md-left">
										<a class="btn btn-info" href="'.base_url().'projects/'.$a->project_id.'">Bid</a>
									</div>
								</div>
							</div>
						</div>
					</div>';
				}
			}

		} else {
			$this->load->view('errors/error_503');
		}
	}

}
