<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('profilemodel');
		$this->load->library('session');
		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function index() {
		if($this->session->has_userdata('logged_in') && $this->session->has_userdata('user_id') && $this->session->has_userdata('username')) {

			$username = $this->session->userdata('username');
			$result = $this->profilemodel->getUserData($username);
			$balance = $this->profilemodel->getWalletBalance($username);
			$avg = $this->profilemodel->findRatingAvg($username);

			$data['name'] = $result[0]->account_name;
			$data['username'] = $result[0]->username;
			$data['email'] = $result[0]->email;
			$data['skypeId'] = $result[0]->skype_id;
			$data['gender'] = $result[0]->gender;
			$data['phone'] = $result[0]->phone;
            $data['portfolio_pic1'] = $result[0]->portfolio_pic1;
            $data['portfolio_link1'] = $result[0]->portfolio_link1;
            $data['portfolio_desc1'] = $result[0]->portfolio_description1;
            $data['portfolio_pic2'] = $result[0]->portfolio_pic2;
            $data['portfolio_link2'] = $result[0]->portfolio_link2;
            $data['portfolio_desc2'] = $result[0]->portfolio_description2;
            $data['portfolio_pic3'] = $result[0]->portfolio_pic3;
            $data['portfolio_link3'] = $result[0]->portfolio_link3;
            $data['portfolio_desc3'] = $result[0]->portfolio_description3;
            $data['portfolio_pic4'] = $result[0]->portfolio_pic4;
            $data['portfolio_link4'] = $result[0]->portfolio_link4;
            $data['portfolio_desc4'] = $result[0]->portfolio_description4;

			$data['address1'] = $result[0]->address_line1;
			$data['address2'] = $result[0]->address_line2;
			$data['city'] = $result[0]->city;
			$data['pincode'] = $result[0]->pincode;
			$data['state'] = $result[0]->state;
			$data['country'] = $result[0]->country;
			$data['qualification'] = $result[0]->qualification;
			$data['professional_title'] = $result[0]->professional_title;
			$data['skills'] = $result[0]->skills;
			$data['bio'] = $result[0]->bio;

            $data['certification_1'] = $result[0]->certification_1;
            $data['certification_1_pdf'] = $result[0]->certification_1_pdf;
            $data['certification_2'] = $result[0]->certification_2;
            $data['certification_2_pdf'] = $result[0]->certification_2_pdf;
            $data['certification_3'] = $result[0]->certification_3;
            $data['certification_3_pdf'] = $result[0]->certification_3_pdf;

			$data['bank_account_name'] = $result[0]->bank_account_name;
			$data['account_number'] = $result[0]->account_no;
			$data['bank_name'] = $result[0]->bank_name;
			$data['bank_branch'] = $result[0]->branch;
			$data['ifsc'] = $result[0]->ifsc_code;
			$data['bank_address'] = $result[0]->branch_address;
			$data['payment_method'] = $result[0]->payment_method;

			$data['balance'] = $balance[0]->balance;

			$data['photo'] = $result[0]->photo;
			$this->session->unset_userdata('photo');
			$this->session->set_userdata('photo', $data['photo']);

			$count = 1;
			foreach ($avg as $a) {
				if($count == 2){
					$data['count'] = $a;
				}
				$count++;
			}

			$this->load->view('profile/profile', $data);
		} else {
			$this->load->view('errors/error_503');
		}
	}

	public function viewProfile($username) {

		if($this->session->has_userdata('logged_in') && $this->session->has_userdata('user_id') && $this->session->has_userdata('username')) {

			// taking precautions // cross site scripting and sql injection prevention //

			$user_name = $this->security->xss_clean(str_replace( array( '\'', '"', ',' , ';', '<', '>', '(', ')' ,'*', '='), '', $username));

			$result = $this->profilemodel->findUserInfo($user_name);
			$avg = $this->profilemodel->findRatingAvg($user_name);

			if($result == 0) {
				$this->load->view('profile/profileError');
			} else {
				$data['name'] = $result[0]->account_name;
				$data['username'] = $result[0]->username;
				$data['gender'] = $result[0]->gender;
				$data['photo'] = $result[0]->photo;
				$data['city'] = $result[0]->city;
				$data['country'] = $result[0]->country;
				$data['skills'] = $result[0]->skills;
				$data['professional_title'] = $result[0]->professional_title;
				$data['qualification'] = $result[0]->qualification;
				$data['bio'] = $result[0]->bio;

				$data['certification_1'] = $result[0]->certification_1;
				$data['certification_1_pdf'] = $result[0]->certification_1_pdf;
				$data['certification_2'] = $result[0]->certification_2;
				$data['certification_2_pdf'] = $result[0]->certification_2_pdf;
				$data['certification_3'] = $result[0]->certification_3;
				$data['certification_3_pdf'] = $result[0]->certification_3_pdf;

                $data['portfolio_pic1'] = $result[0]->portfolio_pic1;
                $data['portfolio_link1'] = $result[0]->portfolio_link1;
                $data['portfolio_desc1'] = $result[0]->portfolio_description1;
                $data['portfolio_pic2'] = $result[0]->portfolio_pic2;
                $data['portfolio_link2'] = $result[0]->portfolio_link2;
                $data['portfolio_desc2'] = $result[0]->portfolio_description2;
                $data['portfolio_pic3'] = $result[0]->portfolio_pic3;
                $data['portfolio_link3'] = $result[0]->portfolio_link3;
                $data['portfolio_desc3'] = $result[0]->portfolio_description3;
                $data['portfolio_pic4'] = $result[0]->portfolio_pic4;
                $data['portfolio_link4'] = $result[0]->portfolio_link4;
                $data['portfolio_desc4'] = $result[0]->portfolio_description4;

				$data['avg'] = $avg;

				$this->load->view('profile/userProfile', $data);
			}

		} else {
			$this->load->view('errors/error_503');
		}

	}

	public function loadReviews() {
		$offset = $this->security->xss_clean(str_replace( array( '\'', '"', ',' , ';', '<', '>', '(', ')' ,'*', '='), '',$this->input->post('offset')));
		$username = $this->security->xss_clean(str_replace( array( '\'', '"', ',' , ';', '<', '>', '(', ')' ,'*', '='), '',$this->input->post('username')));

		$data = (int)$offset;

		if($this->session->has_userdata('logged_in') && $this->session->has_userdata('user_id') && $this->session->has_userdata('username')) {

			$result = $this->profilemodel->loadReviews($username, $data);

			foreach ($result as $review){

				echo '<div class="col col-12 col-sm-12 col-md-6 reviewCol">
                                
						<div class="reviewRating" style="">
							<div class="reviewRatingDivOne">
								<span class="ratingNumber">'.$review->rating.'</span>
							</div>

							<div class="reviewRatingDivTwo">

								<div class="reviewRatingDivTwoOne">
									<fieldset class="rating">

									<input type="radio" id="star1" disabled '; if( $review->rating == 1 ) echo 'checked'; echo '>
									<label class = "full" for="star1" title="Bad!"></label>

									<input type="radio" id="star2" disabled '; if( $review->rating == 2 ) echo 'checked'; echo '>
									<label class = "full" for="star2" title="Average"></label>

									<input type="radio" id="star3" disabled '; if( $review->rating == 3 ) echo 'checked'; echo '>
									<label class = "full" for="star3" title="Nice"></label>

									<input type="radio" id="star4" disabled '; if( $review->rating == 4 ) echo 'checked'; echo '>
									<label class = "full" for="star4" title="Pretty good"></label>

									<input type="radio" id="star5" disabled '; if( $review->rating == 5 ) echo 'checked'; echo '>
									<label class = "full" for="star5" title="Excellent!"></label>

									</fieldset>
								</div>

								<div class="reviewRatingDivTwoTwo">by  '.$review->review_by.'</div>

							</div>
						</div>

						<p class="reviewData">
							'.$review->review.'
						</p>

					</div>';
				}

		} else {
			$this->load->view('errors/error_503');
		}
	}

	public function showMyProjects() {
		if($this->session->has_userdata('logged_in') && $this->session->has_userdata('user_id') && $this->session->has_userdata('username')) {

			$user_id = $this->session->userdata('user_id');

			$result = $this->profilemodel->loadMyProjects($user_id);

			foreach ($result as $project) {

				echo '<div class="col-12 mb-3">
							<a style="text-decoration: none !important;" href="'.base_url().'myprojects/'.$project->project_id.'">
							<div class="project row">
								<div class="col-12 col-md-8 text-center text-md-left">
									<p class="projectTitle">'.$project->title.'</p>
	
									<p class="projectDesc">'.$project->description.'</p>
								</div>
	
								<div class="col-12 col-md-4 mt-2 mt-md-0 text-center">
									<div class="row">
										<div class="col-6 col-md-12 mb-0 mb-md-2">
											<p class="projectSubTitle">Budget</p>
											<p style="color: #212121;">₹ '.$project->budgetRange.'</p>
										</div>
										<div class="col-6 col-md-12">
											<p class="projectSubTitle">Skills</p>
											<p style="font-size: 0.85rem; word-wrap: break-word; color: #212121;">'.$project->skills_required.'</p>
										</div>
									</div>
								</div>
							</div>
							</a>
					   </div>';
			}
		} else {
			$this->load->view('errors/error_503');
		}
	}

	// -------------- Update User Profile Module ------------------ //

	public function updateBioProfile() {
		$this->form_validation->set_rules('bio', 'Bio', 'trim|required|xss_clean|min_length[100]');

		if($this->form_validation->run() === false) {

			$this->session->set_flashdata('validation_error', validation_errors());
			redirect('profile');

		} else {

			$username = $this->session->userdata('username');

			$bio = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '*', '='), '', $this->input->post('bio')));

			$result = $this->profilemodel->updateBio($username, $bio);
			if($result) {
				$error = "Success";
			} else {
				$error = "Failed";
			}
			$this->session->set_flashdata('msg', $error);
			redirect('profile');
		}
	}

	public function updateOverviewProfile (){
		$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean', array('required', '%s field is mandatory'));
		$this->form_validation->set_rules('add1', 'Address', 'trim|required|xss_clean', array('required', '%s field is mandatory'));
		$this->form_validation->set_rules('add2', 'Locality', 'trim|required|xss_clean', array('required', '%s field is mandatory'));
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required|xss_clean|min_length[9]', array('min_length', 'Enter a valid number'));
		$this->form_validation->set_rules('gender', 'Gender', 'trim|required|alpha|xss_clean', array('required', '%s field is mandatory'));

		if($this->form_validation->run() === false) {

			$this->session->set_flashdata('validation_error', validation_errors());
			redirect('profile');

		} else {
			$username = $this->session->userdata('username');

			$name = $this->security->xss_clean(str_replace( array( '\'', '"', ',' , ';', '<', '>', '(', ')' ,'*', '='), '', $this->input->post('name')));
			$address1 = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>' ,'*', '='), '', $this->input->post('add1')));
			$address2 = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>', '*', '='), '', $this->input->post('add2')));
			$phone = $this->security->xss_clean(str_replace( array( '\'', '"', ',' , ';', '<', '>', '(', ')' ,'*', '='), '', $this->input->post('phone')));
			$gender = $this->security->xss_clean(str_replace( array( '\'', '"', ',' , ';', '<', '>', '(', ')' ,'*', '='), '', $this->input->post('gender')));

			$result = $this->profilemodel->updateOverview($username, $name, $address1, $address2, $phone, $gender);
			if($result) {
				$error = "Success";
			} else {
				$error = "Failed";
			}
			$this->session->set_flashdata('msg', $error);
			redirect('profile');
		}
	}

	public function updateDescriptionProfile () {
		$this->form_validation->set_rules('city', 'City', 'trim|xss_clean');
		$this->form_validation->set_rules('state', 'State', 'trim|xss_clean');
		$this->form_validation->set_rules('country', 'Country', 'trim|xss_clean');
		$this->form_validation->set_rules('pincode', 'Pincode', 'trim|numeric|xss_clean', array('numeric', '%s field can only contain numbers'));
		$this->form_validation->set_rules('skypeId', 'Skype Id', 'trim|xss_clean|valid_email');
		$this->form_validation->set_rules('professionalTitle', 'Professional Title', 'trim|xss_clean');
		$this->form_validation->set_rules('qualifications', 'Qualifications', 'trim|xss_clean');
		$this->form_validation->set_rules('skills', 'Skills', 'trim|xss_clean|min_length[3]', array('min_length' => 'Please select at least 1 skill.'));

		if($this->form_validation->run() === false) {

			$this->session->set_flashdata('validation_error', validation_errors());
			redirect('profile');

		} else {
			$username = $this->session->userdata('username');

			$city = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>','*', '='),'', $this->input->post('city')));
			$state = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>','*', '='), '', $this->input->post('state')));
			$country = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>','*', '='), '', $this->input->post('country')));
			$pincode = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>','*', '='), '', $this->input->post('pincode')));
			$skypeId = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>','*', '='), '', $this->input->post('skypeId')));
			$professional_title = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>','*', '='), '', $this->input->post('professionalTitle')));
			$qualification = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>', '*', '='), '', $this->input->post('qualifications')));
			$skills = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>', '*', '='), '', $this->input->post('skills')));

			$result = $this->profilemodel->updateDescription($username, $city, $state, $country, $pincode, $skypeId, $professional_title, $qualification, $skills/*,$certification_[1],$file_path[1],$certification_[2],$file_path[2],$certification_[3],$file_path[3]*/);
			if ($result) {
				$error = "Success";
			} else {
				$error = "Failed";
			}
			$this->session->set_flashdata('msg', $error);
			redirect('profile');
		}
    }

    public function updateCertificate1(){
        $config['upload_path']          = './certificates/';
        $config['allowed_types']        = 'pdf|jpeg|jpg';
        $config['max_size']             = 100000;
        $config['overwrite']            = 'FALSE';
        $this->load->library('upload', $config);

        $username = $this->session->userdata('username');

        if ($this->upload->do_upload('certificationFile1')) {
            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $pdf_1 = $upload_data['file_name'];
            $file_path1 = "./certificates/" . $pdf_1;

            $certification_1 = $this->security->xss_clean(str_replace( array( '\'', '"', ',', ';', '<', '>', '*', '='), '', $this->input->post('certification1')));
            $file_path1 = $this->security->xss_clean(str_replace( array( '\'', '"', ',', ';', '<', '>', '*', '='), '', $file_path1));

            $result = $this->profilemodel->updateCertificate1($certification_1, $file_path1, $username);
            if ($result) {
                $error = "Success";
            } else {
                $error = "Failed";
            }
            //$this->session->set_flashdata('msg', $error);
            //redirect('profile');
			echo $error;
        }

    }
    public function updateCertificate2(){
        $config['upload_path']          = './certificates/';
        $config['allowed_types']        = 'pdf|jpeg|jpg';
        $config['max_size']             = 100000;
        $config['overwrite']            = 'FALSE';
        $this->load->library('upload', $config);

        $username = $this->session->userdata('username');

        if ($this->upload->do_upload('certificationFile2')) {
            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $pdf_2 = $upload_data['file_name'];
            $file_path2 = "./certificates/" . $pdf_2;

            $certification_2 = $this->security->xss_clean(str_replace( array( '\'', '"', ',', ';', '<', '>', '*', '='), '', $this->input->post('certification2')));
            $file_path2 = $this->security->xss_clean(str_replace( array( '\'', '"', ',', ';', '<', '>', '*', '='), '', $file_path2));

            $result = $this->profilemodel->updateCertificate2($certification_2, $file_path2, $username);
            if ($result) {
                $error = "Success";
            } else {
                $error = "Failed";
            }
            //$this->session->set_flashdata('msg', $error);
            //redirect('profile');
			echo $error;
        }

    }
    public function updateCertificate3(){
        $config['upload_path']          = './certificates/';
        $config['allowed_types']        = 'pdf|jpeg|jpg';
        $config['max_size']             = 100000;
        $config['overwrite']            = 'FALSE';
        $this->load->library('upload', $config);

        $username = $this->session->userdata('username');

        if ($this->upload->do_upload('certificationFile3')) {
            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $pdf_3 = $upload_data['file_name'];
            $file_path3 = "./certificates/" . $pdf_3;

            $certification_3 = $this->security->xss_clean(str_replace( array( '\'', '"', ',', ';', '<', '>', '*', '='), '', $this->input->post('certification3'))) ;
            $file_path3 = $this->security->xss_clean(str_replace( array( '\'', '"', ',', ';', '<', '>', '*', '='), '', $file_path3));

            $result = $this->profilemodel->updateCertificate3($certification_3, $file_path3, $username);
            if ($result) {
                $error = "Success";
            } else {
                $error = "Failed";
            }
            //$this->session->set_flashdata('msg', $error);
            //redirect('profile');
			echo $error;
        }

    }


	public function updateKYCProfile() {
		$this->form_validation->set_rules('accountName', 'Account Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('accountNumber', 'Account Number', 'trim|numeric|required|xss_clean');
		$this->form_validation->set_rules('bankName', 'Bank Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('bankBranch', 'Branch', 'trim|required|xss_clean');
		$this->form_validation->set_rules('ifsc', 'IFSC', 'trim|required|alpha_numeric|xss_clean|exact_length[13]');
		$this->form_validation->set_rules('bankAdd', 'Bank Address', 'trim|required|xss_clean');
		$this->form_validation->set_rules('payment_method', 'Payment Method', 'trim|alpha|xss_clean');

		if($this->form_validation->run() === false) {

			$this->session->set_flashdata('validation_error', validation_errors());
			redirect('profile');

		} else {
			$username = $this->session->userdata('username');

			$bank_account_name = $this->security->xss_clean($this->security->xss_clean(str_replace( array( '\'', '"', ',', ';', '<', '>', '*', '='), '', $this->input->post('accountName'))));
			$account_number = $this->security->xss_clean($this->security->xss_clean(str_replace( array( '\'', '"', ',', ';', '<', '>', '*', '='), '', $this->input->post('accountNumber'))));
			$bank_name = $this->security->xss_clean($this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>', '*', '='), '', $this->input->post('bankName'))));
			$bank_branch = $this->security->xss_clean($this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>', '*', '='), '', $this->input->post('bankBranch'))));
			$ifsc = $this->security->xss_clean($this->security->xss_clean(str_replace( array( '\'', '"', ',', ';', '<', '>', '*', '='), '', $this->input->post('ifsc'))));
			$bank_address = $this->security->xss_clean($this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>', '*', '='), '', $this->input->post('bankAdd'))));
			$payment_method = $this->security->xss_clean($this->security->xss_clean(str_replace( array( '\'', '"', ',', ';', '<', '>', '*', '='), '', $this->input->post('payment_method'))));

			$result = $this->profilemodel->updateKYC($username, $bank_account_name, $account_number, $bank_name, $bank_branch, $ifsc, $bank_address, $payment_method);

			if($result) {
				$error = "Success";
			} else {
				$error = "Failed";
			}
			$this->session->set_flashdata('msg', $error);
			redirect('profile');
		}
	}

	// -------------- End - Update User Profile Module ------------------ //


    // --------------- User Profile picture module -------------//

    public function imageUpload(){

		$id = $this->session->userdata('user_id');

        //Get the content of the image and then add slashes to it
        $imagetmp = addslashes (file_get_contents($_FILES['myimage']['tmp_name']));

		$insert_image = "UPDATE user SET photo = '$imagetmp' WHERE user_id = $id";

        //$result = $this->profilemodel->imageUploadDB($imagetmp, $id);

        if($this->db->query($insert_image)) {
			$this->session->set_flashdata('msg', "SUCCESS");
			redirect('profile');
		}
        else {
			$this->session->set_flashdata('msg', "Failed");
			redirect('profile');
		}
    }

    // --------------- End - User Profile Picture module -------------//


	// -------------- Profile Ajax Module ------------------ //

	public function check_bio_length() {
		$this->form_validation->set_rules('bio', 'Bio', 'trim|xss_clean|min_length[100]');

		if($this->form_validation->run() === false)
		{
			echo validation_errors();
		}
		else
		{
			echo 'Bio content acceptable';
		}
	}

	function valid_url($url) {
		if(!filter_var($url, FILTER_VALIDATE_URL) === false) {
			return false;
		} else {
			return true;
		}
	}


	public function portfolio1(){
		$this->form_validation->set_rules('portfolioLink1', 'URL', 'trim|required|xss_clean|callback_valid_url');

		if($this->form_validation->run() === false)
		{
			echo validation_errors();
		}
		else
		{
			$config['upload_path']          = './portfolio_images/';
			$config['allowed_types']        = 'jpeg|jpg';
			$config['max_size']             = 2048;
			$config['overwrite']            = 'FALSE';
			$this->load->library('upload', $config);
			$username = $this->session->userdata('username');
			if ($this->upload->do_upload('portfolioPic1')) {
				$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
				$pic1 = $upload_data['file_name'];
				$pic_path1 = "./portfolio_images/" . $pic1;

				$portfolioLink1 = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>', '*', '='), '', $this->input->post('portfolioLink1')));
				$portfolioDesc1 = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>', '*', '='), '', $this->input->post('portfolioDesc1')));

				$result = $this->profilemodel->updatePortfolio1($pic_path1, $portfolioLink1, $portfolioDesc1,$username);
				if ($result) {
					$error = "Success";
				} else {
					$error = "Failed";
				}
				//$this->session->set_flashdata('msg', $error);
				//redirect('profile');
				echo $error;
			}
		}
    }

    public function portfolio2(){
		$this->form_validation->set_rules('portfolioLink2', 'URL', 'trim|required|xss_clean|callback_valid_url');

		if($this->form_validation->run() === false)
		{
			echo validation_errors();
		}
		else {
			$config['upload_path'] = './portfolio_images/';
			$config['allowed_types'] = 'jpeg|jpg';
			$config['max_size'] = 2048;
			$config['overwrite'] = 'FALSE';
			$this->load->library('upload', $config);
			$username = $this->session->userdata('username');
			if ($this->upload->do_upload('portfolioPic2')) {
				$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
				$pic2 = $upload_data['file_name'];
				$pic_path2 = "./portfolio_images/" . $pic2;

				$portfolioLink2 = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>', '*', '='), '',$this->input->post('portfolioLink2')));
				$portfolioDesc2 = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>', '*', '='), '',$this->input->post('portfolioDesc2')));

				$result = $this->profilemodel->updatePortfolio2($pic_path2, $portfolioLink2, $portfolioDesc2, $username);
				if ($result) {
					$error = "Success";
				} else {
					$error = "Failed";
				}
				//$this->session->set_flashdata('msg', $error);
				//redirect('profile');
				echo $error;
			}
		}
    }

    public function portfolio3(){
		$this->form_validation->set_rules('portfolioLink3', 'URL', 'trim|required|xss_clean|callback_valid_url');

		if($this->form_validation->run() === false)
		{
			echo validation_errors();
		}
		else {
			$config['upload_path'] = './portfolio_images/';
			$config['allowed_types'] = 'jpeg|jpg';
			$config['max_size'] = 2048;
			$config['overwrite'] = 'FALSE';
			$this->load->library('upload', $config);
			$username = $this->session->userdata('username');
			if ($this->upload->do_upload('portfolioPic3')) {
				$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
				$pic3 = $upload_data['file_name'];
				$pic_path3 = "./portfolio_images/" . $pic3;

				$portfolioLink3 = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>', '*', '='), '', $this->input->post('portfolioLink3')));
				$portfolioDesc3 = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>', '*', '='), '', $this->input->post('portfolioDesc3')));

				$result = $this->profilemodel->updatePortfolio3($pic_path3, $portfolioLink3, $portfolioDesc3, $username);
				if ($result) {
					$error = "Success";
				} else {
					$error = "Failed";
				}
				//$this->session->set_flashdata('msg', $error);
				//redirect('profile');
				echo $error;
			}
		}
    }

    public function portfolio4(){
		$this->form_validation->set_rules('portfolioLink4', 'URL', 'trim|required|xss_clean|callback_valid_url');

		if($this->form_validation->run() === false)
		{
			echo validation_errors();
		}
		else {
			$config['upload_path'] = './portfolio_images/';
			$config['allowed_types'] = 'jpeg|jpg';
			$config['max_size'] = 2048;
			$config['overwrite'] = 'FALSE';
			$this->load->library('upload', $config);
			$username = $this->session->userdata('username');
			if ($this->upload->do_upload('portfolioPic4')) {
				$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
				$pic4 = $upload_data['file_name'];
				$pic_path4 = "./portfolio_images/" . $pic4;

				$portfolioLink4 = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>', '*', '='), '', $this->input->post('portfolioLink4')));
				$portfolioDesc4 = $this->security->xss_clean(str_replace( array( '\'', '"', ';', '<', '>', '*', '='), '', $this->input->post('portfolioDesc4')));

				$result = $this->profilemodel->updatePortfolio4($pic_path4, $portfolioLink4, $portfolioDesc4, $username);
				if ($result) {
					$error = "Success";
				} else {
					$error = "Failed";
				}
				//$this->session->set_flashdata('msg', $error);
				//redirect('profile');
				echo $error;
			}
		}
    }


}
