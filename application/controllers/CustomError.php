<?php
/**
 * Created by PhpStorm.
 * User: Shahbaz
 * Date: 1/4/2018
 * Time: 1:33 PM
 */

class CustomError extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        $this->output->set_status_header('404');
        $this->load->view('errors/error');
    }

    public function error_503() {
    	$this->load->view('errors/error_503');
	}
}
