<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('projectmodel');
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('form','url','cookie'));
	}

	public function postProject() {

		if ($this->session->has_userdata('logged_in') && $this->session->has_userdata('user_id') && $this->session->has_userdata('username')) {

			$this->load->view('project/post_project');

		} else {

			$this->load->view('errors/error_503');

		}
	}

	// -------------- Post a Project Page ------------- //

	public function post() {

		if ($this->session->has_userdata('logged_in') && $this->session->has_userdata('user_id') && $this->session->has_userdata('username')) {

			$user_id = $this->session->userdata('user_id');

			$this->form_validation->set_rules('projectName', 'ProjectName', 'trim|required|xss_clean|min_length[10]', array('min_length' => 'Please enter at least 10 characters.'));
			$this->form_validation->set_rules('description', 'Description', 'trim|required|xss_clean|min_length[100]', array('min_length' => 'Please enter at least 100 characters.'));
			$this->form_validation->set_rules('sampleLink', 'Sample Link', 'trim|xss_clean|valid_url');

			$this->form_validation->set_rules('category', 'Category', 'trim|required|xss_clean');
			$this->form_validation->set_rules('skillsRequired', 'Skills', 'trim|required|xss_clean|min_length[2]', array('min_length' => 'Please select at least 1 skill.'));
			$this->form_validation->set_rules('budgetRange', 'Budget Range', 'trim|required|xss_clean');
			$this->form_validation->set_rules('budget', 'Budget', 'trim|required|xss_clean');
			$this->form_validation->set_rules('addons', 'Addons', 'trim|xss_clean');
			$this->form_validation->set_rules('deadline', 'Deadline', 'trim|xss_clean');

			if ($this->form_validation->run() == FALSE) {

				$this->load->view('project/post_project');

			} else {
				$project_name = $this->security->xss_clean($this->input->post('projectName'));
				$description = $this->security->xss_clean($this->input->post('description'));
				$sampleLink = $this->security->xss_clean($this->input->post('sampleLink'));
				$category = $this->security->xss_clean($this->input->post('category'));
				$skills_required = $this->security->xss_clean($this->input->post('skillsRequired'));
				$budget_range = $this->security->xss_clean($this->input->post('budgetRange'));
				$budget = $this->security->xss_clean($this->input->post('budget'));
				$additional_packs = $this->security->xss_clean($this->input->post('addons'));
				$deadline = $this->security->xss_clean($this->input->post('deadline'));

                $config['upload_path']          = './project_files/';
                $config['allowed_types']        = 'pdf|jpeg|jpg';
                $config['max_size']             = 100000;
                $config['overwrite']            = 'FALSE';
                $this->load->library('upload', $config);

//                foreach($this->input->post('file') as $f){
//                    $this->upload->do_upload($f);
//                    $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
//                    $file = $upload_data['file_name'];
//                    $file_path = "./project_files/".$file;
//                    echo $file_path;
//                }
                /*while($this->upload->do_upload('file')){
                    $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                    $file = $upload_data['file_name'];
                    $file_path = "./project_files/".$file;
                    echo $file_path;
                }*/


                if(!empty($_FILES['files']['name'])) {
                    $filesCount = count($_FILES['files']['name']);
                    for ($i = 0; $i < $filesCount; $i++) {
                        $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                        $_FILES['file']['size'] = $_FILES['files']['size'][$i];

                        $config['upload_path']          = './project_files/';
                        $config['allowed_types']        = 'pdf|jpeg|jpg';
                        $config['max_size']             = 100000;
                        $config['overwrite']            = 'FALSE';
                        $this->load->library('upload', $config);
                        if ($this->upload->do_upload('file')) {
                            $fileData = $this->upload->data();
                            $uploadData[$i]['file_name'] = $fileData['file_name'];
                            $file[$i] = './project_files/'.$uploadData[$i]['file_name'];

                        }
                    }

                    $rc= 4 - ($filesCount+1);
                    for($j=4;$j>$rc;$j--){
                        $file[$j] = null;
                    }
                }


				$result = $this->projectmodel->postProject($user_id, $project_name, $description, $sampleLink,$file[0],$file[1], $file[2], $file[3],$file[4], $category, $skills_required, $budget_range, $budget, $additional_packs, $deadline);

				if ($result == 1) {
					$this->session->set_flashdata('msg', "Project Posted Successfully");
					redirect('dashboard');
				} else {
					$this->session->set_flashdata('msg', "There was an error, Please try again.");
					redirect('post-project');
				}
			}
		} else {
			$this->load->view('errors/error_503');
		}
	}

	// -------------- End - Post a Project Page ------------- //


	// -------------- Detailed Project Page - Other Projects ------------- //

	public function viewProject($project_id) {
		$clean_id = $this->security->xss_clean(str_replace( array( '\'', '"', ',' , ';', '<', '>', '(', ')' ,'*', '='), '', $project_id));

		if ($this->session->has_userdata('logged_in') && $this->session->has_userdata('user_id') && $this->session->has_userdata('username')) {

			$result = $this->projectmodel->loadProject($clean_id);
			$data['project_id'] = $result->project_id;
			$data['title'] = $result->title;
			$data['description'] = $result->description;
			$data['link'] = $result->sampleLink;
			$data['skills_required'] = $result->skills_required;
			$data['category'] = $result->category;
			$data['budget_range'] = $result->budgetRange;
			$data['budget'] = $result->budget;
			$data['helper'] = $result->recruiter;
			$data['urgent'] = $result->urgent;
			$data['highlighted'] = $result->featured;
			$data['nda'] = $result->nda;
			$data['deadline'] = $result->deadline;
			$data['photo1'] = $result->photo1;
			$data['photo2'] = $result->photo2;
			$data['photo3'] = $result->photo3;
			$data['photo4'] = $result->photo4;
			$data['photo5'] = $result->photo5;

			$this->load->view('project/detailed_project', $data);

		}
		else {
			$this->load->view('errors/error_503');
		}
	}

	// -------------- End - Detailed Project Page - Other Projects ------------- //


	// -------------- All Project Page - MyProjects ------------- //

	public function myProjects() {
		if ($this->session->has_userdata('logged_in') && $this->session->has_userdata('user_id') && $this->session->has_userdata('username')) {

			$this->load->view('project/my_projects');
		} else {
			$this->load->view('errors/error_503');
		}
	}

	// -------------- Ajax - All Project Page - MyProjects ------------- //

	public function main_myProjects() {
		if ($this->session->has_userdata('logged_in') && $this->session->has_userdata('user_id') && $this->session->has_userdata('username')) {

			$user_id = $this->session->userdata('user_id');

			$result = $this->projectmodel->load_main_myProjects($user_id);

			foreach ($result as $a) {
				echo '<div class="col-12 mb-3">
                    <a style="text-decoration: none !important;" href="'.base_url().'projects/'.$a->project_id.'">
                        <div class="project row">
                            <div class="col-12 col-md-8 text-center text-md-left">
                                <p class="projectTitle">'.$a->title.'</p>

                                <p class="projectDesc">'.$a->description.'</p>
                            </div>

                            <div class="col-12 col-md-4 mt-2 mt-md-0 text-center">
                                <div class="row">
                                    <div class="col-6 col-md-12 mb-0 mb-md-2">
                                        <p class="projectSubTitle">Budget</p>
                                        <p style="color: #212121;">₹ '.$a->budgetRange.'</p>
                                    </div>
                                    <div class="col-6 col-md-12">
                                        <p class="projectSubTitle">Skills</p>
                                        <p style="font-size: 0.85rem; word-wrap: break-word; color: #212121;">'.$a->skills_required.'</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>';
			}

		} else {
			$this->load->view('errors/error_503');
		}
	}

	// -------------- End - All Project Page - MyProjects ------------- //


	// -------------- Detailed Project Page - MyProjects ------------- //

	public function viewMyProjects($project_id) {
		$clean_id = $this->security->xss_clean(str_replace( array( '\'', '"', ',' , ';', '<', '>', '(', ')' ,'*', '='), '', $project_id));

		if ($this->session->has_userdata('logged_in') && $this->session->has_userdata('user_id') && $this->session->has_userdata('username'))
		{
			$user_id = $this->session->userdata('user_id');

			$result = $this->projectmodel->load_detailed_MyProjects($clean_id);

			if($user_id == $result->posted_by) {

				$data['project_id'] = $result->project_id;
				$data['title'] = $result->title;
				$data['description'] = $result->description;
				$data['link'] = $result->sampleLink;
				$data['skills_required'] = $result->skills_required;
				$data['category'] = $result->category;
				$data['budget_range'] = $result->budgetRange;
				$data['budget'] = $result->budget;
				$data['helper'] = $result->recruiter;
				$data['urgent'] = $result->urgent;
				$data['highlighted'] = $result->featured;
				$data['nda'] = $result->nda;
				$data['deadline'] = $result->deadline;
				$data['photo1'] = $result->photo1;
				$data['photo2'] = $result->photo2;
				$data['photo3'] = $result->photo3;
				$data['photo4'] = $result->photo4;
				$data['photo5'] = $result->photo5;

				$this->load->view('project/my_detailed_project', $data);

			} else {
				$this->load->view('errors/error');
			}

		} else {
			$this->load->view('errors/error_503');
		}

	}

	// -------------- End - Detailed Project Page - MyProjects ------------- //


	// -------------- Bid Project Page - Other Projects ------------- //

	public function bidOnAProject($project_id)
	{
		$clean_id = $this->security->xss_clean(str_replace( array( '\'', '"', ',' , ';', '<', '>', '(', ')' ,'*', '='), '', $project_id));

		if ($this->session->has_userdata('logged_in') && $this->session->has_userdata('user_id') && $this->session->has_userdata('username')) {

			$user_id = $this->session->userdata('user_id');

			$a = $this->projectmodel->getProjectDetails($clean_id);

			if($user_id == $a[0]->posted_by)
			{
				$this->load->view('errors/error_you_cannot_do_that');
			}
			else
			{
				$b = $this->projectmodel->check_bids_remaining($user_id);

				$bids_remaining = $b[0]->bids_remaining;

				if($bids_remaining > 0) {

					// Subtract 1 bid from user_subscription table

					// Add user_id of this user to project table

				}
				elseif ($bids_remaining == -1)
				{
					// Add user_id of this user to project table
				}
				else
				{
					// User has exhausted his daily quota of bids

					// show error
				}
			}

		} else {
			$this->load->view('errors/error_503');
		}
	}

	// -------------- Bid Project Page - Other Projects ------------- //


	// -------------- Post Project Ajax Module ------------------ //

	public function projectName_validation() {
		$this->form_validation->set_rules('projectName', 'ProjectName', 'trim|required|min_length[10]|xss_clean', array('min_length' => 'Please enter at least 10 characters.'));

		if($this->form_validation->run() === false)
		{
			echo validation_errors();
		} else {
			echo "true";
		}
	}

	public function description_validation() {
		$this->form_validation->set_rules('description', 'Description', 'trim|required|min_length[100]|xss_clean', array('min_length' => 'Please enter at least 100 characters.'));

		if($this->form_validation->run() === false)
		{
			echo validation_errors();
		} else {
			echo "true";
		}
	}

	public function skills_validation() {
		$this->form_validation->set_rules('skillsRequired', 'Skills', 'trim|min_length[3]|xss_clean', array('min_length' => 'Please select at least 1 skill.'));

		if($this->form_validation->run() === false)
		{
			echo validation_errors();
		} else {
			echo "true";
		}
	}

	public function budget_validation() {
		$this->form_validation->set_rules('budget', 'Budget', 'trim|xss_clean');

		if($this->form_validation->run() === false)
		{
			echo validation_errors();
		} else {
			echo "true";
		}
	}


	// -------------- End - Post Project Ajax Module ------------------ //

}
