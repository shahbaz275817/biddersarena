<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <meta name="description" content="">
    <title>Bidder's Arena</title>
    
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/web/assets/mobirise-icons/mobirise-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/dropdown/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/mobirise/css/mbr-additional.css" type="text/css">
	<!-- Fonts -->
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Josefin+Slab,Arvo,Open+Sans">
    
    <!--  Custom  -->
    <style>
        body {
            background: #efefef;
        }
        .alertMsg {
            position: absolute;
            top: 0px;
            z-index: 1035;
            height: 60.8px;
            width: 100vw;
            text-align: center;
            background: #F44336;
            color: #EEEEEE;
            font-size: 18px;
            line-height: 60.8px;
            
            animation: fadeOutTwoSec 500ms ease-out forwards 3500ms;
        }
        @keyframes fadeOutTwoSec {
            0%{
                top: 0;
            }
            100%{
                top: -65px;
            }
        }
        .mbr-iconfont {
            font-size: 1.3rem !important;
        }
        .profileIconNav {
            height: 35px;
            width: 35px;
            
            background: url("<?php if ($this->session->userdata('photo') == NULL) : ?><?php echo base_url(); ?>assets/Images/default-profile.jpg<?php endif; ?><?php if ($this->session->userdata('photo') != NULL) : ?>data:image/jpeg;base64,<?php echo base64_encode($this->session->userdata('photo')); ?><?php endif; ?>");
            
            margin-right: 10px;
            overflow: hidden;
            background-size: cover;
            background-position: center top;
        }
        
        .is-shown {
            display: block;
        }
        .is-hidden {
            display: none;
        }
        
        .wrapper {
            padding-top: 120px;
            color: #999999;
            box-sizing: border-box;
        }
        .is-card {
            min-height: 200px;
            background: white;
            padding: 20px;
        }
        .is-card-left {
        }
        .is-card-left .imgWrapper {
            background: red;
            width: 80%;
            margin-left: 10%;
            margin-bottom: 20px;
        }
        .is-card-left .imgWrapper .image {
            position:relative;
            overflow:hidden;
            padding-bottom:100%;
        }
        .is-card-left .imgWrapper .img {
            position: absolute;
            height: 100%;
            width: 100%;
            
            background: url("<?php if ($this->session->userdata('photo') == NULL) : ?><?php echo base_url(); ?>assets/Images/default-profile.jpg<?php endif; ?><?php if ($this->session->userdata('photo') != NULL) : ?>data:image/jpeg;base64,<?php echo base64_encode($this->session->userdata('photo')); ?><?php endif; ?>");
            
            background-size: cover;
            background-position: center top;
        }
        
        .is-card-left .sectionController .btn {
            display: block;
            padding: 10px 10px;
            width: 80%;
            margin: 0 auto;
            border-radius: 0 !important;
            border: none !important;
            box-shadow: none !important;
        }
        .is-card-left .sectionController .btn:nth-child(1) {
            border-radius: 3px 3px 0px 0px !important;
        }
        .is-card-left .sectionController .btn:last-child {
            border-radius: 0px 0px 3px 3px !important;
        }
        
        .is-card-left hr {
            border-top-width: 1.5px;
            margin-top: 1.4rem;
        }
        
        .is-card-left .buttonSection .btn {
            padding: 10px 10px;
            width: 180px;
            max-width: 80%;
        }
        .btn-warning {
            background-color: rgb(247, 237, 74);
            color: rgb(63, 60, 3);
            border-color: rgb(247, 237, 74);
        }
        .btn-warning:hover,
        .btn-warning:focus {
            background-color: rgb(234, 221, 10);
            border-color: rgb(234, 221, 10);
        }
        .btn-danger {
            background-color: #ff3366;
            border-color: #ff3366;
            color: #ffffff;
        }
        .btn-danger:hover,
        .btn-danger:focus {
            background-color: #e50039;
            border-color: #e50039;
        }
        .btn-info {
            background-color: #149dcc;
            border-color: #149dcc;
            color: #ffffff;
        }
        .btn-info:hover,
        .btn-info:focus {
            background-color: #0d6786;
            border-color: #0d6786;
        }
        
        .is-card-left .sectionController .is-pressed {
            background-color: #0d6786;
            border-color: #0d6786;
        }
        
        .is-card-right .textContent .textTitle {
            color: #666666;
            font-weight: 600;
            font-size: 1.2rem;
            font-family: "Open Sans", sans-serif;
            padding-right: 5px;
        }
        .is-card-right .textContent .textData {
            font-size: 1.1rem;
            
        }
    </style>
</head>

<body>
    
    <?php if ($this->session->flashdata('msg')) : ?>
        <div class="alertMsg">
            <?= $this->session->flashdata('msg') ?>
        </div>
    <?php endif; ?>
   
    <section class="menu cid-qH62TEfJyl" once="menu" id="menu1-e">
        <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
           
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <div class="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </button>
            
            <div class="menu-logo">
                <div class="navbar-brand">
                    
                    <span class="navbar-caption-wrap">
                        <a class="navbar-caption text-white display-5" href="<?php echo base_url(); ?>">Bidder's Arena</a>
                    </span>
                </div>
            </div>
            
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                    
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="#"><span class="mbri-cash mbr-iconfont mbr-iconfont-btn"></span>My Wallet<br></a></li>
                    
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="#"><span class="mbri-bookmark mbr-iconfont mbr-iconfont-btn"></span>My Projects<br></a></li>
                    
                    <li class="nav-item dropdown">
                        <a class="nav-link link dropdown-toggle text-white display-4" href="#" data-toggle="dropdown-submenu" aria-expanded="false"><span class="mbri-question mbr-iconfont mbr-iconfont-btn"></span>Help<br></a>

                        <div class="dropdown-menu">

                            <a class="dropdown-item text-white display-4" href="#" aria-expanded="false">FAQ</a>
                            
                            <a class="dropdown-item text-white display-4" href="#">Contact Us</a>

                            <a class="dropdown-item text-white display-4" href="#" aria-expanded="false">Our Charges</a>
                        </div>
                    </li>
                    
                    <li class="nav-item dropdown">
                        <a class="nav-link link dropdown-toggle text-white display-4" href="#" data-toggle="dropdown-submenu" aria-expanded="false">
                            <span class="profileIconNav"></span><?= $this->session->userdata('username') ?><br></a>

                        <div class="dropdown-menu">
                            <a class="dropdown-item text-white display-4" href="<?php echo base_url();?>dashboard">Dashboard</a>

                            <a class="dropdown-item text-white display-4" href="<?php echo base_url();?>profile" aria-expanded="false">Account Setting</a>

                            <a class="dropdown-item text-white display-4" href="<?php echo base_url();?>logout" aria-expanded="false">Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </section>
    
    <div class="container wrapper">
        <div class="row">
           
            <div class="col-sm-4 col-lg-3 is-card is-card-left">
                
                <div class="image imgWrapper">
                    <div class="image">
                        <div class="img"></div>
                    </div>
                </div>
                
                <div class="sectionController">
                    
                    <button id="desc" type="button" class="btn btn-info is-pressed" onclick="toggleRightDiv(this.id);">Description</button>
                    
                    <button id="account" type="button" class="btn btn-info" onclick="toggleRightDiv(this.id);">Account</button>
                    
                    <button id="projects" type="button" class="btn btn-info" onclick="toggleRightDiv(this.id);">My Projects</button>
                    
                    <button id="wallet" type="button" class="btn btn-info" onclick="toggleRightDiv(this.id);">Wallet</button>
                    
                    <button id="reviews" type="button" class="btn btn-info" onclick="toggleRightDiv(this.id);">Reviews</button>
                </div>
                
                <hr>
                
                <div class="buttonSection text-center">
                    <button type="button" class="btn btn-warning">Post Project</button>
                    
                    <button type="button" class="btn btn-danger">Browse Projects</button>
                </div>
                
            </div>
            
            <div class="col-sm-8 col-lg-9 is-card is-card-right">
                <div id="desc" class="cards is-shown">
                    <div class="textContent">
                        <span class="textTitle">Name: </span>
                        <span class="textData">Lorem ipsum dolor</span>
                    </div>

                    <div class="textContent">
                        <span class="textTitle">Address: </span>
                        <span class="textData">Lorem ipsum dolor</span>
                    </div>

                    <div class="textContent">
                        <span class="textTitle">State: </span>
                        <span class="textData">Lorem ipsum dolor</span>
                    </div>

                    <div class="textContent">
                        <span class="textTitle">Country: </span>
                        <span class="textData">Lorem ipsum dolor</span>
                    </div>

                    <div class="textContent">
                        <span class="textTitle">Pin-code: </span>
                        <span class="textData">Lorem ipsum dolor</span>
                    </div>

                    <div class="textContent">
                        <span class="textTitle">Mobile: </span>
                        <span class="textData">Lorem ipsum dolor</span>
                    </div>

                    <div class="textContent">
                        <span class="textTitle">E-mail: </span>
                        <span class="textData">Lorem ipsum dolor</span>
                    </div>

                    <div class="textContent">
                        <span class="textTitle">Qualification: </span>
                        <span class="textData">Lorem ipsum dolor</span>
                    </div>

                    <div class="textContent">
                        <span class="textTitle">Certification: </span>
                        <span class="textData">Lorem ipsum dolor</span>
                    </div>

                    <div class="textContent">
                        <span class="textTitle">Skills: </span>
                        <span class="textData">Lorem ipsum dolor</span>
                    </div>

                    <div class="textContent">
                        <span class="textTitle">Bio: </span>
                        <span class="textData">Lorem ipsum dolor</span>
                    </div>

                    <div class="textContent">
                        <span class="textTitle">Professional title: </span>
                        <span class="textData">Lorem ipsum dolor</span>
                    </div>
                </div>
                
                <div id="account" class="cards is-hidden">
                    
                    <div class="textContent">
                        <span class="textTitle">Bank name: </span>
                        <span class="textData">Lorem ipsum dolor</span>
                    </div>

                    <div class="textContent">
                        <span class="textTitle">Branch: </span>
                        <span class="textData">Lorem ipsum dolor</span>
                    </div>

                    <div class="textContent">
                        <span class="textTitle">IFSC: </span>
                        <span class="textData">Lorem ipsum dolor</span>
                    </div>

                    <div class="textContent">
                        <span class="textTitle">Payment method: </span>
                        <span class="textData">Lorem ipsum dolor</span>
                    </div>
                    
                </div>
                
                <div id="projects" class="cards is-hidden">
                    projects
                </div>
                
                <div id="wallet" class="cards is-hidden">
                    wallet
                </div>
                
                <div id="reviews" class="cards is-hidden">
                    reviews
                </div>
            </div>
            
        </div>
    </div>
    
    <script src="<?php echo base_url(); ?>assets/assets/web/assets/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/dropdown/js/script.min.js"></script>
    
    <script>
        function toggleRightDiv(id) {
            $(".sectionController button").removeClass("is-pressed");
            $(".sectionController #" +id).addClass("is-pressed");
            
            $(".is-card-right .cards").addClass("is-hidden");
            $(".is-card-right .cards").removeClass("is-shown");
            
            $(".is-card-right #" +id).removeClass("is-hidden");
            $(".is-card-right #" +id).addClass("is-shown");
        }
    </script>
</body>

</html>
