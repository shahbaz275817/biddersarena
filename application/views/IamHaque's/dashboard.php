<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Bidder's Arena | Dashboard</title>

	<!-- Bulma files -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.1/css/bulma.min.css">
	<!-- Fonts -->
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Josefin+Slab,Arvo,Open+Sans">

	<style>
		body {
			overflow: hidden;
            user-select: none;
		}
        ::-webkit-scrollbar {
            width: 0px;
            background: transparent;
        }
		.navbar-brand a {
			font-family: "Josefin Slab", "Open Sans", sans-serif !important;
			font-size: 1.5em;
			font-weight: bold;
			padding-left: 20px;
			padding-right: 20px;
		}
        
		.columns:last-child {
			margin-bottom: 0px;
		}
		.hero-foot .footerCol{
			background: rgba(21, 101, 192, 0.8);
		}
        
        .userImage {
            left: 50%;
            transform: translateX(-50%);
            border-radius: 50%;
            overflow: hidden;
        }
        
        .post {
            font-family: "Open Sans", sans-serif;
            margin: 0;
        }
        .post a {
            border-radius: 3px;
            width: 100%;
            height: 100%;
            font-family: "Arvo", "Open Sans", sans-serif;
            text-transform: uppercase;
        }
        
        .popPro {
            background: #209cee;
            color: white;
            line-height: 45px;
            height: 45px;
            font-family: "Arvo";
            border-radius: 5px;
            -webkit-box-shadow: inset 0 2px 3px rgba(10,10,10,.1), 0 0 0 1px rgba(10,10,10,.1);
            box-shadow: inset 0 2px 3px rgba(10,10,10,.1), 0 0 0 1px rgba(10,10,10,.1);
        }
        .project {
            padding-bottom: 0.5rem;
            margin-top: 1rem;
            border-bottom: 1px solid #dbdbdb;
            font-family: "Open Sans", sans-serif;
        }
        .project .projectSubtitle {
            font-weight: 500;
            padding-bottom: 6px;
        }
        .project .projectData {
            font-size: 0.9rem;
            font-weight: 400;
        }
        
        .pagination {
            margin-top: 1rem;
        }
        .pagination .is-current {
            color: white !important;
        }
        
        /* For notification */
		.alertMsg {
			position: absolute;
			z-index: 100;
			height: 52px;
            width: auto;
			text-align: center;
			background: #4CAF50;
			font-size: 16px;
            padding: 0px 20px;
            left: 50%;
            transform: translateX(-50%);
            display: table;
            border-radius: 0px 0px 5px 5px;
            
            -webkit-box-shadow: 0 2px 3px rgba(10,10,10,.1), 0 0 0 1px rgba(10,10,10,.1);
            box-shadow: 0 2px 3px rgba(10,10,10,.1), 0 0 0 1px rgba(10,10,10,.1);
            color: #4a4a4a;

			animation: fadeOutThreeSec 3000ms ease-out forwards;
		}
        @media screen and (max-width: 526px) {
            .alertMsg {
                border-radius: 0px;
            }
        }
        .alertMsg span {
			color: #F5F5F5;
            display: table-cell;
            vertical-align: middle;
        }
		@keyframes fadeOutThreeSec {
			0%{
                top: 0px;
			}
			75%{
                top: 0px;
			}
			100%{
                top: -62px;
			}
		}
	</style>
</head>

<body>

<!-- For notification -->
<?php if ($this->session->flashdata('msg')) : ?>
    <div class="alertMsg">
        <?= '<span>'.$this->session->flashdata('msg').'</span>' ?>
    </div>
<?php endif; ?>

<section class="hero is-info is-fullheight">
	<div class="hero-head">
		<nav class="navbar is-transparent">
			<div class="navbar-brand">
                
				<a class="navbar-item" href="<?php echo base_url();?>">
					Bidder's Arena
				</a>
				<div class="navbar-burger burger" data-target="navbarDropDown">
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>

			<div id="navbarDropDown" class="navbar-menu">
				<div class="navbar-end">
					<a class="navbar-item" href="<?php echo base_url();?>">Home</a>
					<a class="navbar-item" href="#">Categories</a>
					<a class="navbar-item" href="<?php echo base_url();?>profile">Profile</a>
					
					<a class="navbar-item" href="#">About Us</a>
					<a class="navbar-item" href="<?php echo base_url();?>logout">Logout</a>
				</div>
			</div>
		</nav>
	</div>

	<div class="hero-body">
		<div class="container">
        
		    <div class="columns profileBg">
                <div class="column is-10 is-offset-1">
                   
                    <div class="tile is-ancestor">
                        <div class="tile">
                            
                            <div class="tile is-4">
                                <div class="tile is-vertical">
                                    <div class="tile is-parent" style="max-height: 300px;">
                                        <div class="tile is-child box">
                                            <div class="photo">
                                                <figure class="image userImage is-96x96">
                                                    <?php if ($this->session->userdata('photo') == NULL) : ?>
														<img src="<?php echo base_url(); ?>assets/Images/default-profile.jpg">
													<?php endif; ?>
													<?php if ($this->session->userdata('photo') != NULL) : ?>
														<img src="data:image/jpeg;base64,<?php echo base64_encode($this->session->userdata('photo')); ?>">
													<?php endif; ?>
                                                </figure>
                                            </div>

                                            <div class="username has-text-centered is-size-5 has-text-weight-semibold" style="padding-top: 10px;">
												<?php if ($this->session->has_userdata('username')) : ?>
													<?= $this->session->userdata('username') ?>
												<?php endif; ?></div>

                                            <hr style="margin: 0.5rem 5% 0;">

                                            <div class="bio has-text-centered" style="padding: 10px 5% 0px 5%; font-size: 0.85rem;">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem ea ducimus aperiam possimus nulla, alias impedit debitis atque voluptate, doloribus eius rerum neque laborum culpa quaerat totam, dolore eligendi unde!
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="tile is-parent is-hidden-mobile">
                                        <div class="tile is-child box">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="tile">
                               
                                <div class="tile is-vertical">
                                    <div class="tile is-parent">
                                        <div class="tile is-child box">
                                            
                                            <div class="columns is-mobile post">
                                                <div class="column is-size-6 has-text-right">
                                                    <span>Want to hire someone to get your work done for you?</span>
                                                </div>
                                                
                                                <div class="column is-5">
                                                    <a class="button is-danger">
                                                        Post<br>
                                                        Project
                                                    </a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="tile is-parent">
                                        <div class="tile is-child box">
                                            <p class="has-text-centered is-size-4 popPro">Popular Projects</p>
                                            
                                                <!-- Project tile -->
                                                <div class="project columns is-desktop">

                                                    <div class="column">
                                                        <p class="projectTitle is-size-4 has-text-weight-semibold">Title of the project</p>

                                                        <p class="projectDesc is-size-6">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi expedita aperiam necessitatibus ipsam adipisci aliquam.</p>
                                                    </div>

                                                    <div class="column">
                                                        <div class="columns is-mobile has-text-centered">

                                                            <div class="c1 column is-4">
                                                                <p class="projectSubtitle is-size-6">Budget</p>
                                                                <p class="projectData is-size-7"><i class="fa fa-inr"></i> 00.00</p>
                                                            </div>

                                                            <div class="c2 column is-4">
                                                                <p class="projectSubtitle is-size-6">Skills</p>
                                                                <p class="projectData is-size-7">skill1</p>
                                                                <p class="projectData is-size-7">skill2</p>
                                                                <p class="projectData is-size-7">skill3</p>
                                                            </div>

                                                            <div class="c3 column is-4">
                                                                <a class="projectButton button is-info">BID</a>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>                                            
                                                <!-- Project tile -->
                                                <div class="project columns is-desktop">

                                                    <div class="column">
                                                        <p class="projectTitle is-size-4 has-text-weight-semibold">Title of the project</p>

                                                        <p class="projectDesc is-size-6">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi expedita aperiam necessitatibus ipsam adipisci aliquam.</p>
                                                    </div>

                                                    <div class="column">
                                                        <div class="columns is-mobile has-text-centered">

                                                            <div class="c1 column is-4">
                                                                <p class="projectSubtitle is-size-6">Budget</p>
                                                                <p class="projectData is-size-7"><i class="fa fa-inr"></i> 00.00</p>
                                                            </div>

                                                            <div class="c2 column is-4">
                                                                <p class="projectSubtitle is-size-6">Skills</p>
                                                                <p class="projectData is-size-7">skill1</p>
                                                                <p class="projectData is-size-7">skill2</p>
                                                                <p class="projectData is-size-7">skill3</p>
                                                            </div>

                                                            <div class="c3 column is-4">
                                                                <a class="projectButton button is-info">BID</a>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>                                            
                                                <!-- Project tile -->
                                                <div class="project columns is-desktop">

                                                    <div class="column">
                                                        <p class="projectTitle is-size-4 has-text-weight-semibold">Title of the project</p>

                                                        <p class="projectDesc is-size-6">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi expedita aperiam necessitatibus ipsam adipisci aliquam.</p>
                                                    </div>

                                                    <div class="column">
                                                        <div class="columns is-mobile has-text-centered">

                                                            <div class="c1 column is-4">
                                                                <p class="projectSubtitle is-size-6">Budget</p>
                                                                <p class="projectData is-size-7"><i class="fa fa-inr"></i> 00.00</p>
                                                            </div>

                                                            <div class="c2 column is-4">
                                                                <p class="projectSubtitle is-size-6">Skills</p>
                                                                <p class="projectData is-size-7">skill1</p>
                                                                <p class="projectData is-size-7">skill2</p>
                                                                <p class="projectData is-size-7">skill3</p>
                                                            </div>

                                                            <div class="c3 column is-4">
                                                                <a class="projectButton button is-info">BID</a>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            
                                            
                                            <!-- Pagination -->
                                            <nav class="pagination" role="navigation" aria-label="pagination">
                                                <a class="pagination-previous">Previous</a>
                                                <a class="pagination-next">Next page</a>
<!--
                                                <ul class="pagination-list">
                                                    <li>
                                                        <a class="pagination-link is-current" aria-label="Page 1" aria-current="page">1</a>
                                                    </li>
                                                    <li>
                                                        <a class="pagination-link" aria-label="Goto page 2">2</a>
                                                    </li>
                                                    <li>
                                                        <a class="pagination-link" aria-label="Goto page 3">3</a>
                                                    </li>
                                                    <li>
                                                        <a class="pagination-link" aria-label="Goto page 4">4</a>
                                                    </li>
                                                    <li>
                                                        <span class="pagination-ellipsis">&hellip;</span>
                                                    </li>
                                                </ul>
-->
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
		    
		</div>
	</div>

	<div class="hero-foot">
		<div class="footerCol columns is-mobile has-text-centered">
			<div class="column">
				<p>
					<strong>© </strong><a href="<?php echo '#';?>">Bidder's Arena</a>.
				</p>
			</div>
			<div class="column">
				<p>
					by <a href="#">ChamberOfDevelopers</a>
				</p>
			</div>
		</div>
	</div>
</section>

<!-- JQuery -->
<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>

<!-- Script to make hamburger menu working -->
<script>
	document.addEventListener('DOMContentLoaded', function () {
		// Get all "navbar-burger" elements
		var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

		// Check if there are any navbar burgers
		if ($navbarBurgers.length > 0) {

			// Add a click event on each of them
			$navbarBurgers.forEach(function ($el) {
				$el.addEventListener('click', function () {

					// Get the target from the "data-target" attribute
					var target = $el.dataset.target;
					var $target = document.getElementById(target);

					// Toggle the class on both the "navbar-burger" and the "navbar-menu"
					$el.classList.toggle('is-active');
					$target.classList.toggle('is-active');
				});
			});
		}

	});
</script>
</body>

</html>
