<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Bidder's Arena</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Arvo' rel='stylesheet' type='text/css'>

	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/icomoon.css">
	<!-- Simple Line Icons -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/simple-line-icons.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.theme.default.min.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
	<!-- Style -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/style_home.css">
	<link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>

	<!-- Modernizr JS -->
	<script src="<?php echo base_url();?>assets/js/modernizr-2.6.2.min.js"></script>

    <!--  Modal  -->
    <style>
        
.modal {
  bottom: 0;
  left: 0;
  position: absolute;
  right: 0;
  top: 0;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  display: none;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  overflow: hidden;
  position: fixed;
  z-index: 40;
}

.modal.is-active {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

.modal-background {
  bottom: 0;
  left: 0;
  position: absolute;
  right: 0;
  top: 0;
  background-color: rgba(10, 10, 10, 0.86);
}

.modal-content,
.modal-card {
  margin: 0 20px;
  max-height: calc(100vh - 160px);
  overflow: auto;
  position: relative;
  width: 100%;
}

@media screen and (min-width: 769px), print {
  .modal-content,
  .modal-card {
    margin: 0 auto;
    max-height: calc(100vh - 40px);
    width: 640px;
  }
}

.modal-close {
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  -moz-appearance: none;
  -webkit-appearance: none;
  background-color: rgba(10, 10, 10, 0.2);
  border: none;
  border-radius: 290486px;
  cursor: pointer;
  display: inline-block;
  -webkit-box-flex: 0;
      -ms-flex-positive: 0;
          flex-grow: 0;
  -ms-flex-negative: 0;
      flex-shrink: 0;
  font-size: 0;
  height: 20px;
  max-height: 20px;
  max-width: 20px;
  min-height: 20px;
  min-width: 20px;
  outline: none;
  position: relative;
  vertical-align: top;
  width: 20px;
  background: none;
  height: 40px;
  position: fixed;
  right: 20px;
  top: 20px;
  width: 40px;
}

.modal-close:before, .modal-close:after {
  background-color: white;
  content: "";
  display: block;
  left: 50%;
  position: absolute;
  top: 50%;
  -webkit-transform: translateX(-50%) translateY(-50%) rotate(45deg);
          transform: translateX(-50%) translateY(-50%) rotate(45deg);
  -webkit-transform-origin: center center;
          transform-origin: center center;
}

.modal-close:before {
  height: 2px;
  width: 50%;
}

.modal-close:after {
  height: 50%;
  width: 2px;
}

.modal-close:hover, .modal-close:focus {
  background-color: rgba(10, 10, 10, 0.3);
}

.modal-close:active {
  background-color: rgba(10, 10, 10, 0.4);
}

.modal-close.is-small {
  height: 16px;
  max-height: 16px;
  max-width: 16px;
  min-height: 16px;
  min-width: 16px;
  width: 16px;
}

.modal-close.is-medium {
  height: 24px;
  max-height: 24px;
  max-width: 24px;
  min-height: 24px;
  min-width: 24px;
  width: 24px;
}

.modal-close.is-large {
  height: 32px;
  max-height: 32px;
  max-width: 32px;
  min-height: 32px;
  min-width: 32px;
  width: 32px;
}

.modal-card {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  max-height: calc(100vh - 40px);
  overflow: hidden;
}

.modal-card-head,
.modal-card-foot {
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  background-color: whitesmoke;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-negative: 0;
      flex-shrink: 0;
  -webkit-box-pack: start;
      -ms-flex-pack: start;
          justify-content: flex-start;
  padding: 20px;
  position: relative;
}

.modal-card-head {
  border-bottom: 1px solid #dbdbdb;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
}

.modal-card-title {
  color: #363636;
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
  -ms-flex-negative: 0;
      flex-shrink: 0;
  font-size: 1.5rem;
  line-height: 1;
}

.modal-card-foot {
  border-bottom-left-radius: 5px;
  border-bottom-right-radius: 5px;
  border-top: 1px solid #dbdbdb;
}

.modal-card-foot .button:not(:last-child) {
  margin-right: 10px;
}

.modal-card-body {
  -webkit-overflow-scrolling: touch;
  background-color: white;
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
  -ms-flex-negative: 1;
      flex-shrink: 1;
  overflow: auto;
  padding: 20px;
}
    </style>
    
    <style>
.tabs {
  -webkit-overflow-scrolling: touch;
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  -webkit-box-align: stretch;
      -ms-flex-align: stretch;
          align-items: stretch;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  font-size: 1rem;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
  overflow: hidden;
  overflow-x: auto;
  white-space: nowrap;
}

.tabs:not(:last-child) {
  margin-bottom: 1.5rem;
}

.tabs a {
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  border-bottom-color: #dbdbdb;
  border-bottom-style: solid;
  border-bottom-width: 1px;
  color: #4a4a4a;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  margin-bottom: -1px;
  padding: 0.5em 1em;
  vertical-align: top;
}

.tabs a:hover {
  border-bottom-color: #363636;
  color: #363636;
}

.tabs li {
  display: block;
}

.tabs li.is-active a {
  border-bottom-color: #3273dc;
  color: #3273dc;
}

.tabs ul {
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  border-bottom-color: #dbdbdb;
  border-bottom-style: solid;
  border-bottom-width: 1px;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
  -ms-flex-negative: 0;
      flex-shrink: 0;
  -webkit-box-pack: start;
      -ms-flex-pack: start;
          justify-content: flex-start;
}

.tabs ul.is-left {
  padding-right: 0.75em;
}

.tabs ul.is-center {
  -webkit-box-flex: 0;
      -ms-flex: none;
          flex: none;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  padding-left: 0.75em;
  padding-right: 0.75em;
}

.tabs ul.is-right {
  -webkit-box-pack: end;
      -ms-flex-pack: end;
          justify-content: flex-end;
  padding-left: 0.75em;
}

.tabs .icon:first-child {
  margin-right: 0.5em;
}

.tabs .icon:last-child {
  margin-left: 0.5em;
}

.tabs.is-centered ul {
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
}

.tabs.is-right ul {
  -webkit-box-pack: end;
      -ms-flex-pack: end;
          justify-content: flex-end;
}

.tabs.is-boxed a {
  border: 1px solid transparent;
  border-radius: 3px 3px 0 0;
}

.tabs.is-boxed a:hover {
  background-color: whitesmoke;
  border-bottom-color: #dbdbdb;
}

.tabs.is-boxed li.is-active a {
  background-color: white;
  border-color: #dbdbdb;
  border-bottom-color: transparent !important;
}

.tabs.is-fullwidth li {
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
  -ms-flex-negative: 0;
      flex-shrink: 0;
}

.tabs.is-toggle a {
  border-color: #dbdbdb;
  border-style: solid;
  border-width: 1px;
  margin-bottom: 0;
  position: relative;
}

.tabs.is-toggle a:hover {
  background-color: whitesmoke;
  border-color: #b5b5b5;
  z-index: 2;
}

.tabs.is-toggle li + li {
  margin-left: -1px;
}

.tabs.is-toggle li:first-child a {
  border-radius: 3px 0 0 3px;
}

.tabs.is-toggle li:last-child a {
  border-radius: 0 3px 3px 0;
}

.tabs.is-toggle li.is-active a {
  background-color: #3273dc;
  border-color: #3273dc;
  color: #fff;
  z-index: 1;
}

.tabs.is-toggle ul {
  border-bottom: none;
}

.tabs.is-toggle.is-toggle-rounded li:first-child a {
  border-bottom-left-radius: 290486px;
  border-top-left-radius: 290486px;
  padding-left: 1.25em;
}

.tabs.is-toggle.is-toggle-rounded li:last-child a {
  border-bottom-right-radius: 290486px;
  border-top-right-radius: 290486px;
  padding-right: 1.25em;
}

.tabs.is-small {
  font-size: 0.75rem;
}

.tabs.is-medium {
  font-size: 1.25rem;
}

.tabs.is-large {
  font-size: 1.5rem;
}
    </style>
    
    <style>
        .alertMsg {
            position: absolute;
            z-index: 100;
            height: 48.8px;
            width: 100vw;
            text-align: center;
            background: #F44336;
            color: #EEEEEE;
            font-size: 18px;
            line-height: 48.8px;
            
            animation: fadeOutTwoSec 2000ms ease-out forwards 2500ms;
        }
        @media screen and (max-width: 768px) {
            .alertMsg {
                top: 70px;
            }
        }
        @keyframes fadeOutTwoSec {
            0%{
                top: 0;
            }
            100%{
                top: -50px;
            }
        }
        
        .tabs {
            overflow: hidden;
        }
        .tabs li {
            width: 50%;
            cursor: pointer;
        }
        .tabs li a{
            text-decoration: none;
            height: 60px;
            font-size: 25px;
        }
        
        .formClass {
            padding: 20px 30px;
        }
        .formClass .forgot {
            text-align: center;
            padding: 0;
            margin: 0;
            padding-top: 15px;
        }
        .formClass .forgot a {
            text-decoration: none;
            cursor: pointer;
            display: inline-block;
        }
        .formClass .help {
            color: red;
            font-weight: bold;
        }
        .formClass .form-control,
        .formClass .btn {
            height: 50px;
        }
        .formClass .btn {
            width: 100%;
            margin-top: 15px;
        }
        .formClass .btn:hover {
            background: black !important;
            color: white !important;
        }
        .formClass .form-control:focus {
            border-color: #000;
            outline: 0;
            -webkit-box-shadow: none;
            box-shadow: none;
        }
    </style>
    
    <!--  Navigation bar CSS  -->
    <style>
        nav{
        display: block;
            width: 100%;
            position: fixed;
            top: 0;
            height: 48.8px;
          background: rgba(0, 0, 0, 1);
        }
        ul{
          margin: 0px;
          padding: 0px;
          list-style: none;
        }
        ul.dropdown{ 
          position: relative; 
          width: 100%; 
        }
        ul.dropdown li{ 
          font-weight: bold; 
          float: right; 
          width: 140px; 
          position: relative;
                display: block;
        }
        ul.dropdown a:hover{
            color: #FFF; 
        }
        ul.dropdown li a { 
          display: block; 
          padding: 10px 6px;
          color: #FFF; 
          position: relative; 
          z-index: 10; 
          text-align: center;
          text-decoration: none;
          font-weight: 300;
        }

        ul.dropdown li a:hover,
        ul.dropdown li a.hover{ 
          background: #3498db;
          position: relative;
          color: #fff;
        }
        ul.dropdown ul{ 
         display: none;
         position: absolute; 
          top: 0; 
          left: 0; 
          width: 140px; 
          z-index: 10;
        }

        ul.dropdown ul li { 
          font-weight: normal; 
          background: rgba(0, 0, 0, 1); 
          color: #FFF; 
        }

        ul.dropdown ul li a{ 
          display: block; 
          background: rgba(0, 0, 0, 1) !important; 
          color: #34495e;
        } 

        ul.dropdown ul li a:hover{
          display: block; 
          background: #3498db !important;
          color: #fff !important;
        } 
        nav ul .logo {
            float: left;
            width: 120px;
              background: #3498db;
              color: #fff;
            font-weight: bold;
        }
        nav ul .logo a {
            font-weight: bold;
        }
        nav ul .logo span {
            display: none;
        }
        
        @media (min-width: 720px) {
            ul.dropdown li{ 
              width: 100px; 
            }
            ul.dropdown li a { 
              padding: 10px 3px;
            }
            ul.dropdown ul{  
              width: 100px; 
            }
            nav ul .logo {
                width: 120px;
            }
        }
        
        
        @media (min-width: 1000px) {
           ul.dropdown li{ 
              width: 140px; 
            }
            ul.dropdown li a { 
              padding: 10px 6px;
            }
            ul.dropdown ul{  
              width: 140px; 
            }
            nav ul .logo {
                width: 140px;
            }
        }
        
        @media (max-width: 720px) {
            ul.dropdown li{ 
              width: 100vw; 
                background: black;
            }
            ul.dropdown li a { 
              padding: 10px 6px;
            }
            ul.dropdown ul{  
              width: 140px; 
            }
            nav ul .logo {
                width: 100vw;
                background: #3498db;
                display: block;
            }
        }
    </style>
    
    <style>
        #fh5co-our-services, #fh5co-about-us, #fh5co-features, #fh5co-testimonials, #fh5co-pricing, #fh5co-press {
            padding-top: 3em;
        }
        .showMore {
            text-decoration: none;
        }
        .showMore:hover {
            text-decoration: none;
        }
        .footerP {
            padding: 0;
            margin: 0;
        }
        .footerP a:hover {
            text-decoration: none;
            color: black;
        }
        #offerShift {
            margin-left: 50%;
        }
        @media screen and (max-width: 480px) {
            #offerShift {
                margin-left: 0%;
            }
        }
    </style>
</head>

<body>

<div class="modal" style="z-index: 100;">
    <div class="modal-background"></div>
        <div class="modal-content container" style="max-height: none;">
        
        <div class="tabs is-large is-centered" style="margin: 0px;">
            <ul style="padding:0px; margin: 0px;">
                <li class="is-active loginClass"><a onclick="switchTab(this.id);" id="idLoginLink">Login</a></li>
                
                <li class="registerClass"><a onclick="switchTab(this.id);" id="idRegisterLink">Register</a></li>
            </ul>
        </div>
        
        <div class="formClass row">
           
            <form method="POST" action="" id="forgotPassword" style="display: none;" class="col col-xs-12" autocomplete="off">
               
                <p class="forgot">Lost your password? Please enter your email address. You will receive a link to create a new password.</p>
                
                <div class="form-group" style="margin-top: 20px;">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-envelope"></span>
                        </div>
                        <input type="email" class="form-control" placeholder="E-mail" name="email" required>
                    </div>
                </div>
                
                <button type="submit" class="btn btn-primary" name="reset">Reset Password</button>
                
                <p class="forgot"><a id="goToLogin" onclick="toggleForgot(this.id);">Back to login</a></p>
            </form>
            
            <form method="post" action="<?php echo base_url();?>login" id="login" style="display: block;" class="col col-xs-12" autocomplete="off">

				<?php if ($this->session->flashdata('login_error')) : ?>
					<p id="emailHelp" class="form-text text-muted" style="text-align: center !important; color: red; font-weight: bold;"><?= $this->session->flashdata('login_error')?></p>
				<?php endif; ?>
            
                <div class="form-group">
                    <label class="control-label" for="email">
                        Email address
                    </label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-envelope"></span>
                        </div>
                        <input type="email" class="form-control" placeholder="Enter email" name="email" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label" for="email">
                        Password
                    </label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-lock"></span>
                        </div>
                        <input type="password" class="form-control" placeholder="Enter password" name="password">
                    </div>
                </div>
                

                <button type="submit" class="btn btn-primary" name="submit">Login</button>
                
                <p class="forgot"><a id="goToForgot" onclick="toggleForgot(this.id);">Forgot password?</a></p>
            </form>

            <form method="post" action="<?php echo base_url();?>signup" id="register" style="display: none;" class="col col-xs-12" autocomplete="off">
                
                <?php if (validation_errors()) : ?>
                    <p id="emailHelp" class="form-text text-muted" style="text-align: center !important; color: red; font-weight: bold;"><?= validation_errors(); ?></p>
                <?php endif; ?>
                
                <div class="form-group">
                    <label class="control-label" for="email">
                        Username
                    </label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-user"></span>
                        </div>
                        <input type="text" id="username" class="form-control" placeholder="Enter username" name="username" required>
                    </div>
					<small id="username_result" class="form-text text-muted" style="color: red; font-weight: 400;"></small>
                </div>

                <div class="form-group">
                    <label class="control-label" for="email">
                        Email address
                    </label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-envelope"></span>
                        </div>
                        <input type="email" id="email" class="form-control" placeholder="Enter email" name="email" required>
                    </div>
					<small id="email_result" class="form-text text-muted" style=" color: red; font-weight: 400;"></small>
                </div>

                <div class="form-group">
                    <label class="control-label" for="email">
                        Password
                    </label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-lock"></span>
                        </div>
                        <input type="password" id="password" class="form-control" placeholder="Enter password" name="password">
                    </div>
					<small id="password_result" class="form-text text-muted" style=" color: red; font-weight: 400;"></small>
                </div>

                <div class="form-group">
                    <label class="control-label" for="email">
                        Confirm password
                    </label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-lock"></span>
                        </div>
                        <input type="password" id="confirm_password" class="form-control" placeholder="Re-enter password" name="confirm_password">
                    </div>
					<small id="password_match_result" class="form-text text-muted" style=" color: red; font-weight: 400;"></small>
                </div>
                
                <div class="radio">
                    <label class="radio-inline" style="padding: 0px; margin-right: 10px;">
                        Signing up to?
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="signUpAs" style="margin-top: 8px;">
                        Hire
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="signUpAs" style="margin-top: 8px;">
                        Work
                    </label>
                </div>

                <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                
                <p class="forgot">By signing in, you accept all our <a href="#">terms and conditions.</a></p>
            </form>
        </div>
        
        </div>
    <button class="modal-close is-large" aria-label="close" onclick="hideModal();"></button>
</div>

<?php if ($this->session->flashdata('msg')) : ?>
    <div class="alertMsg">
        <?= $this->session->flashdata('msg') ?>
    </div>
<?php endif; ?>

<div id="slider" data-section="home">
    <div class="item" style="background-image:url(<?php echo base_url();?>assets/Images/Header/1.jpeg);">
        
        <nav style="z-index: 200;">
            <ul class="dropdown">
                
                <li class="logo"><a href="#">Bidder's Arena</a></li>
                
                <?php if ($this->session->has_userdata('user_id') == 1 && $this->session->has_userdata('username') == 1) : ?>
                    
                    <li><a href="#" >Welcome <?= $this->session->userdata('username') ?>!</a>
                        <ul class="sub_menu">
                            <li><a href="<?php echo base_url();?>dashboard">Dashboard</a></li>
							<li><a href="<?php echo base_url();?>profile">Profile</a></li>
							<li class="divider"></li>
							<li><a href="<?php echo base_url();?>logout">Logout</a></li>
                        </ul>
                    </li>
                    
                <?php endif; ?>
                
                <?php if ($this->session->has_userdata('username') == 0) : ?>
                    
                    <li><a href="#" id="registerNavLink" onclick="showModal(this.id);"><span>Sign Up</span></a></li>
                    <li><a href="#" id="loginNavLink" onclick="showModal(this.id);"><span>Log in</span></a></li>
                    
                <?php endif; ?>
                
                <li><a href="#" data-nav-section="contactForm">Contact us</a></li>
                <li><a href="#" data-nav-section="categories">Categories</a></li>
                <li><a href="#" data-nav-section="work">How we work</a></li>
                <li><a href="#" data-nav-section="home">Home</a></li>
            </ul>
        </nav> 
        
        <div class="container">
            <div class="row">
                <div class="col-xs-10 col-md-8 title">
                    <h1 class="fh5co-lead">We're here to help!</h1>
                    <h2 class="fh5co-sub-lead">Let us get all your work done for you.</h2>
                    
                    <a class="btn btn-primary btn-md" href="<?php echo base_url();?>register"><span>Post A Project</span></a>
                </div>
            </div>
        </div>
    </div>

</div>

<div id="fh5co-press" data-section="work">
	<div class="container">
		<div class="row">
			<div class="col-md-12 section-heading text-center">
				<h2 class="single-animate animate-press-1">How We Work</h2>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-md-8 col-md-offset-2">
			<div class="fh5co-press-item to-animate">
				<div class="fh5co-press-img" style="background-image: url(<?php echo base_url();?>assets/Images/Work/1.jpg); background-position: center;">
				</div>
				<div class="fh5co-press-text">
					<h3 class="h2 fh5co-press-title" style="font-size: 40px;">Requirement <span class="fh5co-border" style="width: 50px;"></span></h3>
					<p style="font-size: 20px;">Tell us what you want to get done by simply uploading your project details, skills required and we will analyze your project. We will notify skilled workers of your required skills about your project.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-md-8 col-md-offset-2">
			<div class="fh5co-press-item to-animate">
				<div class="fh5co-press-img" style="background-image: url(<?php echo base_url();?>assets/Images/Work/2.jpg); left: 0; background-position: center;">
				</div>
				<div class="fh5co-press-text" id="offerShift">
					<h3 class="h2 fh5co-press-title" style="font-size: 40px;">Offers <span class="fh5co-border" style="width: 50px;"></span></h3>
					<p style="font-size: 20px;">The workers offer their best price on your project and you can view their profile and previous works and compare them to decide the best candidate for your project. Award the project whomever you like.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-md-8 col-md-offset-2">
			<div class="fh5co-press-item to-animate">
				<div class="fh5co-press-img" style="background-image: url(<?php echo base_url();?>assets/Images/Work/3.jpg); background-position: center;">
				</div>
				<div class="fh5co-press-text">
					<h3 class="h2 fh5co-press-title" style="font-size: 40px;">Management <span class="fh5co-border" style="width: 50px;"></span></h3>
					<p style="font-size: 20px;">We will keep you updated using our management tools about the progress of the projects. Pay only when satisfied with the quality of work done.</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="fh5co-our-services" data-section="categories">
	<div class="container">
		<div class="row row-bottom-padded-xs">
			<div class="col-md-12 section-heading text-center">
				<h2 class="to-animate">Exlore The Categories</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-lg-3">
				<div class="box to-animate">
					<div class="icon" style="background-image: url(<?php echo base_url();?>assets/Images/Categories/category%20finance.png); background-position: center; background-size: contain; border-radius: 0;"></div>
					<h3>Website, IT &amp; Software</h3>
                    <ul>
                        <li>C Programming</li>
                        <li>Cloud Computing</li>
                        <li>Game Development</li>
                        <li>Javascript</li>
                        <li>MySQL</li>
                        <li>PHP</li>
                        <li>Website Management</li>
                        <li>Wordpress</li>
                        <li><a href="#" class="showMore">show more...</a></li>
                    </ul>
				</div>
				<div class="box to-animate">
					<div class="icon" style="background-image: url(<?php echo base_url();?>assets/Images/Categories/category%20logo%20design.png); background-position: center; background-size: contain; border-radius: 0;"></div>
					<h3>Design &amp; Architecture</h3>
                    <ul>
                        <li>After Effects</li>
                        <li>Animation</li>
                        <li>Building Architecture</li>
                        <li>Graphic Design</li>
                        <li>Illustrator</li>
                        <li>Logo Design</li>
                        <li>Photoshop</li>
                        <li>Website Design</li>
                        <li><a href="#" class="showMore">show more...</a></li>
                    </ul>
				</div>
			</div>

			<div class="col-md-6 col-lg-3">
				<div class="box to-animate">
					<div class="icon" style="background-image: url(<?php echo base_url();?>assets/Images/Categories/category%20marketing.png); background-position: center; background-size: contain; border-radius: 0;"></div>
					<h3>Business, Legal &amp; Accounting</h3>
                    <ul>
                        <li>Accounting</li>
                        <li>Attorney</li>
                        <li>Business Analysis</li>
                        <li>Human Resources</li>
                        <li>Payroll</li>
                        <li>Project Management</li>
                        <li>Startups</li>
                        <li>Tax</li>
                        <li><a href="#" class="showMore">show more...</a></li>
                    </ul>
				</div>
				<div class="box to-animate">
					<div class="icon" style="background-image: url(<?php echo base_url();?>assets/Images/Categories/content%20writing.png); background-position: center; background-size: contain; border-radius: 0;"></div>
					<h3>Writing &amp; Content</h3>
                    <ul>
                        <li>Article Writing</li>
                        <li>Blog</li>
                        <li>Book Writing</li>
                        <li>Financial Research</li>
                        <li>Powerpoint</li>
                        <li>Product Descriptions</li>
                        <li>Report Writing</li>
                        <li>Technical Writing</li>
                        <li><a href="#" class="showMore">show more...</a></li>
                    </ul>
				</div>
			</div>

			<div class="col-md-6 col-lg-3">
				<div class="box to-animate">
					<div class="icon" style="background-image: url(<?php echo base_url();?>assets/Images/Categories/Mobile%20developement.png); background-position: center; background-size: contain; border-radius: 0;"></div>
					<h3>Mobile Development</h3>
                    <ul>
                        <li>Android</li>
                        <li>Java</li>
                        <li>J2ME</li>
                        <li>Kotlin</li>
                        <li>iPad</li>
                        <li>iPhone</li>
                        <li>Mobile App Development</li>
                        <li>Windows Phone</li>
                        <li><a href="#" class="showMore">show more...</a></li>
                    </ul>
				</div>
				<div class="box to-animate">
					<div class="icon" style="background-image: url(<?php echo base_url();?>assets/Images/Categories/sales%20and%20marketing.png); background-position: center; background-size: contain; border-radius: 0;"></div>
					<h3>Sales &amp; Marketing</h3>
                    <ul>
                        <li>Advertising</li>
                        <li>Affiliate Marketing</li>
                        <li>Branding</li>
                        <li>Content Marketing</li>
                        <li>Google Adwords</li>
                        <li>Internet Marketing</li>
                        <li>Journalist</li>
                        <li>Social Media Marketing</li>
                        <li><a href="#" class="showMore">show more...</a></li>
                    </ul>
				</div>
			</div>

			<div class="col-md-6 col-lg-3">
				<div class="box to-animate">
					<div class="icon" style="background-image: url(<?php echo base_url();?>assets/Images/Categories/website.png); background-position: center; background-size: contain; border-radius: 0;"></div>
					<h3>Engineering &amp; Science</h3>
                    <ul>
                        <li>Algorithm</li>
                        <li>Arduino</li>
                        <li>AutoCAD</li>
                        <li>Circuit Design</li>
                        <li>Engineering</li>
                        <li>Machine Learning</li>
                        <li>Matlab and Mathematica</li>
                        <li>Structural Engineering</li>
                        <li><a href="#" class="showMore">show more...</a></li>
                    </ul>
				</div>
				<div class="box to-animate">
					<div class="icon" style="background-image: url(<?php echo base_url();?>assets/Images/Categories/virtual%20assistant.png); background-position: center; background-size: contain; border-radius: 0;"></div>
					<h3>Translation</h3>
                    <ul>
                        <li>Arabic</li>
                        <li>Dutch</li>
                        <li>English</li>
                        <li>Italian</li>
                        <li>Russian</li>
                        <li>Tamil</li>
                        <li>Urdu</li>
                        <li>Farsi</li>
                        <li><a href="#" class="showMore">show more...</a></li>
                    </ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="slider" data-section="getStarted">
    <div class="item" style="background-image: url(<?php echo base_url();?>assets/Images/Header/2.jpeg); 
  vertical-align: middle; text-align: center; z-index: 0;">
        
        <div class="container">
            <div class="row">
                <div class="col-xs-12 get">
                    <h1 class="getTitle">App coming soon!</h1>
                    <h2 class="getSubTitle" style="font-size: 2.5rem !important; font-family: 'Open Sans', sans-serif; font-weight: 400; color: white; ">Take your workplace wherever you go with our upcoming app!</h2>
                </div>
            </div>
        </div>
        
    </div>
</div>

<div id="slider" data-section="getStarted">
    <div class="item" style="background-image: url(<?php echo base_url();?>assets/Images/Header/3.jpg); 
  vertical-align: middle; text-align: center; z-index: 0;">
        
        <div class="container">
            <div class="row">
                <div class="col-xs-12 get">
                    <h1 class="getTitle">Let’s get this relationship started!</h1>
                    
                    <div class="center-wrap">
                        <div class="button">
                            <a href="#" onclick="showModal('registerNavLink');">Get Started<span class="shift">›</span></a>
                            <div class="mask"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>

<div id="fh5co-our-services" data-section="contactForm">
    <div class="container">
        <div class="row row-bottom-padded-xs">
            <div class="col-md-12 section-heading text-center">
                <h2 class="to-animate">Contact Us</h2>
            </div>
        </div>
        <div class="row">
            <div class="box to-animate col-md-6" style="background-image: url(<?php echo base_url();?>assets/Images/ab_bg.jpg); background-size: cover; height: 432px">
                <div class="bg" style="text-align: left">
                    <div class="au" style="color: black;">
                        <i class="icon-phone" aria-hidden="true"></i>
                        <span class="headingSpan">Call us</span><br>
                        <span class="con">+91 (818) 204 7199</span>
                    </div>
                    <div class="au" style="color: black;">
                        <i class="icon-envelope" aria-hidden="true"></i>
                        <span class="headingSpan">Email us</span><br>
                        <span class="con">admin@biddersarena.com</span>
                    </div>
                    <div class="au" style="color: black;">
                        <i class="icon-location-pin" aria-hidden="true"></i>
                        <span class="headingSpan">Reach us</span><br>
                        <span class="con">111/111, somewthere</span><br>
                        <span class="con">someplace in lucknow</span><br>
                        <span class="con">Lucknow</span><br>
                        <span class="con">India</span>
                    </div>
                </div>
            </div>

            <div class="box to-animate col-md-6" style="text-align: left">
                <form action="<?php echo base_url();?>user/feedback" method="post">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Enter name"  name="name">
                    </div>
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email"  name="email">
                    </div>
                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea type="text" class="form-control" id="message" aria-describedby="emailHelp" placeholder="Enter your message" rows="4" style="resize: none;"  name="message"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Send</button>
                </form>
            </div>
        </div>
    </div>
</div>

<footer id="footer" data-section="contact" role="contentinfo">
	<div class="container">
		<div class="row row-bottom-padded-xs">
			<div class="col-md-12">
                <p class="footerP text-center"><a href="<?php echo base_url(); ?>about">About us</a></p>
                <p class="footerP text-center"><a href="<?php echo base_url(); ?>privacy">Privacy policy</a></p>
                <p class="footerP text-center"><a href="<?php echo base_url(); ?>terms">Terms &amp; Conditions</a></p>
                		
				<p class="copyright text-center">&copy; 2017  <a href="<?php echo base_url();?>about">Bidders Arena</a>. All Rights Reserved. <br> Designed &amp; Developed by <a href="<?php echo base_url();?>" target="_blank">Chamber of Developers</a></p>
			</div>
		</div>
	</div>
</footer>

<!-- jQuery -->
<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="<?php echo base_url();?>assets/js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="<?php echo base_url();?>assets/js/jquery.waypoints.min.js"></script>
<!-- Owl Carousel -->
<script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>

<!-- Main JS (Do not remove) -->
<script src="<?php echo base_url();?>assets/js/main.js"></script>

<script>
var maxHeight = 400;

$(function(){

    $(".dropdown > li").hover(function() {
    
         var $container = $(this),
             $list = $container.find("ul"),
             $anchor = $container.find("a"),
             height = $list.height() * 1.1,       // make sure there is enough room at the bottom
             multiplier = height / maxHeight;     // needs to move faster if list is taller
        
        // need to save height here so it can revert on mouseout            
        $container.data("origHeight", $container.height());
        
        // so it can retain it's rollover color all the while the dropdown is open
        $anchor.addClass("hover");
        
        // make sure dropdown appears directly below parent list item    
        $list
            .show()
            .css({
                paddingTop: $container.data("origHeight")
            });
        
        // don't do any animation if list shorter than max
        if (multiplier > 1) {
            $container
                .css({
                    height: maxHeight,
                    overflow: "hidden"
                })
                .mousemove(function(e) {
                    var offset = $container.offset();
                    var relativeY = ((e.pageY - offset.top) * multiplier) - ($container.data("origHeight") * multiplier);
                    if (relativeY > $container.data("origHeight")) {
                        $list.css("top", -relativeY + $container.data("origHeight"));
                    };
                });
        }
        
    }, function() {
    
        var $el = $(this);
        
        // put things back to normal
        $el
            .height($(this).data("origHeight"))
            .find("ul")
            .css({ top: 0 })
            .hide()
            .end()
            .find("a")
            .removeClass("hover");
    
    });  
    
});
</script>

<script>
    function showModal(id) {
        $(".modal").addClass("is-active");
        if(id == "loginNavLink") {
            $("#login").css("display", "block");
            $("#register").css("display", "none");
            $(".loginClass").addClass("is-active");
            $(".registerClass").removeClass("is-active");
        } else if (id == "registerNavLink") {
            $("#login").css("display", "none");
            $("#register").css("display", "block");
            $(".loginClass").removeClass("is-active");
            $(".registerClass").addClass("is-active");
        }
    }
    function hideModal() {
        $(".modal").removeClass("is-active");
    }
    function switchTab(id) {
        if(id == "idLoginLink") {
            $("#login").css("display", "block");
            $("#register").css("display", "none");
            $(".loginClass").addClass("is-active");
            $(".registerClass").removeClass("is-active");
            $("#forgotPassword").css("display", "none");
            
            $("#register input").val('');
            $("#register #emailHelp").hide();
            $("#forgotPassword input").val('');
            $("#register small").html("");
            $("#register input[name='signUpAs']").attr('checked', false);s
        } else if(id == "idRegisterLink") {
            
            $("#login").css("display", "none");
            $("#register").css("display", "block");
            $(".loginClass").removeClass("is-active");
            $(".registerClass").addClass("is-active");
            $("#forgotPassword").css("display", "none");
            
            $("#login input").val('');
            $("#login #emailHelp").hide();
            $("#forgotPassword input").val('');
        }
    }
    function toggleForgot(id) {
        if(id == "goToLogin") {
            $("#login").css("display", "block");
            $("#forgotPassword").css("display", "none");
            
            $("#register input").val('');
            $("#register small").html("");
            $("#register input[name='signUpAs']").attr('checked', false);
            $("#forgotPassword input").val('');
        } else if(id == "goToForgot") {
            
            $("#login").css("display", "none");
            $("#forgotPassword").css("display", "block");
            
            $("#login input").val('');
            $("#login #emailHelp").hide();
            $("#forgotPassword input").val('');
        }
    }

	$('.dropdown').hover(function(){
		$('.dropdown-toggle', this).trigger('click');
	});

	$(document).ready(function() {
        
        if(window.location.href.indexOf('login') != -1) {
			$('.modal').addClass('is-active');
		}
		if(window.location.href.indexOf('register') != -1) {
			$('.modal').addClass('is-active');
            $("#login").css("display", "none");
            $("#register").css("display", "block");
            $(".loginClass").removeClass("is-active");
            $(".registerClass").addClass("is-active");
		}

	});

	$(document).ready(function(){
		$('#username').change(function(){
			var username = $('#username').val();
			if(username != '')
			{
				$.ajax({
					url:"<?php echo base_url(); ?>user/check_username_availability",
					method:"POST",
					data:{username : username},
					success:function(data){
						$('#username_result').html(data);
                        if( data == "Username Available" ) {
                            $('#username_result').css("color", "green");
                        } else {
                            $('#username_result').css("color", "red");
                        }
					}
				});
			}
		});

		$('#email').change(function(){
			var email = $('#email').val();
			if(email != '')
			{
				$.ajax({
					url:"<?php echo base_url(); ?>user/check_email_availability",
					method : "POST",
					data:{email:email},
					success:function(data){
						$('#email_result').html(data);
					}
				});
			}
		});

		$('#password').change(function(){
			var password = $('#password').val();
			if(password != '')
			{
				$.ajax({
					url:"<?php echo base_url(); ?>user/check_password_validity",
					method : "post",
					data:{password : password},
					success:function(data){
						$('#password_result').html(data);
					}
				});
			}
		});

		$('#confirm_password').change(function(){

			var password = $('#password').val();
			var confirm_password = $('#confirm_password').val();

			if(confirm_password != '')
			{
				$.ajax({
					url:"<?php echo base_url(); ?>user/password_match",
					method : "post",
					data:{password : password , confirm_password : confirm_password},
					success:function(data){
						$('#password_match_result').html(data);
					}
				});
			}
		});
	});

</script>
</body>
</html>
