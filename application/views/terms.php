<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bidder's Arena | Terms &amp; Conditions</title>

        <!-- Fonts -->
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Josefin+Slab,Arvo,Open+Sans">
        
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-grid.css">
        
        <style>
            body {
                user-select: none;
                margin: 0;
                padding: 0;
                background: cornflowerblue;
                box-sizing: border-box;
            }
            ::-webkit-scrollbar {
                width: 0px;
                background: transparent;
            }
            .wrapper {
                background: white;
                height: 80vh;
                margin-top: 20vh;
                border-radius: 2.5rem 2.5rem 0 0;
            }
            .logo {
                position: absolute;
                font-family: "Open Sans", sans-serif;
                font-weight: bold;
                text-align: center;
                font-size: 2rem;
                top: 8vh;
                cursor: pointer;
                letter-spacing: 2px;
            }
            .logo a {
                color: white;
                text-decoration: none;
            }
            .logo a:hover {
                color: #E0E0E0;
            }
            .content {
                position: absolute;
                width: 80%;
                height: 90%;
                bottom: 0;
                left: 50%;
                overflow: scroll;
                transform: translateX(-50%);
                text-align: center;
                font-family: "Open Sans";
            }

            .content .headingTitle {
                font-size: 1.5em;
                font-weight: 700;
                text-decoration: underline;
                margin-top: 0;
            }
            .content .subHeading {
                font-size: 1.2em;
                font-weight: 600;
            }
            .content .point {
                font-size: 1em;
                text-decoration: underline;
                font-weight: 600;
            }
            .content span {
                display: inline-block;
                font-size: 0.9em;
            }
            .content #sub {
                color: red;
                font-weight: bold;
            }
            .content #sub::before,
            .content #sub::after {
                content: " --- ";
            }
        </style>
    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col col-12 col-sm-10 col-md-8 offset-sm-1 offset-md-2 logo">
                    <a href="<?php echo base_url(); ?>">Bidder's Arena</a>
                </div>
                <div class="col col-12 col-sm-10 col-md-8 offset-sm-1 offset-md-2 wrapper">
                    <div class="content">
                        <p class="headingTitle">NOTICE OF AGREEMENT</p>
                        <span id="sub">Any participation in this service will constitute acceptance of this agreement</span><br><br>

                        <p class="subHeading sh1">Privacy Policy</p>

                        <span>We have created this privacy policy in order to demonstrate our firm commitment to privacy. The following discloses the information gathering and dissemination practices for our Bidder’s Arena website.</span><br><br>

                        <span>By using the Arena’s site and accepting the User Agreement you also agree to this Privacy Policy. If you do not agree to this Privacy Policy, you must not use our site. The terms "We," "Us," "Our," includes Us  i.e. bidder’s arena member and workers</span><br><br>

                        <p class="point">What information do we collect?</p>
                        <span>Bidder’s Arena collects your information when you register on our site and when you visit our web pages.</span><br><br>

                        <span><span class="point">Personal Information:</span> We may collect the following types of personal information in order to provide you with the use and access to the Arena’s site, services and tools, and for any additional purposes set out in this Privacy Policy.</span><br><br>

                        <span>
                        a&#41; Name, and contact information, such as email address, phone number, mobile telephone number, physical address, and (depending on the service used) sometimes financial information, such as bank account numbers.<br>
                        b&#41; Transactional information based on your activities on the sites (such as bidding, buying, selling, item and content you generate or that relates to your account), billing and other information you provide to purchase an item.<br>
                        c&#41; Personal information you provide to us through correspondence, chats, dispute resolution, or shared by you from other social applications, services or websites.<br>
                        d&#41; Additional personal information we ask you to submit to authenticate yourself if we believe you are violating site policies (for example, we may ask you to send us an ID to answer additional questions online to help verify your identity).<br>
                        e&#41; Information from your interaction with our sites, services, content and advertising, including, but not limited to, device ID, device type, location, geo-location information, computer and connection information, statistics on page views, traffic to and from the sites, ad data, IP address and standard web log information.
                        </span><br><br>

                        <span><span class="point">Non-Personal Information:</span> We may collect non-personal information as you use our website. When you use the site , third-  party service providers (e.g. Google Analytics), and partners may receive and record non-personal information from cookies, server logs, and similar technology from your browser or mobile device, including your IP address.</span><br><br>

                        <span>We may combine some Non-Personal Information with the Personal Information we collect. Where we do so, we will treat the combined information as Personal Information if the resulting combination may be used to readily identify or locate you in the same manner as Personal Information alone.</span><br><br>

                        <p class="point">How we store your information?</p>
                        <span>We use hosting provider, therefore your information may be transferred to and stored on servers in various locations both in and outside of India. We maintain control of your information. We protect your information using technical and administrative security measures to reduce the risks of loss, misuse, unauthorized access, disclosure and alteration. Some of the safeguards we use are firewalls and data encryption, physical access controls to our data centers, and information access authorization controls.</span><br><br>

                        <p class="point">How we use your information?</p>
                        <span>
                        1&#41; When you use the bidder’s arena site, we may request certain information. we does not share any of your personally identifiable or transactional information with any person or entity, other than as set out in this policy. No other third party receives your personally identifiable information or other transactional data except for approximate location / geo-location information when you use the Local jobs service and those with whom you have transactions and share that information. The information we collect is used to improve the content of our web site, used to notify consumers about updates to our web site and for communications, such as customer service.<br><br>

                        2&#41; <span class="point">Email Communications:</span> We may send you a welcome email to verify your account and other transactional emails for operational purposes, such as billing, account management, or system maintenance. You may only stop those emails by terminating your account. We may also send you promotions, product announcements, surveys, newsletters, developer updates, product evaluations, and event information or other marketing or commercial e-mails. You can opt out of receiving these email communications from us at any time by unsubscribing using the unsubscribe link within each email, updating your e-mail preferences on your Arena’s account or emailing us to have your contact information removed from our email list or registration database.<br><br>

                        3&#41; <span class="point">SMS communications:</span> We may send you SMS messages to your secure phone number for operational purposes, such as notifying you about the status of your project. You can opt out of receiving these SMS messages from Bidder’s arena at any time by updating your notifications preferences on your Arena’s account or by replying 'STOP' to any message we send.<br><br>

                        4&#41; <span class="point">Marketing:</span> You agree that we may use your personal information to tell you about our services and tools, deliver targeted marketing and promotional offers based on your communication preferences, and customize measure and improve our advertising. You can unsubscribe from emails at any time by clicking the unsubscribe link contained in the email.<br><br>

                        5&#41; We do not rent, sell, or share Personal Information about you with other people or non- affiliated companies for marketing purposes (including          direct marketing purposes) without your permission.
                        </span><br><br>

                        <p class="point">Updating your Information</p>
                        <span>You can update your information through your account profile settings. If you believe that personal information we hold about you is incorrect, incomplete or inaccurate, then you may request us to amend it.</span><br><br>

                        <span>Additionally, You may request access to any personal information we hold about you at any time by contacting us. Where we hold information that you are entitled to access, we will try to provide you with suitable means of accessing it (for example, by mailing or emailing it to you). </span><br><br>

                        <span>There may be instances where we cannot grant you access to the personal information we hold. For example, we may need to refuse access if granting access would interfere with the privacy of others or if it would result in a breach of confidentiality. If that happens, we will give you written reasons for any refusal.</span><br>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>