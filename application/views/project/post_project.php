<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <meta name="description" content="">
    <title>Bidder's Arena | Post A Project</title>
    
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/web/assets/mobirise-icons/mobirise-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/dropdown/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/mobirise/css/mbr-additional.css" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Fonts -->
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Josefin+Slab,Arvo,Open+Sans">
	
    <!-- TagsInput CSS file -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/tagsinput.css" type="text/css">
    
    <!--  Custom  -->
    <style>
		body {
            user-select: none;
			background: #efefef;
		}
		.alertMsg {
			position: absolute;
			top: 0px;
			z-index: 1035;
			height: 60.8px;
			width: 100vw;
			text-align: center;
			background: #66BB6A;
			color: #FFF;
			font-size: 18px;
			line-height: 60.8px;

			animation: fadeOutTwoSec 500ms ease-out forwards 3500ms;
		}
		@keyframes fadeOutTwoSec {
			0%{
				top: 0;
			}
			100%{
				top: -65px;
			}
		}
		.mbr-iconfont {
			font-size: 1.3rem !important;
		}
		.profileIconNav {
			height: 35px;
			width: 35px;

			background: url("<?php if ($this->session->userdata('photo') == NULL) : ?><?php echo base_url(); ?>assets/Images/default-profile.jpg<?php endif; ?><?php if ($this->session->userdata('photo') != NULL) : ?>data:image/jpeg;base64,<?php echo base64_encode($this->session->userdata('photo')); ?><?php endif; ?>");

			margin-right: 10px;
			overflow: hidden;
			background-size: cover;
			background-position: center top;
		}

		.dropdown-submenu {
			left: 0 !important;
			transform: translateX(-100%) !important;
		}
		.dropdown-menu .dropdown-toggle[data-toggle="dropdown-submenu"]::after {
			left: 1.1538em !important;
			transform: rotate(180deg) !important;
		}
		@media (max-width: 991px) {
			.dropdown-submenu {
				left: 100% !important;
			}
			.dropdown-menu .dropdown-toggle[data-toggle="dropdown-submenu"]::after {
				right: 1.1538em !important;
				transform: rotate(0deg) !important;
				border-bottom: 0em solid transparent !important;
			}
		}
        
        /*  Card  */
		.wrapper {
			padding-top: 100px;
			padding-bottom: 40px;
            color: #212121;
			box-sizing: border-box;
		}
		.is-card {
			min-height: 200px;
			background: white;
			padding: 20px 30px;
            box-shadow: 0 0 4px 0 rgba(0,0,0,.08), 0 2px 4px 0 rgba(0,0,0,.12);
		}
		.is-card p {
            margin: 0;
            padding: 0;
		}
		.is-card a {
            text-decoration: none;
            outline: 0;
		}
        
        .is-card .H1 {
            font-family: "Josefin Slab", "Arvo", "Open Sans", sans-serif;
            font-size: 2.5rem;
            font-weight: bold;
            margin-bottom: 1.25rem;
        }
        .is-card .H2 {
            font-family: "Rubik", sans-serif;
            font-size: 1.5rem;
            margin-bottom: 0.5rem;
            font-weight: 500;
        }
        .is-card .H3 {
            font-family: "Open Sans", sans-serif;
            font-size: 1rem;
            margin-bottom: 0.25rem;
        }
        .is-card .underlined {
            border-bottom: 1.5px solid #0d6786;
        }
        .is-card .titleUnderline {
            display: block;
            height: 2px;
            width: 60px;
            background: #0d6786;
            border-radius: 5px;
            margin-bottom: 1.4rem;
        }
        
        .is-card form .form-control {
            padding: .5rem .75rem;
            font-size: 1rem;
            line-height: 1.25;
        }
        .is-card form .formLabel {
            font-family: "Rubik", sans-serif;
            font-weight: 500;
            font-size: 1.25rem;
        }
        .is-card form .form-text {
            font-family: "Open Sans", sans-serif;
            font-weight: bold;
            font-size: 0.8rem;
            color: #ff3366;
        }
        .is-card form .textareaStyle {
            overflow: hidden;
            resize: none;
        }
        
        /*  Button  */
        .btn {
            margin: 0;
        }
		.btn-info {
			background-color: #149dcc;
			border-color: #149dcc;
			color: #ffffff;
            font-size: 1.25rem;
            padding: 1rem 2rem;
		}
		.btn-info:hover,
		.btn-info:focus {
			background-color: #0d6786;
			border-color: #0d6786;
		}
        
        .is-card .muted-text {
            font-size: 0.95rem;
            line-height: 1.55rem;
            color: #747474;
        }
        .is-card .muted-link {
            font-size: 0.95rem;
            line-height: 1.55rem;
            color: #212121;
        }
        .is-card .muted-link:hover {
            text-decoration: underline;
            color: #2185d5;
        }
        
        .is-card .servicePack {
            padding: 15px;
        }
        .is-card .servicePack div {
            width: 100%;
            padding: 20px 15px;
            text-align: center;
            color: white;
            display: table;
            cursor: pointer;
            min-height: 150px;
            border-radius: 3px;
            box-shadow: 0 0 4px 0 rgba(0,0,0,.08), 0 2px 4px 0 rgba(0,0,0,.12);
            transition: all 0.2s ease;
        }
        .is-card .servicePack div:hover {
            transform: scale(1.05);
        }
        .is-card .servicePack div:hover > .servicePackTitle .fa {
            top: 20px;
            left: 15px;
        }
        .is-card .servicePack input {
            display: none;
        }
        .is-card .servicePackTitle {
            font-size: 1.4rem;
            font-weight: bold;
            font-family: "Josefin Slab", "Open Sans", sans-serif;
            display: table-row;
            text-transform: uppercase;
        }
        .is-card .servicePackSubTitle {
            font-size: 1.1rem;
            font-weight: 500;
            font-family: "Open Sans", sans-serif;
            display: table-row;
        }
        .is-card .servicePackDesc {
            font-size: 0.95rem;
            font-family: "Open Sans", sans-serif;
            display: table-row;
        }
        .is-card .servicePackPrice {
            font-size: 1.5rem;
            font-weight: 600;
            font-family: "Arvo", "Josefin Slab", sans-serif;
            padding-top: 1rem;
            letter-spacing: 0.1rem;
        }
        .is-card .servicePack .fa {
            position: absolute;
        }
    </style>
	
	<style>
        .is-card span.tag.label.label-info {
            background-color: #5bc0de;
        }
        .is-card span.tag.label {
            display: inline;
            padding: .3em .6em .3em;
            font-size: 75%;
            font-weight: 700;
            color: #fff;
            line-height: 26px;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: .25em;
        }
        .is-card .bootstrap-tagsinput {
            width: 100%;
            background: #f5f5f5;
        }
        .accordion {
            margin-bottom:-3px;
        }
        .is-card .accordion-group {
            border: none;
        }
        .is-card .twitter-typeahead .tt-query,
        .is-card .twitter-typeahead .tt-hint {
            margin-bottom: 0;
        }
        .is-card .twitter-typeahead .tt-hint
        {
            display: none;
        }
        .is-card .tt-menu {
            position: absolute;
            top: 100%;
            left: 0;
            z-index: 1000;
            display: none;
            float: left;
            min-width: 160px;
            padding: 5px 0;
            margin: 2px 0 0;
            list-style: none;
            font-size: 14px;
            background-color: #fff;
            border: 1px solid #cccccc;
            border: 1px solid rgba(0, 0, 0, 0.15);
            border-radius: 4px;
            -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);
            box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);
            background-clip: padding-box;
            cursor: pointer;
        }
        .is-card .tt-suggestion {
            display: block;
            padding: 3px 20px;
            clear: both;
            font-weight: normal;
            line-height: 1.428571429;
            color: #333333;
            white-space: nowrap;
        }
        .is-card .tt-suggestion:hover,
        .is-card .tt-suggestion:focus {
          color: #ffffff;
          text-decoration: none;
          outline: 0;
          background-color: #428bca;
        }
        .is-card .bootstrap-tagsinput .tag [data-role="remove"] {
          margin-left: 8px;
          cursor: pointer;
        }
        .is-card .bootstrap-tagsinput .tag [data-role="remove"]:after {
          content: "x";
          padding: 0px 2px;
        }
        .is-card .bootstrap-tagsinput .tag [data-role="remove"]:hover {
          box-shadow: none;
        }
        .is-card .bootstrap-tagsinput .tag [data-role="remove"]:hover:active {
          box-shadow: none;
        }
    </style>
    
    <style>
        .custom-file {
            width: 100%;
            height: 100px;
            border: 3px dashed #e8e8e8;
            border-radius: 0.25rem;
            cursor: pointer;
            background: #f5f5f5;
        }
        .custom-file span {
            text-align: center;
            position: absolute;
            color: #565656;
            font-family: 'Rubik', sans-serif;
            width: 100%;
            top: 50%;
            transform: translateY(-50%);
        }
        .custom-file input {
            position: absolute;
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            outline: none;
            opacity: 0;
        }
        .fileNames {
            margin-bottom: 16px;
            color: #565656;
            font-family: 'Rubik', sans-serif;
        }
    </style>

</head>

<body>
    
<?php if ($this->session->flashdata('msg')) : ?>
    <div class="alertMsg">
        <?= $this->session->flashdata('msg') ?>
    </div>
<?php endif; ?>

<?php if (validation_errors()) : ?>
    <p id="emailHelp" class="alert alert-danger" style="text-align: center !important; color: red; font-weight: bold;"><?= validation_errors(); ?></p>
<?php endif; ?>

<section class="menu cid-qH62TEfJyl" once="menu" id="menu1-e">
	<nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-toggleable-sm">

		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<div class="hamburger">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
		</button>

		<div class="menu-logo">
			<div class="navbar-brand">

                    <span class="navbar-caption-wrap">
                        <a class="navbar-caption text-white display-5" href="<?php echo base_url(); ?>">Bidder's Arena</a>
                    </span>
			</div>
		</div>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">

				<li class="nav-item"><a class="nav-link link text-white display-4" href="#"><span class="mbri-cash mbr-iconfont mbr-iconfont-btn"></span>My Wallet<br></a></li>

				<li class="nav-item"><a class="nav-link link text-white display-4" href="#"><span class="mbri-bookmark mbr-iconfont mbr-iconfont-btn"></span>My Projects<br></a></li>

				<li class="nav-item"><a class="nav-link link text-white display-4" href="<?php echo base_url();?>dashboard"><span class="mbri-sites mbr-iconfont mbr-iconfont-btn"></span>Dashboard<br></a></li>

				<li class="nav-item dropdown">
					<a class="nav-link link dropdown-toggle text-white display-4" href="#" data-toggle="dropdown-submenu" aria-expanded="false">
						<span class="profileIconNav"></span><?= $this->session->userdata('username') ?><br></a>

					<div class="dropdown-menu">

						<div class="dropdown">
							<a class="dropdown-item text-white dropdown-toggle display-4" data-toggle="dropdown-submenu" aria-expanded="false">Help</a>

							<div class="dropdown-menu dropdown-submenu">
								<a class="dropdown-item text-white display-4" href="#" aria-expanded="false">FAQ</a>

								<a class="dropdown-item text-white display-4" href="#">Contact Us</a>

								<a class="dropdown-item text-white display-4" href="#" aria-expanded="false">Our Charges</a>
							</div>
						</div>

						<a class="dropdown-item text-white display-4" href="<?php echo base_url();?>profile" aria-expanded="false">My Profile</a>

						<a class="dropdown-item text-white display-4" href="<?php echo base_url();?>logout" aria-expanded="false">Logout</a>

					</div>
				</li>
			</ul>
		</div>
	</nav>
</section>

<div class="container wrapper">
	<div class="row">

		<div class="col col-12 is-card">
            
            <p class="H1 underlined">Post a Project</p>

            <p class="H2">Tell us what you need to get done</p>

            <p class="H3">Get free quotes from skilled freelancers within minutes, view profiles, ratings and portfolios and chat with them. Pay the freelancer only when you are 100% satisfied with their work.</p>
            
            <form action="<?php echo base_url();?>project/post" method="post" class="mt-5" onsubmit="return verifyBudget()" enctype="multipart/form-data">
               
                <div class="form-group">
                    <label class="formLabel" for="projectName">Give a title to your project</label>
                    
                    <input type="text" name="projectName" class="form-control" id="projectName" placeholder="eg. Want an android app" required>
                    
                    <small id="projectName_result" class="form-text"></small>
                </div>

                <div class="form-group">
                    <label class="formLabel" for="description">Tell us about your project</label>
                    
                    <textarea class="form-control textareaStyle" name="description" placeholder="Describe your project" rows="5" id="description" maxlength="500" minlength="100" required></textarea>
                    
                    <small id="description_result" class="form-text"></small>
                </div>
               
                <div class="form-group">
                    <label class="formLabel" for="sampleLink">Link for a sample (if any)</label>
                    
                    <input type="text" name="sampleLink" class="form-control" id="sampleLink" placeholder="www.yourSampleLink.com">
                    
                    <small id="sampleLink_result" class="form-text"></small>
                </div>

                <label class="formLabel">Share files for better understanding</label>
                <label class="custom-file">
                    <input type="file" name="files[]" id="file" multiple accept="file_extension|.pdf|.jpg|.jpeg">
                    
                    <span>Drag your files here or click in this area. Select all files at once.</span>
                </label>
                <label class="fileNames">*Select all files at once</label>

                <div class="form-group">
                    <label class="formLabel" for="category">Project category</label>
                    
                    <select name="category" class="form-control" id="category" required>
                        <option id="c1" value='Website, IT, Software'>Website, IT, Software</option>
                        <option id="c2" value='Mobile Phones'>Mobile Phones</option>
                        <option id="c3" value='Writing and Content'>Writing and Content</option>
                        <option id="c4" value='Design, Media and Architecture'>Design, Media and Architecture</option>
                        <option id="c5" value='Data Entry and Admin'>Data Entry and Admin</option>
                        <option id="c6" value='Engineering and Science'>Engineering and Science</option>
                        <option id="c7" value='Sales and Marketing'>Sales and Marketing</option>
                        <option id="c8" value='Business, Accounting, Human Resources &amp; Legal'>Business, Accounting, Human Resources &amp; Legal</option>
                        <option id="c9" value='Translation'>Translation</option>
                    </select>
                    
                    <small id="category_result" class="form-text"></small>
                </div>
                
                <div class="form-group">
                    <label class="formLabel" for="skills">Skills required</label>
                    
                    <div class="skill1 skills">
                        <input type="text" class="form-control" id="skills" placeholder="Enter the skills required" data-role="tagsinput">
                    </div>
                    
                    <div class="skill2 skills">
                        <input type="text" class="form-control" id="skills2" placeholder="Enter the skills required" data-role="tagsinput">
                    </div>
                    
                    <div class="skill3 skills">
                        <input type="text" class="form-control" id="skills3" placeholder="Enter the skills required" data-role="tagsinput">
                    </div>
                    
                    <div class="skill4 skills">
                        <input type="text" class="form-control" id="skills4" placeholder="Enter the skills required" data-role="tagsinput">
                    </div>
                    
                    <div class="skill5 skills">
                        <input type="text" class="form-control" id="skills5" placeholder="Enter the skills required" data-role="tagsinput">
                    </div>
                    
                    <div class="skill6 skills">
                        <input type="text" class="form-control" id="skills6" placeholder="Enter the skills required" data-role="tagsinput">
                    </div>
                    
                    <div class="skill7 skills">
                        <input type="text" class="form-control" id="skills7" placeholder="Enter the skills required" data-role="tagsinput">
                    </div>
                    
                    <div class="skill8 skills">
                        <input type="text" class="form-control" id="skills8" placeholder="Enter the skills required" data-role="tagsinput">
                    </div>
                    
                    <div class="skill9 skills">
                        <input type="text" class="form-control" id="skills9" placeholder="Enter the skills required" data-role="tagsinput">
                    </div>
                    
                    <small id="skills_result" class="form-text"></small>
                </div>
                
                <div class="row">
                    <div class="form-group col-12 col-sm-6">
                        <label class="formLabel" for="budgetRange">Estimated budget range</label>

                        <select name="budgetRange" id="budgetRange" class="form-control" required>
                             <option value="500-1500">Micro Projects (&#8377; 500 - &#8377; 1500)</option>
                            <option value="1500-5000">Mini Projects (&#8377; 1500 - &#8377; 5000)</option>
                            <option value="5000-15000">Small Projects (&#8377; 5000 - &#8377; 15000)</option>
                            <option value="15000-75000">Medium Projects (&#8377; 15000 - &#8377; 75000)</option>
                            <option value="75000-15000">Large Projects (&#8377; 75000 - &#8377; 150000)</option>
                        </select>
                    </div>
                    
                    <div class="form-group col-12 col-sm-6">
                        <label class="formLabel">Your budget</label>

                        <div class="input-group">
                            <input type="number" name="budget" class="form-control" id="budget" placeholder="Your budget" required min="500" max="1500">
                        </div>
                    </div>
                </div>
                
                <label class="formLabel">Additional Service Packs</label>
                
                <div class="row" style="border: 3px dashed #e8e8e8; margin-bottom: 1rem;">
                   
                    <div class="col-12 col-sm-6 col-lg-3 servicePack">
                        <div style="background: #F57F17;" id="Recruiter" onclick="checkboxClick(this.id);">
                            <input type="checkbox" name="addons[]" value="Recruiter">
                            <p class="servicePackTitle"><i class="fa fa-check"></i>Helper</p>
                            
                            <p class="servicePackDesc">Confused in selecting the best freelancer? Leave the burden to us. We will choose the best freelancer for you.</p>
                            
                            <p class="servicePackPrice">&#8377; 399/-</p>
                        </div>
                    </div>
                    
                    <div class="col-12 col-sm-6 col-lg-3 servicePack">
                        <div style="background: #2185d5;" id="Featured" onclick="checkboxClick(this.id);">
                            <input type="checkbox" name="addons[]" value="Featured">
                            <p class="servicePackTitle"><i class="fa fa-check"></i>Highlighted</p>
                            
                            <p class="servicePackDesc">Attract the best freelancer to your project by taking our highlighted feature.</p>
                            
                            <p class="servicePackPrice">&#8377; 499/-</p>
                        </div>
                    </div>
                    
                    <div class="col-12 col-sm-6 col-lg-3 servicePack">
                        <div style="background: #009688;" id="NDA" onclick="checkboxClick(this.id);">
                            <input type="checkbox" name="addons[]" value="NDA">
                            <p class="servicePackTitle"><i class="fa fa-check"></i>NDA</p>
                            
                            <p class="servicePackDesc">The details of your project will not be shared with anyone whatsoever</p>
                            
                            <p class="servicePackPrice">&#8377; 399/-</p>
                        </div>
                    </div>
                    
                    <div class="col-12 col-sm-6 col-lg-3 servicePack">
                        <div style="background: #F44336;" id="Urgent" onclick="checkboxClick(this.id);">
                            <input type="checkbox" name="addons[]" value="Urgent">
                            <p class="servicePackTitle"><i class="fa fa-check"></i>Urgent</p>
                            
                            <p class="servicePackDesc">Get your project completed as soon as you want.</p>
                            
                            <p class="servicePackPrice">&#8377; 399/-</p>
                        </div>
                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="form-group deadline col-12 col-md-6 col-lg-3">
                        <label class="formLabel" for="deadline">Deadline date</label>

                        <input type="date" class="form-control" name="deadline" placeholder="Enter your project deadline (dd/mm/yyyy)" id="deadline">

                        <small id="deadline_result" class="form-text"></small>
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" name="postproject" class="btn btn-info">Post Your Project</button>
                    
                    <span class="H2 float-right" id="total" style="height: 60.89px; line-height: 60.89px;">Total : &#8377; 0</span>
                </div>
                
            </form>
            
            <hr style="border-top-width: 1.5px; margin-top: 4rem;">
            
            <p class="muted-text">By clicking 'Post Your Project', you agree to the <a href="<?php echo base_url(); ?>terms" class="muted-link">Terms &amp; Conditions</a> and <a href="<?php echo base_url(); ?>privacy" class="muted-link">Privacy Policy</a></p>
            
            <p class="muted-text mt-5">Copyright © 2018 Bidder's Arena</p>
            
            <p class="muted-text">Designed &amp; developed by <a href="#" class="muted-link">Chamber of Developers</a></p>
            
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/assets/web/assets/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/assets/dropdown/js/script.min.js"></script>

<script src="<?php echo base_url();?>assets/js/tagsinput.js"></script>
<script src="<?php echo base_url();?>assets/js/typeahead.bundle.js"></script>

<script id="typeahead">
	// ------------- Shows autocomplete and suggestions for skills ----------- //
    var skills = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      prefetch: {
        url: '<?php echo base_url(); ?>/assets/JSON/category1.json',
        filter: function(list) {
          return $.map(list, function(cityname) {
            return { name: cityname }; });
        }
      }
    });
    skills.initialize();
    
    var skills2 = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      prefetch: {
        url: '<?php echo base_url(); ?>/assets/JSON/category2.json',
        filter: function(list) {
          return $.map(list, function(cityname) {
            return { name: cityname }; });
        }
      }
    });
    skills2.initialize();
    
    var skills3 = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      prefetch: {
        url: '<?php echo base_url(); ?>/assets/JSON/category3.json',
        filter: function(list) {
          return $.map(list, function(cityname) {
            return { name: cityname }; });
        }
      }
    });
    skills3.initialize();
    
    var skills4 = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      prefetch: {
        url: '<?php echo base_url(); ?>/assets/JSON/category4.json',
        filter: function(list) {
          return $.map(list, function(cityname) {
            return { name: cityname }; });
        }
      }
    });
    skills4.initialize();
    
    var skills5 = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      prefetch: {
        url: '<?php echo base_url(); ?>/assets/JSON/category5.json',
        filter: function(list) {
          return $.map(list, function(cityname) {
            return { name: cityname }; });
        }
      }
    });
    skills5.initialize();
    
    var skills6 = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      prefetch: {
        url: '<?php echo base_url(); ?>/assets/JSON/category6.json',
        filter: function(list) {
          return $.map(list, function(cityname) {
            return { name: cityname }; });
        }
      }
    });
    skills6.initialize();
    
    var skills7 = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      prefetch: {
        url: '<?php echo base_url(); ?>/assets/JSON/category7.json',
        filter: function(list) {
          return $.map(list, function(cityname) {
            return { name: cityname }; });
        }
      }
    });
    skills7.initialize();
    
    var skills8 = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      prefetch: {
        url: '<?php echo base_url(); ?>/assets/JSON/category8.json',
        filter: function(list) {
          return $.map(list, function(cityname) {
            return { name: cityname }; });
        }
      }
    });
    skills8.initialize();
    
    var skills9 = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      prefetch: {
        url: '<?php echo base_url(); ?>/assets/JSON/category9.json',
        filter: function(list) {
          return $.map(list, function(cityname) {
            return { name: cityname }; });
        }
      }
    });
    skills9.initialize();
</script>

<script>

    $(document).ready(function () {
        $(".is-card .servicePack .fa").hide();
        $(".is-card .deadline").hide();
        $(".is-card .form-group .skills").hide();
        $(".is-card .form-group .skills input").prop('required', false);
        $(".is-card .form-group .skill1").show();
        $(".is-card .form-group #skills").prop('required', true);
        $(".is-card .form-group #skills").attr('name', 'skillsRequired');
        
        $('#projectName').bind('input', function () {
            var projectName = $('#projectName').val();
            if (projectName == '') {
                $('#projectName_result').html("Please enter at least 10 characters.");
                $('#projectName_result').css("color", "red");
                $('#projectName').css("border-color", "red");
            } else {
                $.ajax({
                    url: "<?php echo base_url(); ?>project/projectName_validation",
                    method: "post",
                    data: {projectName: projectName},
                    success: function (data) {
                        if (data == "true") {
                            $('#projectName_result').html("You're good to go!");
                            $('#projectName_result').css("color", "green");
                            $('#projectName').css("border-color", "green");
                        } else {
                            $('#projectName_result').html(data);
                            $('#projectName_result').css("color", "red");
                            $('#projectName').css("border-color", "red");
                        }
                    }
                });
            }
        });

        $('#description').bind('input', function () {
            var description = $('#description').val();
            if (description == '') {
                $('#description_result').html("Please enter at least 100 characters.");
                $('#description_result').css("color", "red");
                $('#description').css("border-color", "red");
            } else {
                $.ajax({
                    url: "<?php echo base_url(); ?>project/description_validation",
                    method: "post",
                    data: {description: description},
                    success: function (data) {
                        if (data == "true") {
                            $('#description_result').html("You're good to go!");
                            $('#description_result').css("color", "green");
                            $('#description').css("border-color", "green");
                        } else {
                            $('#description_result').html(data);
                            $('#description_result').css("color", "red");
                            $('#description').css("border-color", "red");
                        }
                    }
                });
            }
        });

        $('#skills').bind('input', function () {
            var skills = $('#skills').val();
            if (skills == '') {
                $('#skills_result').html("Please select at least 1 skill.");
                $('#skills_result').css("color", "red");
                $('#skills').css("border-color", "red");
            } else {
                $.ajax({
                    url: "<?php echo base_url(); ?>project/skills_validation",
                    method: "post",
                    data: {skillsRequired: skills},
                    success: function (data) {
                        if (data == "true") {
                            $('#skills_result').html("You're good to go!");
                            $('#skills_result').css("color", "green");
                            $('#skills').css("border-color", "green");
                        } else {
                            $('#skills_result').html(data);
                            $('#skills_result').css("color", "red");
                            $('#skills').css("border-color", "red");
                        }
                    }
                });
            }
        });

        $('#budget').bind('input', function () {
            var projectName = $('#projectName').val();
            if (projectName == '') {
                $('#budget_result').html(data);
                $('#budget_result').css("color", "red");
                $('#budget').css("border-color", "red");
            } else {
                $.ajax({
                    url: "<?php echo base_url(); ?>project/budget_validation",
                    method: "post",
                    data: {projectName: projectName},
                    success: function (data) {
                        if (data == "true") {
                            $('#budget_result').html("You're good to go!");
                            $('#budget_result').css("color", "green");
                            $('#budget').css("border-color", "green");
                        } else {
                            $('#budget_result').html(data);
                            $('#budget_result').css("color", "red");
                            $('#budget').css("border-color", "red");
                        }
                    }
                });
            }
        });

        // ------------- Fpr select files field ----------- //
        $('.custom-file input').change(function () {
            console.log(this.files);
            if(this.files.length <= 5) {
                $('.custom-file span').html(this.files.length + " file(s) selected");
            }
            else {
                $('.custom-file span').html("Maximum 5 files can be selected!");
            }
            
            var fileNames = "";
            for (var i=0; i < parseInt(this.files.length); i++) {
                if( i+1 != parseInt(this.files.length) )
                    fileNames = fileNames + this.files[i].name + '<br>';
                else
                    fileNames = fileNames + this.files[i].name;
            }
            $(".fileNames").html(fileNames);
        });
    });
    
    // ------------- Selects the appropriate json for skill ----------- //
    var catVal = "c1";
    $("#category").on('change', function() {
        catVal = $(this).children(":selected").attr('id');
        catVal = catVal.substring(1);
        
        $(".is-card .form-group .skills").hide();
        $(".is-card .form-group .skills input").attr('value', "");
        $(".is-card .form-group .skills input").prop('required', false);
        $(".is-card .form-group .skills input").removeAttr('name');
        $(".is-card .form-group .skill" + catVal).show();
        $(".is-card .form-group #skills" + catVal).prop('required', true);
        $(".is-card .form-group #skills" + catVal).attr('name', 'skillsRequired');
    });
    
    // ------------- Applies min/max on budget ----------- //
    var min = "500";
    var max = "1500";
    $("#budgetRange").on("change", function() {
        var budgetRange = $(this).children(":selected").val();
        min = budgetRange.substring(0, budgetRange.indexOf('-'));
        max = budgetRange.substring(budgetRange.indexOf('-')+1);
        
        $("#budget").attr('min', min);
        $("#budget").attr('max', max);
    });
    
    // ------------- Suspends form from being submitted if budget error exists ----------- //
    function verifyBudget() {
        if( (parseInt($("#budget").val()) < parseInt(min)) ||(parseInt($("#budget").val()) > parseInt(max)) ) {
            alert("Budget should be within the selected range!");
            return false;
        }
        return true;
    }
    
	// ------------- Update project service price ----------- //
    function checkboxClick(id) {
        var price = 0;
        if( $('.is-card .servicePack div input[type=checkbox][value=' + id + ']').prop('checked') == false) {
            $('.is-card .servicePack div input[type=checkbox][value=' + id + ']').prop('checked', true);
            
            $('.is-card .servicePack #'+id+' .fa').fadeIn(200);
        }
        else {
            $('.is-card .servicePack div input[type=checkbox][value=' + id + ']').prop('checked', false);
            
            $('.is-card .servicePack #'+id+' .fa').fadeOut(200);
        }
        
        $.each($(".is-card .servicePack div input:checkbox:checked"), function() {
            if( $(this).val() == 'Featured' )
                price += 499;
            else
                price += 399;
        });
        $("#total").html('Total: &#8377; ' + price);
        
        if( $(".servicePack #Urgent input").prop("checked") ) {
            $(".is-card .deadline").fadeIn(200);
            $(".is-card .deadline").prop('required', true);
        } else {
            $(".is-card .deadline").fadeOut(200);
            $(".is-card .deadline").prop('required', false);
        }
    }
</script>

<script id="keyScr">
    $('#skills').tagsinput({
        maxTags: 8,
        typeaheadjs: {
            name: 'skills',
            limit: 10,
            displayKey: 'name',
            valueKey: 'name',
            source: skills.ttAdapter()
      }
    });
    $('#skills2').tagsinput({
        maxTags: 8,
        typeaheadjs: {
            name: 'skills',
            limit: 10,
            displayKey: 'name',
            valueKey: 'name',
            source: skills2.ttAdapter()
      }
    });
    $('#skills3').tagsinput({
        maxTags: 8,
        typeaheadjs: {
            name: 'skills',
            limit: 10,
            displayKey: 'name',
            valueKey: 'name',
            source: skills3.ttAdapter()
      }
    });
    $('#skills4').tagsinput({
        maxTags: 8,
        typeaheadjs: {
            name: 'skills',
            limit: 10,
            displayKey: 'name',
            valueKey: 'name',
            source: skills4.ttAdapter()
      }
    });
    $('#skills5').tagsinput({
        maxTags: 8,
        typeaheadjs: {
            name: 'skills',
            limit: 10,
            displayKey: 'name',
            valueKey: 'name',
            source: skills5.ttAdapter()
      }
    });
    $('#skills6').tagsinput({
        maxTags: 8,
        typeaheadjs: {
            name: 'skills',
            limit: 10,
            displayKey: 'name',
            valueKey: 'name',
            source: skills6.ttAdapter()
      }
    });
    $('#skills7').tagsinput({
        maxTags: 8,
        typeaheadjs: {
            name: 'skills',
            limit: 10,
            displayKey: 'name',
            valueKey: 'name',
            source: skills7.ttAdapter()
      }
    });
    $('#skills8').tagsinput({
        maxTags: 8,
        typeaheadjs: {
            name: 'skills',
            limit: 10,
            displayKey: 'name',
            valueKey: 'name',
            source: skills8.ttAdapter()
      }
    });
    $('#skills9').tagsinput({
        maxTags: 8,
        typeaheadjs: {
            name: 'skills',
            limit: 10,
            displayKey: 'name',
            valueKey: 'name',
            source: skills9.ttAdapter()
      }
    });
</script>

</body>

</html>
