<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <meta name="description" content="">
    <title>Bidder's Arena | Dashboard</title>
    
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/web/assets/mobirise-icons/mobirise-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/dropdown/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/mobirise/css/mbr-additional.css" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- Fonts -->
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Josefin+Slab,Arvo,Open+Sans">
    
    <!--  Loading Overlay  -->
    <style>
        .loadingOverlay {
            height: 100vh;
            width: 100vw;
            position: fixed;
            z-index: 99999;
            background: #6997DB;
        }
        #loader {
            position: absolute;
            top: calc(50% - 20px);
            left: calc(50% - 20px);
        }
        #box {
            width: 50px;
            height: 50px;
            background: #fff;
            animation: animate .5s linear infinite;
            position: absolute;
            top: 0;
            left: 0;
            border-radius: 3px;
        }
        @keyframes animate {
            17% {
                border-bottom-right-radius: 3px;
            }
            25% {
                transform: translateY(9px) rotate(22.5deg);
            }
            50% {
                transform: translateY(18px) scale(1,.9) rotate(45deg) ;
                border-bottom-right-radius: 40px;
            }
            75% {
                transform: translateY(9px) rotate(67.5deg);
            }
            100% {
                transform: translateY(0) rotate(90deg);
            }
        } 
        #shadow { 
            width: 50px;
            height: 5px;
            background: #000;
            opacity: 0.1;
            position: absolute;
            top: 59px;
            left: 0;
            border-radius: 50%;
            animation: shadow .5s linear infinite;
        }
        @keyframes shadow {
            50% {
                transform: scale(1.2, 1);
            }
        }
        #loading { 
            position: absolute;
            top: 80px;
            left: -12px;
            color: white;
            font-family: "Open Sans", sans-serif;
            letter-spacing: 0.1rem;
            animation: textAnimation 1.5s ease-in-out infinite;
        }
        @keyframes textAnimation {
            0%, 100% {
                opacity: 1;
            }
            50% {
                opacity: 0.5;
            }
        }
    </style>
    
    <!--  Custom  -->
    <style>
		body {
            user-select: none;
			background: #efefef;
		}
		.alertMsg {
			position: absolute;
			top: 0px;
			z-index: 1035;
			height: 60.8px;
			width: 100vw;
			text-align: center;
			background: #66BB6A;
			color: #FFF;
			font-size: 18px;
			line-height: 60.8px;

			animation: fadeOutTwoSec 500ms ease-out forwards 3500ms;
		}
        
		@keyframes fadeOutTwoSec {
			0%{
				top: 0;
			}
			100%{
				top: -65px;
			}
		}
		.mbr-iconfont {
			font-size: 1.3rem !important;
		}
		.profileIconNav {
			height: 35px;
			width: 35px;

			background: url("<?php if ($this->session->userdata('photo') == NULL) : ?><?php echo base_url(); ?>assets/Images/default-profile.jpg<?php endif; ?><?php if ($this->session->userdata('photo') != NULL) : ?>data:image/jpeg;base64,<?php echo base64_encode($this->session->userdata('photo')); ?><?php endif; ?>");

			margin-right: 10px;
			overflow: hidden;
			background-size: cover;
			background-position: center top;
		}

		.dropdown-submenu {
			left: 0 !important;
			transform: translateX(-100%) !important;
		}
		.dropdown-menu .dropdown-toggle[data-toggle="dropdown-submenu"]::after {
			left: 1.1538em !important;
			transform: rotate(180deg) !important;
		}
		@media (max-width: 991px) {
			.dropdown-submenu {
				left: 100% !important;
			}
			.dropdown-menu .dropdown-toggle[data-toggle="dropdown-submenu"]::after {
				right: 1.1538em !important;
				transform: rotate(0deg) !important;
				border-bottom: 0em solid transparent !important;
			}
		}
        
        /*  Card  */
		.wrapper {
			padding-top: 100px;
			padding-bottom: 40px;
            color: #212121;
			box-sizing: border-box;
		}
        @media (max-width: 768px){
            .is-card {
                box-shadow: 0px 0px 20px -5px rgba(0, 0, 0, 0) !important;
            }
            .wrapper {
                padding-top: 61px;
                padding-bottom: 0px;
            }
            body {
                background: white;
            }
        }
        @media (min-width: 768px){
            .is-card {
                box-shadow: 0 0 4px 0 rgba(0,0,0,.08), 0 2px 4px 0 rgba(0,0,0,.12) !important;
            }
            .wrapper {
                padding-top: 100px;
                padding-bottom: 40px;
            }
            body {
                background: #efefef;
            }
        }
		.is-card {
			min-height: 200px;
			background: white;
			padding: 20px 30px;
            box-shadow: 0 0 4px 0 rgba(0,0,0,.08), 0 2px 4px 0 rgba(0,0,0,.12);
		}
		.is-card p {
            margin: 0;
            padding: 0;
		}
		.is-card a {
            text-decoration: none;
            outline: 0;
		}
        
        .is-card .H1 {
            font-family: "Josefin Slab", "Arvo", "Open Sans", sans-serif;
            font-size: 2.5rem;
            font-weight: bold;
            margin-bottom: 1.25rem;
            color: #424242;
        }
        .is-card .H2 {
            font-family: "Rubik", sans-serif;
            font-size: 1.5rem;
            margin-bottom: 0.5rem;
            font-weight: 500;
        }
        .is-card .H3 {
            font-family: "Open Sans", sans-serif;
            font-size: 1rem;
            margin-bottom: 0.25rem;
        }
        .is-card .text-hint {
            font-size: 0.9rem;
            color: #616161;
            padding: 18px 0;
        }
        
        /*  Projects  */
        .is-card .project {
            background: #F5F5F5;
            padding: 15px 0px;
            border-radius: 3px;
            border: 1.5px solid #E0E0E0;
        }
        .is-card .project .projectTitle {
            font-family: "Josefin Slab", "Arvo", monospace;
            font-weight: bold;
            font-size: 1.5rem;
            color: #424242;
            word-wrap: break-word;
        }
        .is-card .project .projectDesc {
            font-family: "Open Sans", sans-serif;
            font-size: 0.95rem;
            color: #616161;
            word-wrap: break-word;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-box-orient: vertical;
            -webkit-line-clamp: 4;
            line-height: 1.3rem;
            max-height: 5.2rem;
            overflow: hidden;
        }
        .is-card .project .projectSubTitle {
            text-transform: uppercase;
            font-weight: bold;
            color: #616161;
        }
        .is-card .project .projectSubTitle {
            text-transform: uppercase;
            font-weight: 600;
            letter-spacing: 0.1rem;
            color: #616161;
            font-family: "Arvo", monospace;
        }
        @media (min-width: 768px){
            .is-card .project .bid .btn {
                position: absolute;
                top: 50%;
                transform: translateY(-50%);
                right: 20px;
            }
        }
    </style>
	
    <style>
        .stick {
            margin-top: 0 !important;
            position: fixed;
            top: 0;
            z-index: 10000;
            padding-top: 15px !important;
        }
        #sticky {
            margin-left: -10px;
            padding: 0px 10px 15px;
            background: white;
        }
    </style>
    
    <!--  Buttons  -->
    <style>
        .btn {
			padding: 10px 10px;
			width: 180px;
			max-width: 80%;
            margin: 0;
        }
        .btn-warning {
			background-color: rgb(247, 237, 74);
			color: rgb(63, 60, 3);
			border-color: rgb(247, 237, 74);
		}
		.btn-warning:hover,
		.btn-warning:focus {
			background-color: rgb(234, 221, 10);
			border-color: rgb(234, 221, 10);
			color: rgb(63, 60, 3);
		}
		.btn-danger {
			background-color: #ff3366;
			border-color: #ff3366;
			color: #ffffff;
		}
		.btn-danger:hover,
		.btn-danger:focus {
			background-color: #e50039;
			border-color: #e50039;
		}
		.btn-info {
			background-color: #149dcc;
			border-color: #149dcc;
			color: #ffffff;
		}
		.btn-info:hover,
		.btn-info:focus {
			background-color: #0d6786;
			border-color: #0d6786;
		}
        .btn a {
            text-decoration: none;
            color: white;
        }
        .btn-warning a {
			color: rgb(63, 60, 3);
        }
    </style>

</head>

<body>
   
<!--  Loading Overlay  -->
<div class="loadingOverlay">
    <div id="loader">
        <div id="shadow"></div>
        <div id="box"></div>
        <div id="loading">Loading...</div>
    </div>
</div>
    
<?php if ($this->session->flashdata('msg')) : ?>
    <div class="alertMsg">
        <?= $this->session->flashdata('msg') ?>
    </div>
<?php endif; ?>

<section class="menu cid-qH62TEfJyl" once="menu" id="menu1-e">
	<nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-toggleable-sm">

		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<div class="hamburger">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
		</button>

		<div class="menu-logo">
			<div class="navbar-brand">

                    <span class="navbar-caption-wrap">
                        <a class="navbar-caption text-white display-5" href="<?php echo base_url(); ?>">Bidder's Arena</a>
                    </span>
			</div>
		</div>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">

				<li class="nav-item"><a class="nav-link link text-white display-4" href="#"><span class="mbri-cash mbr-iconfont mbr-iconfont-btn"></span>My Wallet<br></a></li>

				<li class="nav-item"><a class="nav-link link text-white display-4" href="<?php echo base_url();?>dashboard"><span class="mbri-sites mbr-iconfont mbr-iconfont-btn"></span>Dashboard<br></a></li>

				<li class="nav-item"><a class="nav-link link text-white display-4" href="<?php echo base_url();?>profile"><span class="mbri-user mbr-iconfont mbr-iconfont-btn"></span>My Profile<br></a></li>

				<li class="nav-item dropdown">
					<a class="nav-link link dropdown-toggle text-white display-4" href="#" data-toggle="dropdown-submenu" aria-expanded="false">
						<span class="profileIconNav"></span><?= $this->session->userdata('username') ?><br></a>

					<div class="dropdown-menu">

						<div class="dropdown">
							<a class="dropdown-item text-white dropdown-toggle display-4" data-toggle="dropdown-submenu" aria-expanded="false">Help</a>

							<div class="dropdown-menu dropdown-submenu">
								<a class="dropdown-item text-white display-4" href="#" aria-expanded="false">FAQ</a>

								<a class="dropdown-item text-white display-4" href="#">Contact Us</a>

								<a class="dropdown-item text-white display-4" href="#" aria-expanded="false">Our Charges</a>
							</div>
						</div>

<!--						<a class="dropdown-item text-white display-4" href="<?php echo base_url();?>profile" aria-expanded="false">My Profile</a>-->

						<a class="dropdown-item text-white display-4" href="<?php echo base_url();?>logout" aria-expanded="false">Logout</a>

					</div>
				</li>
			</ul>
		</div>
	</nav>
</section>

<div class="container wrapper">
	<div class="row">
        <div class="col col-12 is-card">
            <div id="sticky-anchor"></div>
            
            <div id="sticky">
                <span class="H1">Your Projects</span>
            </div>
            
            <div id="projectSection" class="row projectWrapper pr-3 pl-3">
                
            </div>
            
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/assets/web/assets/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/assets/dropdown/js/script.min.js"></script>

<script>
	$(document).ready(function () {
		$.ajax ({
			url: "<?php echo base_url(); ?>project/main_myProjects",
			method: "post",
			success: function (data)
			{
				$("#projectSection").html("");
				$("#projectSection").html(data);
                
                $(".loadingOverlay #loader").fadeOut(500);
                $(".loadingOverlay").fadeOut(1000);
			}
		});
	});
    function sticky_relocate() {
        var window_top = $(window).scrollTop();
        var div_top = $('#sticky-anchor').offset().top;
        if (window_top > div_top) {
            $('#sticky').addClass('stick');
            $('#sticky-anchor').height($('#sticky').outerHeight());
        } else {
            $('#sticky').removeClass('stick');
            $('#sticky-anchor').height(0);
        }
    }
    $(function() {
        $(window).scroll(sticky_relocate);
        sticky_relocate();
        
        $(window).resize(function() {
            $('#sticky').width($(".is-card").width());
        });
        $(document).ready(function() {
            $('#sticky').width($(".is-card").width());
        });
    });
</script>

</body>

</html>
