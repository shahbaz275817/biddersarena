<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <meta name="description" content="">
    <title>Bidder's Arena | Project Description</title>
    
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/web/assets/mobirise-icons/mobirise-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/dropdown/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/mobirise/css/mbr-additional.css" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- Fonts -->
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Josefin+Slab,Arvo,Open+Sans">
    
    <!--  Loading Overlay  -->
    <style>
        .loadingOverlay {
            height: 100vh;
            width: 100vw;
            position: fixed;
            z-index: 99999;
            background: #6997DB;
        }
        #loader {
            position: absolute;
            top: calc(50% - 20px);
            left: calc(50% - 20px);
        }
        #box {
            width: 50px;
            height: 50px;
            background: #fff;
            animation: animate .5s linear infinite;
            position: absolute;
            top: 0;
            left: 0;
            border-radius: 3px;
        }
        @keyframes animate {
            17% {
                border-bottom-right-radius: 3px;
            }
            25% {
                transform: translateY(9px) rotate(22.5deg);
            }
            50% {
                transform: translateY(18px) scale(1,.9) rotate(45deg) ;
                border-bottom-right-radius: 40px;
            }
            75% {
                transform: translateY(9px) rotate(67.5deg);
            }
            100% {
                transform: translateY(0) rotate(90deg);
            }
        } 
        #shadow { 
            width: 50px;
            height: 5px;
            background: #000;
            opacity: 0.1;
            position: absolute;
            top: 59px;
            left: 0;
            border-radius: 50%;
            animation: shadow .5s linear infinite;
        }
        @keyframes shadow {
            50% {
                transform: scale(1.2,1);
            }
        }
        #loading { 
            position: absolute;
            top: 80px;
            left: -12px;
            color: white;
            font-family: "Open Sans", sans-serif;
            letter-spacing: 0.1rem;
        }
    </style>
    
    <!--  Custom  -->
    <style>
		body {
            user-select: none;
			background: #efefef;
		}
		.alertMsg {
			position: absolute;
			top: 0px;
			z-index: 1035;
			height: 60.8px;
			width: 100vw;
			text-align: center;
			background: #66BB6A;
			color: #FFF;
			font-size: 18px;
			line-height: 60.8px;
			animation: fadeOutTwoSec 500ms ease-out forwards 3500ms;
		}
		.yoloMsg {
			position: relative;
			top: 60.8px;
			z-index: 1035;
			height: 60.8px;
			width: 100vw;
			text-align: center;
			color: #FFF;
			font-size: 18px;
			line-height: 60.8px;
            border: 1.5px solid #c0392b;
		}
        .yoloMsg .alert-warning {
            background: #e74c3c !important;
        }
        .yoloMsg .alert-danger {
            background: #c0392b !important;
        }
        .yoloMsg .alert-link {
            color: #ecf0f1;
            text-transform: uppercase;
        }
        
		@keyframes fadeOutTwoSec {
			0%{
				top: 0;
			}
			100%{
				top: -65px;
			}
		}
		.mbr-iconfont {
			font-size: 1.3rem !important;
		}
		.profileIconNav {
			height: 35px;
			width: 35px;

			background: url("<?php if ($this->session->userdata('photo') == NULL) : ?><?php echo base_url(); ?>assets/Images/default-profile.jpg<?php endif; ?><?php if ($this->session->userdata('photo') != NULL) : ?>data:image/jpeg;base64,<?php echo base64_encode($this->session->userdata('photo')); ?><?php endif; ?>");

			margin-right: 10px;
			overflow: hidden;
			background-size: cover;
			background-position: center top;
		}

		.dropdown-submenu {
			left: 0 !important;
			transform: translateX(-100%) !important;
		}
		.dropdown-menu .dropdown-toggle[data-toggle="dropdown-submenu"]::after {
			left: 1.1538em !important;
			transform: rotate(180deg) !important;
		}
		@media (max-width: 991px) {
			.dropdown-submenu {
				left: 100% !important;
			}
			.dropdown-menu .dropdown-toggle[data-toggle="dropdown-submenu"]::after {
				right: 1.1538em !important;
				transform: rotate(0deg) !important;
				border-bottom: 0em solid transparent !important;
			}
		}
        
        /*  Card  */
		.wrapper {
			padding-top: 100px;
			padding-bottom: 40px;
            color: #212121;
			box-sizing: border-box;
		}
		.is-card {
			min-height: 200px;
			background: white;
			padding: 20px 30px;
            box-shadow: 0 0 4px 0 rgba(0,0,0,.08), 0 2px 4px 0 rgba(0,0,0,.12);
		}
        .is-card-strip {
			min-height: 100px !important;
        }
		.is-card p {
            margin: 0;
            padding: 0;
		}
		.is-card a {
            text-decoration: none;
            outline: 0;
		}
        
        .H1,
        .is-card .H1 {
            font-family: "Josefin Slab", "Arvo", "Open Sans", sans-serif;
            font-size: 2.5rem;
            font-weight: bold;
            margin-bottom: 1.25rem;
            color: #424242;
        }
        .H2,
        .is-card .H2 {
            font-family: "Rubik", sans-serif;
            font-size: 1.5rem;
            margin-bottom: 0.5rem;
            font-weight: 500;
        }
        .H3,
        .is-card .H3 {
            font-family: "Rubik", sans-serif;
            font-size: 1.2rem;
            margin-bottom: 0.5rem;
            font-weight: 500;
        }
        .H4,
        .is-card .H4 {
            font-family: "Open Sans", sans-serif;
            font-size: 1rem;
            color: #7f8c8d;
            word-wrap: break-word;
        }
        .H5,
        .is-card .H5 {
            font-family: "Rubik", sans-serif;
            font-size: 0.9rem;
            font-weight: 500;
        }
        
        .is-card .block {
            display: inline-block;
            padding: 0 15px;
            border-right: 1px solid #b2bec3;
            text-align: center;
            height: 100%;
        }
        @media (max-width: 479px) {
            .is-card #tempBlock {
                display: none;
            }
            .is-card .block {
                max-width: 160px;
                overflow-x: hidden;
            }
        }
        @media (min-width: 480px) {
            .is-card #tempBlock {
                display: inline-block;
            }
            .is-card .block {
                max-width: 1000px;
                overflow-x: inherit;
            }
        }
        .is-card .block .top {
            font-size: 15px;
            line-height: 15px;
            color: #212121;
            font-weight: 500;
            margin-bottom: 8px;
            margin-top: 8px;
        }
        .is-card .block .bottom {
            font-size: 22px;
            line-height: 22px;
            color: cornflowerblue;
            font-weight: bold;
        }
        
        .is-card .icons span {
            padding: 4px 7px;
            color: white;
            border-radius: 5px;
            font-weight: bold;
            font-size: 0.7rem;
        }
        
        .is-card .files span {
            display: inline-block;
            float: left;
            margin-right: 10px;
        }
        .is-card .files .links {
            display: inline-block;
            float: left;
        }
        .is-card .files .links a {
            display: block;
        }
        .is-card .files .clearfix {
            clear: both;
        }
        
        .is-card .foot .idPro {
            height: 43.56px;
            line-height: 43.56px;
        }
    </style>
	
    <!--  Buttons  -->
    <style>
        .btn {
			padding: 10px 10px;
			max-width: 80%;
            margin: 0;
        }
        .btn-warning {
			background-color: rgb(247, 237, 74);
			color: rgb(63, 60, 3);
			border-color: rgb(247, 237, 74);
		}
		.btn-warning:hover,
		.btn-warning:focus {
			background-color: rgb(234, 221, 10);
			border-color: rgb(234, 221, 10);
			color: rgb(63, 60, 3);
		}
		.btn-danger {
			background-color: #ff3366;
			border-color: #ff3366;
			color: #ffffff;
		}
		.btn-danger:hover,
		.btn-danger:focus {
			background-color: #e50039;
			border-color: #e50039;
		}
		.btn-info {
			background-color: #149dcc;
			border-color: #149dcc;
			color: #ffffff;
		}
		.btn-info:hover,
		.btn-info:focus {
			background-color: #0d6786;
			border-color: #0d6786;
		}
        .btn a {
            text-decoration: none;
            color: white;
        }
        .btn-warning a {
			color: rgb(63, 60, 3);
        }
    </style>

</head>

<body>

<!--  Loading Overlay  -->
<!--
<div class="loadingOverlay">
    <div id="loader">
        <div id="shadow"></div>
        <div id="box"></div>
        <div id="loading">Loading...</div>
    </div>
</div>
-->
    
<?php if ($this->session->flashdata('msg')) : ?>
    <div class="alertMsg">
        <?= $this->session->flashdata('msg') ?>
    </div>
<?php endif; ?>

<?php if (validation_errors()) : ?>
    <div class="yoloMsg">
        <div class="alert alert-danger"><?= validation_errors(); ?></div>
    </div>
<?php endif; ?>

<section class="menu cid-qH62TEfJyl" once="menu" id="menu1-e">
	<nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-toggleable-sm">

		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<div class="hamburger">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
		</button>

		<div class="menu-logo">
			<div class="navbar-brand">

                    <span class="navbar-caption-wrap">
                        <a class="navbar-caption text-white display-5" href="<?php echo base_url(); ?>">Bidder's Arena</a>
                    </span>
			</div>
		</div>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">

				<li class="nav-item"><a class="nav-link link text-white display-4" href="#"><span class="mbri-cash mbr-iconfont mbr-iconfont-btn"></span>My Wallet<br></a></li>

				<li class="nav-item"><a class="nav-link link text-white display-4" href="<?php echo base_url();?>myprojects"><span class="mbri-bookmark mbr-iconfont mbr-iconfont-btn"></span>My Projects<br></a></li>

				<li class="nav-item"><a class="nav-link link text-white display-4" href="<?php echo base_url();?>dashboard"><span class="mbri-sites mbr-iconfont mbr-iconfont-btn"></span>Dashboard<br></a></li>

				<li class="nav-item dropdown">
					<a class="nav-link link dropdown-toggle text-white display-4" href="#" data-toggle="dropdown-submenu" aria-expanded="false">
						<span class="profileIconNav"></span><?= $this->session->userdata('username') ?><br></a>

					<div class="dropdown-menu">

						<div class="dropdown">
							<a class="dropdown-item text-white dropdown-toggle display-4" data-toggle="dropdown-submenu" aria-expanded="false">Help</a>

							<div class="dropdown-menu dropdown-submenu">
								<a class="dropdown-item text-white display-4" href="#" aria-expanded="false">FAQ</a>

								<a class="dropdown-item text-white display-4" href="#">Contact Us</a>

								<a class="dropdown-item text-white display-4" href="#" aria-expanded="false">Our Charges</a>
							</div>
						</div>

						<a class="dropdown-item text-white display-4" href="<?php echo base_url();?>profile" aria-expanded="false">My Profile</a>

						<a class="dropdown-item text-white display-4" href="<?php echo base_url();?>logout" aria-expanded="false">Logout</a>

					</div>
				</li>
			</ul>
		</div>
	</nav>
</section>

<div class="container wrapper">
	<div class="row">
       
        <p class="H2 pl-4 pl-sm-0"><?php if (isset($title)) : ?><?= $title ?><?php endif; ?></p>
        
        <div class="col col-12 is-card is-card-strip mb-3">
            
            <div class="block pl-0">
                <p class="top">Bids</p>
                <p class="bottom">0</p>
            </div>
            
            <div class="block">
                <p class="top">Avg Bid</p>
                <p class="bottom">N/A</p>
            </div>
            
            <div class="block" id="tempBlock">
                <p class="top">Max Budget</p>
                <p class="bottom"><?php if (isset($budget)) : ?><?= '&#8377; '.$budget; ?><?php endif; ?></p>
            </div>
            
            <div class="block" style="border: none;">
                <p class="top">Budget Range</p>
                <p class="bottom"><?php if (isset($budget_range)) : ?><?= '&#8377; '.$budget_range; ?><?php endif; ?></p>
            </div>
            
        </div>
        
        <div class="col col-12 is-card">
            <div class="row">
                <div class="col-7">
                    <p class="H3">Project Description</p>
                </div>
                <div class="col-5 text-right">
                    <p class="icons">
                        <?php if($helper == "1") : ?><?= '<span style="background: #F57F17;">Helper</span>' ?><?php endif; ?>
                        
                        <?php if($highlighted == "1") : ?><?= '<span style="background: #2185d5;">Highlighted</span>' ?><?php endif; ?>
                        
                        <?php if($nda == "1") : ?><?= '<span style="background: #009688;">NDA</span>' ?><?php endif; ?>
                        
                        <?php if($urgent == "1") : ?><?= '<span style="background: #F44336;">Urgent</span>' ?><?php endif; ?>
                    </p>
                </div>
            </div>
            
            <div class="row">
                <div class="col-12 col-sm-10 col-lg-8">
                    <p class="H4"><?php if (isset($description)) : ?><?= $description?><?php endif; ?></p>
                </div>
            </div>
            
            <?php if (isset($skills_required) && ($skills_required != "")) : ?>
                <?php
                    echo '<p class="H3 mt-3">Skills Required</p>';
                    echo '<p class="H4">'.$skills_required.'</p>';
                ?>
            <?php endif; ?>
            
            <?php if (isset($link) && ($link != "")) : ?>
                <?php
                    echo '<p class="H3 mt-3">Sample Web Link</p>';
                    echo '<p class="H4">'.$link.'</p>';
                ?>
            <?php endif; ?>
            
            <?php if (isset($budget) && ($budget != "")) : ?>
                <?php
                    echo '<p class="H3 mt-3">Actual Budget</p>';
                    echo '<p class="H4">&#8377; '.$budget.'</p>';
                ?>
            <?php endif; ?>
            
            <?php if (isset($deadline) && ($deadline != "")) : ?>
                <?php
                    echo '<p class="H3 mt-3">Project Deadline</p>';
                    echo '<p class="H4">'.$deadline.'</p>';
                ?>
            <?php endif; ?>

            <?php
                if($photo1 != "" || $photo2 != "" || $photo3 != "" || $photo4 != "" || $photo5 != "") {
                    echo '<div class="files mt-3"><span class="H5">Additional files: </span><div class="links">';
                    
                    if($photo1 != "") {
                        $name = substr($photo1, strrpos($photo1, '/')+1);
                        $link = base_url().$photo1;
                        echo '<a href="'.$link.'" target="blank"><span class="file pl-2">'.$name.'</span></a>';
                    }
                    
                    if($photo2 != "") {
                        $name = substr($photo1, strrpos($photo2, '/')+1);
                        $link = base_url().$photo2;
                        echo '<a href="'.$link.'" target="blank"><span class="file pl-2">'.$name.'</span></a>';
                    }
                    
                    if($photo3 != "") {
                        $name = substr($photo1, strrpos($photo3, '/')+1);
                        $link = base_url().$photo3;
                        echo '<a href="'.$link.'" target="blank"><span class="file pl-2">'.$name.'</span></a>';
                    }
                    
                    if($photo4 != "") {
                        $name = substr($photo1, strrpos($photo4, '/')+1);
                        $link = base_url().$photo4;
                        echo '<a href="'.$link.'" target="blank"><span class="file pl-2">'.$name.'</span></a>';
                    }
                    
                    if($photo5 != "") {
                        $name = substr($photo1, strrpos($photo5, '/')+1);
                        $link = base_url().$photo5;
                        echo '<a href="'.$link.'" target="blank"><span class="file pl-2">'.$name.'</span></a>';
                    }
                    
                    echo '</div><div class="clearfix"></div></div>';
                }
            ?>
            
            <hr class="mt-5">
            <div class="foot row">
                <div class="col-12 col-sm-6 text-center text-sm-left">
                    <a class="btn btn-info" href="<?php echo base_url(); ?>post-project">Post Your Own Project</a>
                </div>
                
                
                <div class="idPro col-12 col-sm-6 text-center text-sm-right pt-2 pt-sm-0">
                    <span class="H5 pr-2">Project ID:</span><?php if (isset($project_id)) : ?><?= $project_id ?><?php endif; ?>
                </div>
            </div>
        </div>
        
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/assets/web/assets/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/assets/dropdown/js/script.min.js"></script>

<script>
</script>

</body>

</html>
