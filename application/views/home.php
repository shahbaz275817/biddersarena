<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <meta name="description" content="">
    <title>Bidder's Arena</title>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/web/assets/mobirise-icons/mobirise-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/tether/tether.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/dropdown/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/socicon/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/animatecss/animate.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/theme/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/mobirise/css/mbr-additional.css" type="text/css">


    <!--  Tabs  -->

    <style>
.tabs {
  -webkit-overflow-scrolling: touch;
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  -webkit-box-align: stretch;
      -ms-flex-align: stretch;
          align-items: stretch;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  font-size: 1rem;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
  overflow: hidden;
  overflow-x: auto;
  white-space: nowrap;
}

.tabs:not(:last-child) {
  margin-bottom: 1.5rem;
}

.tabs a {
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  border-bottom-color: #dbdbdb;
  border-bottom-style: solid;
  border-bottom-width: 1px;
  color: #4a4a4a;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  margin-bottom: -1px;
  padding: 0.5em 1em;
  vertical-align: top;
}

.tabs a:hover {
  border-bottom-color: #363636;
  color: #363636;
}

.tabs li {
  display: block;
}

.tabs li.is-active a {
  border-bottom-color: #3273dc;
  color: #3273dc;
}

.tabs ul {
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  border-bottom-color: #dbdbdb;
  border-bottom-style: solid;
  border-bottom-width: 1px;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
  -ms-flex-negative: 0;
      flex-shrink: 0;
  -webkit-box-pack: start;
      -ms-flex-pack: start;
          justify-content: flex-start;
}

.tabs ul.is-left {
  padding-right: 0.75em;
}

.tabs ul.is-center {
  -webkit-box-flex: 0;
      -ms-flex: none;
          flex: none;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  padding-left: 0.75em;
  padding-right: 0.75em;
}

.tabs ul.is-right {
  -webkit-box-pack: end;
      -ms-flex-pack: end;
          justify-content: flex-end;
  padding-left: 0.75em;
}

.tabs .icon:first-child {
  margin-right: 0.5em;
}

.tabs .icon:last-child {
  margin-left: 0.5em;
}

.tabs.is-centered ul {
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
}

.tabs.is-right ul {
  -webkit-box-pack: end;
      -ms-flex-pack: end;
          justify-content: flex-end;
}

.tabs.is-boxed a {
  border: 1px solid transparent;
  border-radius: 3px 3px 0 0;
}

.tabs.is-boxed a:hover {
  background-color: whitesmoke;
  border-bottom-color: #dbdbdb;
}

.tabs.is-boxed li.is-active a {
  background-color: white;
  border-color: #dbdbdb;
  border-bottom-color: transparent !important;
}

.tabs.is-fullwidth li {
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
  -ms-flex-negative: 0;
      flex-shrink: 0;
}

.tabs.is-toggle a {
  border-color: #dbdbdb;
  border-style: solid;
  border-width: 1px;
  margin-bottom: 0;
  position: relative;
}

.tabs.is-toggle a:hover {
  background-color: whitesmoke;
  border-color: #b5b5b5;
  z-index: 2;
}

.tabs.is-toggle li + li {
  margin-left: -1px;
}

.tabs.is-toggle li:first-child a {
  border-radius: 3px 0 0 3px;
}

.tabs.is-toggle li:last-child a {
  border-radius: 0 3px 3px 0;
}

.tabs.is-toggle li.is-active a {
  background-color: #3273dc;
  border-color: #3273dc;
  color: #fff;
  z-index: 1;
}

.tabs.is-toggle ul {
  border-bottom: none;
}

.tabs.is-toggle.is-toggle-rounded li:first-child a {
  border-bottom-left-radius: 290486px;
  border-top-left-radius: 290486px;
  padding-left: 1.25em;
}

.tabs.is-toggle.is-toggle-rounded li:last-child a {
  border-bottom-right-radius: 290486px;
  border-top-right-radius: 290486px;
  padding-right: 1.25em;
}

.tabs.is-small {
  font-size: 0.75rem;
}

.tabs.is-medium {
  font-size: 1.25rem;
}

.tabs.is-large {
  font-size: 1.5rem;
}
    </style>
    
    <!--  Modal  -->
    <style>
        
.modal {
  bottom: 0;
  left: 0;
  position: absolute;
  right: 0;
  top: 0;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  display: none;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  overflow: hidden;
  position: fixed;
  z-index: 40;
}

.modal.is-active {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

.modal-background {
  bottom: 0;
  left: 0;
  position: absolute;
  right: 0;
  top: 0;
  background-color: rgba(10, 10, 10, 0.86);
}

.modal-content,
.modal-card {
  margin: 0 20px;
  max-height: calc(100vh - 160px);
  overflow: auto;
  position: relative;
  width: 100%;
}

@media screen and (min-width: 769px), print {
  .modal-content,
  .modal-card {
    margin: 0 auto;
    max-height: calc(100vh - 40px);
    width: 640px;
  }
}

.modal-close {
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  -moz-appearance: none;
  -webkit-appearance: none;
  background-color: rgba(10, 10, 10, 0.2);
  border: none;
  border-radius: 290486px;
  cursor: pointer;
  display: inline-block;
  -webkit-box-flex: 0;
      -ms-flex-positive: 0;
          flex-grow: 0;
  -ms-flex-negative: 0;
      flex-shrink: 0;
  font-size: 0;
  height: 20px;
  max-height: 20px;
  max-width: 20px;
  min-height: 20px;
  min-width: 20px;
  outline: none;
  position: relative;
  vertical-align: top;
  width: 20px;
  background: none;
  height: 40px;
  position: fixed;
  right: 20px;
  top: 20px;
  width: 40px;
}

.modal-close:before, .modal-close:after {
  background-color: white;
  content: "";
  display: block;
  left: 50%;
  position: absolute;
  top: 50%;
  -webkit-transform: translateX(-50%) translateY(-50%) rotate(45deg);
          transform: translateX(-50%) translateY(-50%) rotate(45deg);
  -webkit-transform-origin: center center;
          transform-origin: center center;
}

.modal-close:before {
  height: 2px;
  width: 50%;
}

.modal-close:after {
  height: 50%;
  width: 2px;
}

.modal-close:hover, .modal-close:focus {
  background-color: rgba(10, 10, 10, 0.3);
}

.modal-close:active {
  background-color: rgba(10, 10, 10, 0.4);
}

.modal-close.is-small {
  height: 16px;
  max-height: 16px;
  max-width: 16px;
  min-height: 16px;
  min-width: 16px;
  width: 16px;
}

.modal-close.is-medium {
  height: 24px;
  max-height: 24px;
  max-width: 24px;
  min-height: 24px;
  min-width: 24px;
  width: 24px;
}

.modal-close.is-large {
  height: 32px;
  max-height: 32px;
  max-width: 32px;
  min-height: 32px;
  min-width: 32px;
  width: 32px;
}

.modal-card {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  max-height: calc(100vh - 40px);
  overflow: hidden;
}

.modal-card-head,
.modal-card-foot {
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  background-color: whitesmoke;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-negative: 0;
      flex-shrink: 0;
  -webkit-box-pack: start;
      -ms-flex-pack: start;
          justify-content: flex-start;
  padding: 20px;
  position: relative;
}

.modal-card-head {
  border-bottom: 1px solid #dbdbdb;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
}

.modal-card-title {
  color: #363636;
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
  -ms-flex-negative: 0;
      flex-shrink: 0;
  font-size: 1.5rem;
  line-height: 1;
}

.modal-card-foot {
  border-bottom-left-radius: 5px;
  border-bottom-right-radius: 5px;
  border-top: 1px solid #dbdbdb;
}

.modal-card-foot .button:not(:last-child) {
  margin-right: 10px;
}

.modal-card-body {
  -webkit-overflow-scrolling: touch;
  background-color: white;
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
  -ms-flex-negative: 1;
      flex-shrink: 1;
  overflow: auto;
  padding: 20px;
}
    </style>

    <!--  Custom  -->
    <style>
        .alertMsg {
            position: absolute;
            top: 0px;
            z-index: 1035;
            height: 60.8px;
            width: 100vw;
            text-align: center;
            background: #F44336;
            color: #EEEEEE;
            font-size: 18px;
            line-height: 60.8px;
            
            animation: fadeOutTwoSec 500ms ease-out forwards 3500ms;
        }
        @keyframes fadeOutTwoSec {
            0%{
                top: 0;
            }
            100%{
                top: -65px;
            }
        }
        
        .tabs {
            overflow: hidden;
        }
        .tabs li {
            width: 50%;
            cursor: pointer;
        }
        .tabs li a{
            text-decoration: none;
            height: 60px;
            font-size: 25px;
        }
        
        .formClass {
            padding: 20px 30px;
        }
        .formClass .forgot {
            text-align: center;
            padding: 0;
            margin: 0;
            padding-top: 15px;
        }
        .formClass .forgot a {
            text-decoration: none;
            cursor: pointer;
            display: inline-block;
        }
        .formClass .help {
            color: red;
            font-weight: bold;
        }
        .formClass .form-control,
        .formClass .btn {
            height: 50px;
        }
        .formClass .btn {
            width: 100%;
            margin: 0;
            margin-top: 15px !important;
            font-size: 
        }
        .formClass .btn:hover {
            background: black !important;
            color: white !important;
            border-color: black;
        }
        .formClass .btn:focus {
            background: black !important;
            color: white !important;
            border-color: black;
            outline: none;
            box-shadow: none;
        }
        .formClass .form-control:focus {
            border: 1.5px solid cornflowerblue;
            outline: 0;
            -webkit-box-shadow: none;
            box-shadow: none;
        }
    </style>
</head>

<body style="overflow-x: hidden;">

    <div class="modal" style="z-index: 1040;">
        <div class="modal-background"></div>
            <div class="modal-content container" style="max-height: none; overflow: hidden;">

            <div class="tabs is-large is-centered" style="margin: 0px;">
                <ul style="padding:0px; margin: 0px;">
                    <li class="is-active loginClass"><a onclick="switchTab(this.id);" id="idLoginLink">Login</a></li>

                    <li class="registerClass"><a onclick="switchTab(this.id);" id="idRegisterLink">Register</a></li>
                </ul>
            </div>

            <div class="formClass row">

                <form method="post" action="<?php echo base_url();?>user/passresetreq" id="forgotPassword" style="display: none;" class="col col-xs-12">

                    <p class="forgot">Lost your password? Please enter your email address. You will receive a link to create a new password.</p>

                    <div class="form-group" style="margin-top: 20px;">
                        <div class="input-group">
                            <input type="email" class="form-control" placeholder="E-mail" name="email" required>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary" name="reset">Reset Password</button>

                    <p class="forgot"><a id="goToLogin" onclick="toggleForgot(this.id);">Back to login</a></p>
                </form>

                <form method="post" action="<?php echo base_url();?>login" id="login" style="display: block;" class="col col-xs-12">

                    <?php if ($this->session->flashdata('login_error')) : ?>
                        <p id="emailHelp" class="form-text text-muted" style="text-align: center !important; color: red !important; font-weight: bold;"><?= $this->session->flashdata('login_error')?></p>
                    <?php endif; ?>

					<?php if ($this->session->flashdata('register_success')) : ?>
						<p id="emailHelp" class="form-text text-muted" style="text-align: center !important; color: #009688 !important; font-weight: bold;"><?= $this->session->flashdata('register_success')?></p>
					<?php endif; ?>

                    <div class="form-group">
                        <label class="control-label" for="email">
                            Email address
                        </label>
                        <div class="input-group">
                            <input type="email" class="form-control" placeholder="Enter email" name="email" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="email">
                            Password
                        </label>
                        <div class="input-group">
                            <input type="password" class="form-control" placeholder="Enter password" name="password" required autocomplete="off">
                        </div>
                    </div>


                    <button type="submit" class="btn btn-primary" name="submit">Login</button>

                    <p class="forgot"><a id="goToForgot" onclick="toggleForgot(this.id);">Forgot password?</a></p>
                </form>

                <form method="post" action="<?php echo base_url();?>signup" id="register" style="display: none;" class="col col-xs-12" autocomplete="off">

                    <?php if ($this->session->flashdata('registration_error')) : ?>
                        <p id="emailHelp" class="form-text text-muted" style="text-align: center !important; color: red; font-weight: bold;"><?= $this->session->flashdata('registration_error') ?></p>
                    <?php endif; ?>

                    <div class="form-group">
                        <label class="control-label" for="email">
                            Username
                        </label>
                        <div class="input-group">
                            <input type="text" id="username" class="form-control" placeholder="Enter username" name="username" required>
                        </div>
                        <small id="username_result" class="form-text" style="color: red; font-weight: 400;"></small>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="email">
                            Email address
                        </label>
                        <div class="input-group">
                            <input type="email" id="email" class="form-control" placeholder="Enter email" name="email" required>
                        </div>
                        <small id="email_result" class="form-text" style="color: red; font-weight: 400;"></small>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="email">
                            Password
                        </label>
                        <div class="input-group">
                            <input type="password" id="password" class="form-control" placeholder="Enter password" name="password" required>
                        </div>
                        <small id="password_result" class="form-text" style=" color: red; font-weight: 400;"></small>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="email">
                            Confirm password
                        </label>
                        <div class="input-group">
                            <input type="password" id="confirm_password" class="form-control" placeholder="Re-enter password" name="confirm_password" required>
                        </div>
                        <small id="password_match_result" class="form-text" style=" color: red ; font-weight: 400;"></small>
                    </div>

                    <div class="radio">
                        <label class="radio-inline" style="padding: 0px; margin-right: 10px;">
                            Signing up to?
                        </label>
                        <label class="radio-inline">
                            <input id="hire" type="radio" name="user_type" value="Hire" style="margin-top: 8px;" checked>
                            Hire
                        </label>
                        <label class="radio-inline">
                            <input id="work" type="radio" name="user_type" value="Work" style="margin-top: 8px;">
                            Work
                        </label>
                    </div>

                    <button type="submit" class="btn btn-primary" name="submit">Submit</button>

                    <p class="forgot">By signing in, you accept all our <a href="<?php echo base_url();?>terms">terms and conditions.</a></p>
                </form>
            </div>

            </div>
        <button class="modal-close is-large" aria-label="close" onclick="hideModal();"></button>
    </div>

    <?php if ($this->session->flashdata('msg')) : ?>
        <div class="alertMsg">
            <?= $this->session->flashdata('msg') ?>
        </div>
    <?php endif; ?>
   
    <section class="menu cid-qH62TEfJyl" once="menu" id="menu1-e">
        <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
           
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <div class="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </button>
            
            <div class="menu-logo">
                <div class="navbar-brand">
                    
                    <span class="navbar-caption-wrap">
                        <a class="navbar-caption text-white display-5" href="<?php echo base_url(); ?>">Bidder's Arena</a>
                    </span>
                </div>
            </div>
            
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                   
                    <li class="nav-item">
                        <a class="nav-link link text-white display-4" href="#content9-1f">How we work<br></a>
                    </li>
                    
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="#content4-w">Categories<br></a></li>
                    
                    <li class="nav-item"><a class="nav-link link text-white display-4" href="#content4-16">Contact us<br></a></li>
                    
                    <?php if ($this->session->has_userdata('user_id') == 1 && $this->session->has_userdata('username') == 1) : ?>
                       
                        <li class="nav-item dropdown">
                            <a class="nav-link link dropdown-toggle text-white display-4" href="#" data-toggle="dropdown-submenu" aria-expanded="false">Welcome <?= $this->session->userdata('username') ?>!<br></a>
                           
                            <div class="dropdown-menu">
                                <a class="dropdown-item text-white display-4" href="<?php echo base_url();?>dashboard">Dashboard</a>
                                
                                <a class="dropdown-item text-white display-4" href="<?php echo base_url();?>profile" aria-expanded="false">Profile</a>
                                
                                <a class="dropdown-item text-white display-4" href="<?php echo base_url();?>logout" aria-expanded="false">Logout</a>
                            </div>
                        </li>
                        
                    <?php endif; ?>

                    <?php if ($this->session->has_userdata('username') == 0) : ?>

                        <li class="nav-item"><a class="nav-link link text-white display-4" href="#" id="loginNavLink" onclick="showModal(this.id);">Login<br></a></li>
                        
                        <li class="nav-item"><a class="nav-link link text-white display-4" href="#" id="registerNavLink" onclick="showModal(this.id);">Sign up<br></a></li>
                        
                    <?php endif; ?>
                    
                </ul>
            </div>
        </nav>
    </section>
    
    <section class="header9 cid-qH69aPWdBF mbr-fullscreen" id="header9-k">
        <div class="container">
            <div class="media-container-column mbr-white col-md-8">
                <h1 class="mbr-section-title align-left mbr-bold pb-3 mbr-fonts-style display-1">WE'RE HERE TO HELP!</h1>

                <p class="mbr-text align-left pb-3 mbr-fonts-style display-5">Let us get all your work done for you.</p>
                <div class="mbr-section-btn align-left"><a class="btn btn-md btn-primary display-4" href="#" id="registerNavLink" onclick="showModal(this.id);">POST PROJECT</a></div>
            </div>
        </div>
    </section>

    <section class="mbr-section article content9 cid-qH6mY3Y7gJ" id="content9-1f">
        <div class="container">
            <div class="inner-container" style="width: 100%;">
                <hr class="line" style="width: 20%;">
                <div class="section-text align-center mbr-fonts-style display-2"><strong style="color: white;">HOW WE WORK</strong></div>
                <hr class="line" style="width: 20%;">
            </div>
        </div>
    </section>

    <section class="features8 cid-qH6kLRqGjL mbr-parallax-background" id="features8-19">
        <div class="container">
            <div class="media-container-row">

                <div class="card  col-12 col-md-6 col-lg-4">
                    <div class="card-img">
                        <div style="background: url(<?php echo base_url(); ?>assets/Images/Work/1.png); height: 100px; width: 100px; margin: 0 auto; background-size: cover; background-position: center center;"></div>
                    </div>
                    <div class="card-box align-center">
                        <h4 class="card-title mbr-fonts-style display-7">
                            Requirement
                        </h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                            Tell us what you want to get done by simply uploading your project details, skills required and we will analyze your project. We will notify skilled workers of your required skills about your project.
                        </p>
                    </div>
                </div>

                <div class="card  col-12 col-md-6 col-lg-4">
                    <div class="card-img">
                        <div style="background: url(<?php echo base_url(); ?>assets/Images/Work/2.png); height: 100px; width: 100px; margin: 0 auto; background-size: cover; background-position: center center;"></div>
                    </div>
                    <div class="card-box align-center">
                        <h4 class="card-title mbr-fonts-style display-7">
                            Offers</h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                            The workers offer their best price on your project and you can view their profile and previous works and compare them to decide the best candidate for your project. Award the project whomever you like.
                        </p>
                    </div>
                </div>

                <div class="card  col-12 col-md-6 col-lg-4">
                    <div class="card-img">
                        <div style="background: url(<?php echo base_url(); ?>assets/Images/Work/3.png); height: 100px; width: 100px; margin: 0 auto; background-size: cover; background-position: center center;"></div>
                    </div>
                    <div class="card-box align-center">
                        <h4 class="card-title mbr-fonts-style display-7">
                            Management</h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                            We will keep you updated using our management tools about the progress of the projects. Pay only when satisfied with the quality of work done.
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="mbr-section content4 cid-qH6c6z1lf9" id="content4-w">
        <div class="container">
            <div class="media-container-row">
                <div class="title col-12 col-md-8">
                    <h2 class="align-center pb-3 mbr-fonts-style display-2">   
                        <strong>EXLORE THE CATEGORIES</strong>
                    </h2>
                </div>
            </div>
        </div>
    </section>

    <section class="features6 cid-qH6bXJ2RwL" id="features6-t">
        <div class="container">
            <div class="media-container-row">

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-img pb-3">
                        <div style="background: url(<?php echo base_url(); ?>assets/Images/Categories/category%20finance.png); height: 72px; width: 72px; margin: 0 auto; background-size: cover; background-position: center center;"></div>
                    </div>
                    <div class="card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-7">Website, IT &amp; Software</h4>
                        <p class="mbr-text mbr-fonts-style display-7">C Programming
                            <br>Cloud Computing
                            <br>Game Development
                            <br>Javascript
                            <br>MySQL
                            <br>PHP
                            <br>Website Management
                            <br>Wordpress
                            <br><a href="#">show more...</a></p>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-img pb-3">
                        <div style="background: url(<?php echo base_url(); ?>assets/Images/Categories/category%20logo%20design.png); height: 72px; width: 72px; margin: 0 auto; background-size: cover; background-position: center center;"></div>
                    </div>
                    <div class="card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-7">Design &amp; Architecture</h4>
                        <p class="mbr-text mbr-fonts-style display-7">After Effects
                            <br>Animation
                            <br>Building Architecture
                            <br>Graphic Design
                            <br>Illustrator
                            <br>Logo Design
                            <br>Photoshop
                            <br>Website Design
                            <br><a href="#">show more...</a></p>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-img pb-3">
                        <div style="background: url(<?php echo base_url(); ?>assets/Images/Categories/category%20marketing.png); height: 72px; width: 72px; margin: 0 auto; background-size: cover; background-position: center center;"></div>
                    </div>
                    <div class="card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-7">Business, Legal &amp; Accounting</h4>
                        <p class="mbr-text mbr-fonts-style display-7">Accounting
                            <br>Attorney
                            <br>Business Analysis
                            <br>Human Resources
                            <br>Payroll
                            <br>Project Management
                            <br>Startups
                            <br>Tax
                            <br><a href="#">show more...</a></p>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-img pb-3">
                        <div style="background: url(<?php echo base_url(); ?>assets/Images/Categories/content%20writing.png); height: 72px; width: 72px; margin: 0 auto; background-size: cover; background-position: center center;"></div>
                    </div>
                    <div class="card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-7">Writing &amp; Content
                        </h4>
                        <p class="mbr-text mbr-fonts-style display-7">Article Writing
                            <br>Blog
                            <br>Book Writing
                            <br>Financial Research
                            <br>Powerpoint
                            <br>Product Descriptions
                            <br>Report Writing
                            <br>Technical Writing
                            <br><a href="#">show more...</a></p>
                    </div>
                </div>

            </div>

        </div>

    </section>

    <section class="features6 cid-qH6c1Ul9VL" id="features6-u">
        <div class="container">
            <div class="media-container-row">

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-img pb-3">
                        <div style="background: url(<?php echo base_url(); ?>assets/Images/Categories/Mobile%20developement.png); height: 72px; width: 72px; margin: 0 auto; background-size: cover; background-position: center center;"></div>
                    </div>
                    <div class="card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-7">Mobile Development</h4>
                        <p class="mbr-text mbr-fonts-style display-7">Android
                            <br>Java
                            <br>J2ME
                            <br>Kotlin
                            <br>iPad
                            <br>iPhone
                            <br>Mobile App Development
                            <br>Windows Phone
                            <br><a href="#">show more...</a></p>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-img pb-3">
                        <div style="background: url(<?php echo base_url(); ?>assets/Images/Categories/sales%20and%20marketing.png); height: 72px; width: 72px; margin: 0 auto; background-size: cover; background-position: center center;"></div>
                    </div>
                    <div class="card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-7">Sales &amp; Marketing</h4>
                        <p class="mbr-text mbr-fonts-style display-7">Advertising
                            <br>Affiliate Marketing
                            <br>Branding
                            <br>Content Marketing
                            <br>Google Adwords
                            <br>Internet Marketing
                            <br>Journalist
                            <br>Social Media Marketing
                            <br><a href="#">show more...</a></p>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-img pb-3">
                        <div style="background: url(<?php echo base_url(); ?>assets/Images/Categories/website.png); height: 72px; width: 72px; margin: 0 auto; background-size: cover; background-position: center center;"></div>
                    </div>
                    <div class="card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-7">Engineering &amp; Science</h4>
                        <p class="mbr-text mbr-fonts-style display-7">Algorithm
                            <br>Arduino
                            <br>AutoCAD
                            <br>Circuit Design
                            <br>Engineering
                            <br>Machine Learning
                            <br>Mathematica
                            <br>Structural Engineering
                            <br><a href="#">show more...</a></p>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-img pb-3">
                        <div style="background: url(<?php echo base_url(); ?>assets/Images/Categories/virtual%20assistant.png); height: 72px; width: 72px; margin: 0 auto; background-size: cover; background-position: center center;"></div>
                    </div>
                    <div class="card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-7">
                            Translation
                        </h4>
                        <p class="mbr-text mbr-fonts-style display-7">Arabic
                            <br>Dutch
                            <br>English
                            <br>Italian
                            <br>Russian
                            <br>Tamil
                            <br>Urdu
                            <br>Farsi
                            <br><a href="#">show more...</a></p>
                    </div>
                </div>

            </div>

        </div>

    </section>

    <section class="mbr-section article content9 cid-qH6mPRF83E" id="content9-1d">
        <div class="container">
            <div class="inner-container" style="width: 100%;">
                <hr class="line" style="width: 20%;">
                <div class="section-text align-center mbr-fonts-style display-2"><strong style="color: white;">APP UNDER DEVELOPMENT</strong></div>
                <hr class="line" style="width: 20%;">
            </div>
        </div>
    </section>

    <section class="cid-qH6e8cwbex mbr-fullscreen mbr-parallax-background" id="header2-z">
        <div class="container align-center">
            <div class="row justify-content-md-center">
                <div class="mbr-white col-md-10">
                    <h1 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-1">
                        APP COMING SOON
                    </h1>

                    <p class="mbr-text pb-3 mbr-fonts-style display-5">
                        Take your workplace wherever you go with our upcoming app!
                    </p>

                </div>
            </div>
        </div>

    </section>

    <section class="mbr-section article content9 cid-qH6OoFIMDU" id="content9-1g">
        <div class="container">
            <div class="inner-container" style="width: 100%;">
                <hr class="line" style="width: 20%;">
                <div class="section-text align-center mbr-fonts-style display-2"><strong style="color: white;">JOIN THE FAMILY</strong></div>
                <hr class="line" style="width: 20%;">
            </div>
        </div>
    </section>

    <section class="cid-qH6enUdfw4 mbr-fullscreen mbr-parallax-background" id="header2-10">
        <div class="container align-center">
            <div class="row justify-content-md-center">
                <div class="mbr-white col-md-10">
                    <p class="mbr-text pb-3 mbr-fonts-style display-1">
                       <strong>Let’s get this relationship started!</strong>
                    </p>
                    
                    <div class="mbr-section-btn"><a class="btn btn-md btn-white display-4" href="#" id="registerNavLink" onclick="showModal(this.id);">GET STARTED</a></div>
                </div>
            </div>
        </div>

    </section>

    <section class="mbr-section content4 cid-qH6fw5Ikbb" id="content4-16">
        <div class="container">
            <div class="media-container-row">
                <div class="title col-12 col-md-8">
                    <h2 class="align-center pb-3 mbr-fonts-style display-2">
                        <strong>CONTACT US</strong>
                    </h2>
                </div>
            </div>
        </div>
    </section>

    <section class="mbr-section form4 cid-qH6fgGafZ1" id="form4-13">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="google-map"><iframe frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0Dx_boXQiwvdz8sJHoYeZNVTdoWONYkU&amp;q=place_id:ChIJpefp3bv9mzkR2pmn-zJkVss" allowfullscreen=""></iframe></div>
                </div>
                
                <div class="col-md-6">
                    <div>
                        <div class="icon-block pb-3">
                        </div>
                    </div>
                    
                    <form class="block mbr-form" action="<?php echo base_url();?>user/feedback" method="post">
                        <div class="row">
                            <div class="col-md-6 multi-horizontal" data-for="name">
                                <input type="text" class="form-control input" name="name" data-form-field="Name" placeholder="Your Name" required="" id="name-form4-13">
                            </div>

                            <div class="col-md-12" data-for="email">
                                <input type="email" class="form-control input" name="email" data-form-field="Email" placeholder="Email" required="" id="email-form4-13">
                            </div>

                            <div class="col-md-12" data-for="message">
                                <textarea class="form-control input" name="message" rows="3" data-form-field="Message" placeholder="Message" id="message-form4-13" style="overflow: hidden; resize: none;"></textarea>
                            </div>

                            <div class="input-group-btn col-md-12" style="margin-top: 10px;">
                                <button type="submit" class="btn btn-black-outline display-4">SEND MESSAGE</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section once="" class="cid-qH6fkI09jh" id="footer7-14">
        <div class="container">
            <div class="media-container-row align-center mbr-white">
                
                <div class="row social-row">
                    <div class="social-list align-right pb-2">
                        <div class="soc-item">
                            <a href="#" target="_blank">
                                <span class="socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="#" target="_blank">
                                <span class="socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="#" target="_blank">
                                <span class="socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="#" target="_blank">
                                <span class="socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                        </div>
                    </div>
                </div>
                
                <div class="row row-links">
                    <ul class="foot-menu">
                        <li class="foot-menu-item mbr-fonts-style display-7">
                            <p><a class="text-white mbr-bold" href="<?php echo base_url(); ?>about" target="_blank">About us</a>
                            </p>
                        </li>
                        
                        <li class="foot-menu-item mbr-fonts-style display-7">
                            <a class="text-white mbr-bold" href="<?php echo base_url(); ?>terms" target="_blank">Terms &amp; Conditions</a>
                        </li>
                            
                        <li class="foot-menu-item mbr-fonts-style display-7">
                            <p><a class="text-white mbr-bold" href="<?php echo base_url(); ?>privacy" target="_blank">Privacy Policy</a></p>
                        </li>
                    </ul>
                </div>
                
                <div class="row row-copirayt">
                    <p class="mbr-text mb-0 mbr-fonts-style mbr-white align-center display-7">
                        © Copyright 2017 Bidder's Arena - All Rights Reserved
                    </p>
                </div>

				<br>

				<div class="row row-copirayt">
					<p class="mbr-text mb-0 mbr-fonts-style mbr-white align-center display-7">
						Designed and Developed by Chamber of Developers
					</p>
				</div>
            </div>
        </div>
    </section>


    <script src="<?php echo base_url(); ?>assets/assets/web/assets/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/popper/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/tether/tether.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/smoothscroll/smooth-scroll.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/dropdown/js/script.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/viewportchecker/jquery.viewportchecker.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/parallax/jarallax.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/touchswipe/jquery.touch-swipe.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/theme/js/script.js"></script>
    
<script>
    
	$(document).ready(function() {
        
        if(window.location.href.indexOf('#login') != -1) {
			$('.modal').addClass('is-active');
		} else if(window.location.href.indexOf('#register') != -1) {
			$('.modal').addClass('is-active');
            $("#login").css("display", "none");
            $("#register").css("display", "block");
            $(".loginClass").removeClass("is-active");
            $(".registerClass").addClass("is-active");
		} else if(window.location.href.indexOf('#forgot') != -1) {
			$('.modal').addClass('is-active');
            $("#login").css("display", "none");
            $("#forgotPassword").css("display", "block");
		}

	});
    
    function toggleForgot(id) {
        $('#username').css("border-color", "#e8e8e8");
        $('#email').css("border-color", "#e8e8e8");
        $('#password').css("border-color", "#e8e8e8");
        $('#confirm_password').css("border-color", "#e8e8e8");
            
        $("#register input").val('');
        $("#register small").html("");
        $("#register #hire").attr('checked', true);
        $("#forgotPassword input").val('');
            
        $("#login input").val('');
        $("#login #emailHelp").hide();
        $("#forgotPassword input").val('');
        
        if(id == "goToLogin") {
            $("#login").css("display", "block");
            $("#forgotPassword").css("display", "none");
        } else if(id == "goToForgot") {
            $("#login").css("display", "none");
            $("#forgotPassword").css("display", "block");
        }
    }
    function switchTab(id) {
        $('#username').css("border-color", "#e8e8e8");
        $('#email').css("border-color", "#e8e8e8");
        $('#password').css("border-color", "#e8e8e8");
        $('#confirm_password').css("border-color", "#e8e8e8");
            
        $("#register input").val('');
        $("#register #emailHelp").hide();
        $("#forgotPassword input").val('');
        $("#register small").html("");
        $("#register #hire").attr('checked', true);

        $("#login input").val('');
        $("#login #emailHelp").hide();
        $("#forgotPassword input").val('');
        
        if(id == "idLoginLink") {
            $("#login").css("display", "block");
            $("#register").css("display", "none");
            $(".loginClass").addClass("is-active");
            $(".registerClass").removeClass("is-active");
            $("#forgotPassword").css("display", "none");
        } else if(id == "idRegisterLink") {
            $("#login").css("display", "none");
            $("#register").css("display", "block");
            $(".loginClass").removeClass("is-active");
            $(".registerClass").addClass("is-active");
            $("#forgotPassword").css("display", "none");
        }
    }
    function showModal(id) {
        $(".modal").addClass("is-active");
        if(id == "loginNavLink") {
            $("#login").css("display", "block");
            $("#register").css("display", "none");
            $(".loginClass").addClass("is-active");
            $(".registerClass").removeClass("is-active");
        } else if (id == "registerNavLink") {
            $("#login").css("display", "none");
            $("#register").css("display", "block");
            $(".loginClass").removeClass("is-active");
            $(".registerClass").addClass("is-active");
        }
    }
    function hideModal() {
        $(".modal").removeClass("is-active");
    }
    
	$(document).ready(function() {

		$('#username').bind('input', function () {
			var username = $('#username').val();
			if (username == '') {
				$('#username_result').html("Username Cannot be empty");
				$('#username_result').css("color", "#f77d0e");
				$('#username').css("border-color", "#f77d0e");
			} else {
				$.ajax({
					url: "<?php echo base_url(); ?>user/check_username_availability",
					method: "POST",
					data: {username: username},
					success: function (data) {
						$('#username_result').html(data);
						if (data == "Username Available") {
							$('#username_result').css("color", "green");
							$('#username').css("border-color", "green");
						} else {
							$('#username_result').css("color", "red");
							$('#username').css("border-color", "red");
						}
					}
				});
			}
		});

		$('#email').bind('input',function () {
			var email = $('#email').val();
			if (email == '') {
				$('#email_result').html("Email cannot be empty");
				$('#email_result').css("color", "#f77d0e");
				$('#email').css("border-color", "#f77d0e");
			} else {
				$.ajax({
					url: "<?php echo base_url(); ?>user/check_email_availability",
					method: "post",
					data: {email: email},
					success: function (data) {
						$('#email_result').html(data);
						if (data == "Email Available") {
							$('#email_result').css("color", "green");
							$('#email').css("border-color", "green");
						} else {
							$('#email_result').css("color", "red");
							$('#email').css("border-color", "red");
						}

					}
				});
			}
		});


		$('#password').bind('input',function () {
			var password = $('#password').val();
			if (password == '') {
				$('#password_result').html("Password cannot be empty");
				$('#password_result').css("color", "#f77d0e");
				$('#password').css("border-color", "#f77d0e");
			} else {
				$.ajax({
					url: "<?php echo base_url(); ?>user/check_password_validity",
					method: "post",
					data: {password: password},
					success: function (data) {
						$('#password_result').html(data);
						if (data == "Password OK!") {
							$('#password_result').css("color", "green");
							$('#password').css("border-color", "green");
						} else {
							$('#password_result').css("color", "red");
							$('#password').css("border-color", "red");
						}
					}
				});
			}
		});

		$('#confirm_password').bind('input',function () {
			var password = $('#password').val();
			var confirm_password = $('#confirm_password').val();
			if (confirm_password == '') {
				$('#password_match_result').html("This field cannot be empty!");
				$('#password_match_result').css("color", "#f77d0e");
				$('#confirm_password').css("border-color", "#f77d0e");
			} else {
				$.ajax({
					url: "<?php echo base_url(); ?>user/password_match",
					method: "post",
					data: {password: password, confirm_password: confirm_password},
					success: function (data) {
						$('#password_match_result').html(data);
						if (data == "Password Match!") {
							$('#password_match_result').css("color", "green");
							$('#confirm_password').css("border-color", "green");
						} else {
							$('#password_match_result').css("color", "red");
							$('#confirm_password').css("border-color", "red");
						}
					}
				});
			}
		});

	});
</script>

    <div id="scrollToTop" class="scrollToTop mbr-arrow-up" style="z-index: 1031;"><a style="text-align: center;"><i></i></a></div>
    <input name="animation" type="hidden">

</body>

</html>
