<?php if (isset($avg)) : ?>
    <?php
        $count = 0;
        $average;
        $reviews;
        foreach ($avg as $a){
            if($count == 0)
                $average = $a;
            if($count == 1)
                $reviews = $a;
            $count = $count + 1;
        }
        $pages = ceil( ((int)$reviews) / 4 );
    ?>
<?php endif; ?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
	<meta name="description" content="">
	<title>Bidder's Arena | <?php if (isset($username)) : ?><?= $username ?><?php endif; ?></title>

	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/web/assets/mobirise-icons/mobirise-icons.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap-grid.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap-reboot.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/dropdown/css/style.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/mobirise/css/mbr-additional.css" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Fonts -->
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Josefin+Slab,Arvo,Open+Sans">

	<!--  Custom  -->
	<style>
		body {
			background: #efefef;
            user-select: none;
		}
		.alertMsg {
			position: absolute;
			top: 0px;
			z-index: 1035;
			height: 60.8px;
			width: 100vw;
			text-align: center;
			background: #66BB6A;
			color: #FFF;
			font-size: 18px;
			line-height: 60.8px;

			animation: fadeOutTwoSec 500ms ease-out forwards 3500ms;
		}
		@keyframes fadeOutTwoSec {
			0%{
				top: 0;
			}
			100%{
				top: -65px;
			}
		}
		.mbr-iconfont {
			font-size: 1.3rem !important;
		}
		.profileIconNav {
			height: 35px;
			width: 35px;

			background: url("<?php if ($this->session->userdata('photo') == NULL) : ?><?php echo base_url(); ?>assets/Images/default-profile.jpg<?php endif; ?><?php if ($this->session->userdata('photo') != NULL) : ?>data:image/jpeg;base64,<?php echo base64_encode($this->session->userdata('photo')); ?><?php endif; ?>");

			margin-right: 10px;
			overflow: hidden;
			background-size: cover;
			background-position: center top;
		}

		.dropdown-submenu {
			left: 0 !important;
			transform: translateX(-100%) !important;
		}
		.dropdown-menu .dropdown-toggle[data-toggle="dropdown-submenu"]::after {
			left: 1.1538em !important;
			transform: rotate(180deg) !important;
		}
		@media (max-width: 991px) {
			.dropdown-submenu {
				left: 100% !important;
			}
			.dropdown-menu .dropdown-toggle[data-toggle="dropdown-submenu"]::after {
				right: 1.1538em !important;
				transform: rotate(0deg) !important;
				border-bottom: 0em solid transparent !important;
			}
		}

		.wrapper {
			padding-top: 100px;
			padding-bottom: 40px;
			color: #999999;
			box-sizing: border-box;
		}
		.is-card {
			min-height: 200px;
			background: white;
			padding: 20px;
		}

		.is-card-left .imgWrapper {
			width: 80%;
			margin-left: 10%;
			margin-bottom: 20px;
			overflow: hidden;
			border-radius: 3px;
		}
		.is-card-left .imgWrapper .image {
			position:relative;
			overflow:hidden;
			padding-bottom:100%;
		}
		.is-card-left .imgWrapper .img {
			position: absolute;
			height: 100%;
			width: 100%;

			background: url("<?php if ($photo == NULL) : ?><?php echo base_url(); ?>assets/Images/default-profile.jpg<?php endif; ?><?php if ($photo != NULL) : ?>data:image/jpeg;base64,<?php echo base64_encode($photo); ?><?php endif; ?>");

			background-size: cover;
			background-position: center top;
		}
        
		.is-card-left hr {
			border-top-width: 1.5px;
			margin-top: 1.4rem;
			margin-bottom: 1.4rem;
		}

		.is-card-left .buttonSection .btn {
			padding: 10px 10px;
			width: 180px;
			max-width: 80%;
		}
		.btn-warning {
			background-color: rgb(247, 237, 74);
			color: rgb(63, 60, 3);
			border-color: rgb(247, 237, 74);
		}
		.btn-warning:hover,
		.btn-warning:focus {
			color: rgb(63, 60, 3);
			background-color: rgb(234, 221, 10);
			border-color: rgb(234, 221, 10);
		}
		.btn-danger {
			background-color: #ff3366;
			border-color: #ff3366;
			color: #ffffff;
		}
		.btn-danger:hover,
		.btn-danger:focus {
			background-color: #e50039;
			border-color: #e50039;
		}
		.btn-info {
			background-color: #149dcc;
			border-color: #149dcc;
			color: #ffffff;
		}
		.btn-info:hover,
		.btn-info:focus {
			background-color: #0d6786;
			border-color: #0d6786;
		}
        .btn a {
            text-decoration: none;
            color: white;
        }
        .btn-warning a {
			color: rgb(63, 60, 3);
        }
        .viewAs {
            padding: 0.4rem 1rem;
            width: 180px;
            max-width: 80%;
            margin-top: 0.5rem;
            color: #149dcc;
            background-color: transparent;
            border-color: #149dcc;
        }
        .viewAs:hover,
        .viewAs:focus {
			background-color: #149dcc;
			border-color: #149dcc;
			color: #ffffff;
        }
        
        
        .is-card-left .sectionController .rating {
            text-align: center;
            position: relative;
        }
        .is-card-left .sectionController .rating .ratingLine {
            margin-top: -0.5rem;
        }
        .is-card-left .sectionController .rating .ratingLine .ratingTitle {
            font-size: 0.75rem;
            color: #BDBDBD;
            font-family: "Open Sans", sans-serif;
            font-weight: bold;
            letter-spacing: 0.6px;
            text-transform: uppercase;
        }
        .is-card-left .sectionController .rating .ratingLine .ratingData {
            font-size: 1.3rem;
            color: #212121;
            font-family: "Open Sans", sans-serif;
            font-weight: bold;
            letter-spacing: 1px;
            padding-left: 0.2rem;
        }
        
        .is-card-left .sectionController .rating > input {
            display: none;
        }
        .is-card-left .sectionController .rating > label:before {
            margin: 0.045rem;
            font-size: 1.25rem;
            font-family: FontAwesome;
            display: inline-block;
            content: "\f005";
        }
        .is-card-left .sectionController .rating > .half:before {
            content: "\f089";
            position: absolute;
            top: 0px;
            margin-left: 5px;
        }
        .is-card-left .sectionController .rating > label {
            <?php if ( $average == "" ) : ?><?= "color: #ddd;" ?><?php endif; ?>
            <?php if ( $average != "" ) : ?><?= "color: #FFD700;" ?><?php endif; ?>
        }
        .is-card-left .sectionController .rating > input:checked ~ label {
            color: #ddd;
        }
        .is-card-left .sectionController .rating > input:checked + label {
            color: #FFD700;
        }
	</style>

	<!--  Custom  -->
	<style>
		.is-card-right .title p {
			font-size: 2rem;
			font-weight: 800;
			font-family: "Josefin Slab", monospace;
			color: #0d6786;
			padding: 0;
			margin: 0;
		}
		.is-card-right .title span {
			display: block;
			height: 2px;
			width: 60px;
			background: #0d6786;
			border-radius: 5px;
			margin-bottom: 1.4rem;
		}

		.is-card-right .textContent .textTitle {
			color: #666666;
			font-weight: 600;
			font-size: 1.2rem;
			font-family: "Open Sans", sans-serif;
			padding-right: 5px;
		}
		.is-card-right .textContent .textData {
			font-size: 1.1rem;
		}

		.titleInfo,
		.professionalTitle,
		.tab-contents .overviewTab p {
			padding: 0;
			margin: 0;
		}
		.titleInfo .username {
			font-family: "Josefin Slab", "Arvo", "Open Sans";
			color: #212121;
			font-size: 2.5rem;
			font-weight: bold;
		}
		.titleInfo .fa {
			font-size: 1.1rem;
			padding-left: 10px;
			padding-right: 2px;
			color: #9E9E9E;
            text-transform: capitalize;
		}
		.titleInfo .location {
			font-size: 0.9rem;
			color: #9E9E9E;
			font-family: "Open Sans", sans-serif;
            text-transform: capitalize;
		}
		.professionalTitle {
			font-size: 1rem;
			color: #2962FF;
			font-weight: 600;
			font-family: "Open Sans", sans-serif;
		}
		.tab-contents .overviewTab {
			font-family: "Open Sans", sans-serif;
		}
		.tab-contents .overviewTab .muted {
			text-transform: uppercase;
			font-size: 0.6rem;
			letter-spacing: 0.15rem;
			color: #BDBDBD;
			font-weight: bold;
			padding: 18px 0;
		}
		.tab-contents .overviewTab .dataLine {
			padding: 6px 0px;
		}
		.tab-contents .overviewTab .dataLine .dataTitle {
			font-weight: 600;
			display: inline-block;
			width: 120px;
			color: #757575;
		}
		.tab-contents .overviewTab .dataLine .dataContent {
			font-size: 1.1rem;
		}
        
        .tab .imageCard {
            padding-top: 20px;
        }
        
        .tab .imageCard .card-body {
            padding: 0.8rem;
            background: #2185d5;
        }
        .tab .imageCard .card-body > p {
            font-size: 0.9rem;
            color: #fff;
        }
        .tab .imageCard .card-body:hover {
            text-decoration: none;
        }
        
        .tab .imageCard .card-footer {
            padding: 1rem;
            background: white;
        }
        .tab .imageCard .card-footer > small {
            font-size: 0.95rem;
            color: #757575;
        }
        .tab #portfolioTitle {
            border-bottom: 1px solid #757575;
            display: block;
            width: 100%;
            margin: 30px 15px 0;
            text-align: center;
            font-size: 2rem;
            color: #757575;
            font-weight: 600;
            padding-bottom: 10px;
        }
        
        /*  Tags  */
        .tag {
            border-radius: 3px 0 0 3px;
            display: inline-block;
            height: 24px;
            line-height: 24px;
            padding: 0 20px 0 23px;
            position: relative;
            margin: 0 10px 10px 0;
            text-decoration: none;
            -webkit-transition: color 0.2s;
            color: white;
            background-color: #2185d5;
			font-size: 1rem;
        }
        .tag::before {
            background: #fff;
            border-radius: 10px;
            box-shadow: inset 0 1px rgba(0, 0, 0, 0.25);
            content: '';
            height: 6px;
            left: 10px;
            position: absolute;
            width: 6px;
            top: 10px;
        }
        .tag::after {
            background: #fff;
            border-bottom: 12px solid transparent;
            border-left: 10px solid #eee;
            border-top: 12px solid transparent;
            content: '';
            position: absolute;
            right: 0;
            top: 0;
            border-left-color: #2185d5; 
        }

		.tab-contents .bioTab .bioText {
			font-size: 0.95rem;
			width: 90%;
			color: #757575;
			padding-top: 15px;
		}
        
        /*  Review Tab  */
        .reviewCol {
            margin-bottom: 25px;
        }
        .reviewCol .reviewRating {height: 40px;}
        
        .reviewCol .reviewRatingDivOne {height: 100%; width: 40px; float: left; border-radius: 2px; background: #2185d5; text-align: center;}
        .reviewCol .reviewRatingDivTwo {height: 100%; width: auto; float: left; padding-left: 15px;}
        .reviewCol .reviewRatingDivTwoOne {height:  25px; width:  auto; line-height: 25px;}
        .reviewCol .reviewRatingDivTwoTwo {height: 15px; line-height: 15px;}
        
        .reviewCol .ratingNumber {
            line-height: 40px;
            color: white;
            font-weight: bold;
            font-size: 1.1rem;
        }
        .reviewCol .reviewData {
            margin: 15px 0px 0px;
            color: #757575;
            font-size: 1rem;
            line-height: 1.4rem;
        }
        
        .reviewCol .rating > input {
            display: none;
        }
        .reviewCol .rating > label:before {
            font-size: 1.2rem;
            font-family: FontAwesome;
            display: block;
            content: "\f005";
        }
        .reviewCol .rating > label {
            color: #2185d5;
        }
        .reviewCol .rating > input:checked ~ label {
            color: #ddd;
        }
        .reviewCol .rating > input:checked + label {
            color: #2185d5;
        }
        .pagination .page-link {
            cursor: pointer;
        }
        .pagination .page-link:hover {
            color: black !important;
        }
        .pagination .active .page-link:hover {
            color: white !important;
            cursor: default;
        }
	</style>

	<!--  Tabs  -->
	<style>
		/*----- Tabs -----*/
		.tabs {
			width:100%;
			display:inline-block;
		}
		/*----- Tab Links -----*/
		/* Clearfix */
		.tab-links:after {
			display:block;
			clear:both;
			content:'';
		}
		.tab-links {
			padding-left: 0;
			margin-bottom: 15px;
		}
		.tab-links li {
			margin: 0px 25px 0px 0px;
			float:left;
			list-style:none;
			background:#fff;
			border-bottom: 1.5px solid transparent;
		}
		.tab-links i {
			padding-right: 10px;
			font-size: 0.8em;
		}
		.tab-links a {
			padding:8px 2px 8px;
			display:inline-block;
			font-size:16px;
			font-weight:600;
			color:#4c4c4c;
			transition:all linear 0.15s;
		}
		.tab-links a:hover {
			text-decoration:none;
		}
		li.active a, li.active a:hover {
			color:#4c4c4c;
			border-bottom: 1.5px solid #2185d5;
		}
		.tab {
			display:none;
		}
		.tab.active {
			display:block;
		}
	</style>
</head>

<body>

<?php if ($this->session->flashdata('msg')) : ?>
	<div class="alertMsg">
		<?= $this->session->flashdata('msg') ?>
	</div>
<?php endif; ?>

<section class="menu cid-qH62TEfJyl" once="menu" id="menu1-e">
	<nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-toggleable-sm">

		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<div class="hamburger">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
		</button>

		<div class="menu-logo">
			<div class="navbar-brand">

                    <span class="navbar-caption-wrap">
                        <a class="navbar-caption text-white display-5" href="<?php echo base_url(); ?>">Bidder's Arena</a>
                    </span>
			</div>
		</div>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">

				<li class="nav-item"><a class="nav-link link text-white display-4" href="#"><span class="mbri-cash mbr-iconfont mbr-iconfont-btn"></span>My Wallet<br></a></li>

				<li class="nav-item"><a class="nav-link link text-white display-4" href="#"><span class="mbri-bookmark mbr-iconfont mbr-iconfont-btn"></span>My Projects<br></a></li>

				<li class="nav-item"><a class="nav-link link text-white display-4" href="<?php echo base_url();?>dashboard"><span class="mbri-sites mbr-iconfont mbr-iconfont-btn"></span>Dashboard<br></a></li>

				<li class="nav-item dropdown">
					<a class="nav-link link dropdown-toggle text-white display-4" href="#" data-toggle="dropdown-submenu" aria-expanded="false">
						<span class="profileIconNav"></span><?= $this->session->userdata('username') ?><br></a>

					<div class="dropdown-menu">

						<div class="dropdown">
							<a class="dropdown-item text-white dropdown-toggle display-4" data-toggle="dropdown-submenu" aria-expanded="false">Help</a>

							<div class="dropdown-menu dropdown-submenu">
								<a class="dropdown-item text-white display-4" href="#" aria-expanded="false">FAQ</a>

								<a class="dropdown-item text-white display-4" href="#">Contact Us</a>

								<a class="dropdown-item text-white display-4" href="#" aria-expanded="false">Our Charges</a>
							</div>
						</div>

						<a class="dropdown-item text-white display-4" href="<?php echo base_url();?>profile" aria-expanded="false">My Profile</a>

						<a class="dropdown-item text-white display-4" href="<?php echo base_url();?>logout" aria-expanded="false">Logout</a>

					</div>
				</li>
			</ul>
		</div>
	</nav>
</section>

<div class="container wrapper">
	<div class="row">

		<div class="col-sm-4 col-lg-3 is-card is-card-left">

			<div class="image imgWrapper">
				<div class="image">
					<div class="img">
					</div>
				</div>
			</div>

			<div class="sectionController">
                <fieldset class="rating">
                   
                    <input type="radio" id="starhalf" disabled>
                    <label class="half" for="starhalf"></label>
                    
                    <input type="radio" id="star1" disabled>
                    <label class = "full" for="star1" title="Bad!"></label>
                   
                    <input type="radio" id="star1half" disabled>
                    <label class="half" for="star1half"></label>

                    <input type="radio" id="star2" disabled>
                    <label class = "full" for="star2" title="Average"></label>
                   
                    <input type="radio" id="star2half" disabled>
                    <label class="half" for="star2half"></label>

                    <input type="radio" id="star3" disabled>
                    <label class = "full" for="star3" title="Nice"></label>
                   
                    <input type="radio" id="star3half" disabled>
                    <label class="half" for="star3half"></label>

                    <input type="radio" id="star4" disabled>
                    <label class = "full" for="star4" title="Pretty good"></label>
                   
                    <input type="radio" id="star4half" disabled>
                    <label class="half" for="star4half"></label>
                    
                    <input type="radio" id="star5" disabled>
                    <label class = "full" for="star5" title="Excellent!"></label>
                    
                    <p class="ratingLine">
                        <span class="ratingTitle">Rating: </span>
                        <span class="ratingData">N/A</span>
                    </p>
                </fieldset>
                
                <div class="text-center">
                    <a class="btn btn-info btn-outline-primary viewAs" href="<?php echo base_url();?>profile">Back to Profile</a>
                </div>
			</div>
			
			<hr>

			<div class="buttonSection text-center">
                <a class="btn btn-warning" href="<?php echo base_url(); ?>post-project">Post Project</a>

                <a class="btn btn-danger" href="#">Browse Projects</a>
			</div>

		</div>

		<div class="col-sm-8 col-lg-9 is-card is-card-right">

			<div id="overview" class="cards">

				<p class="titleInfo">
					<span class="username" id="username"><?php if (isset($username)) : ?><?= $username ?><?php endif; ?></span>
					<span><i class="fa fa-map-marker"></i></span>
					<span class="location"><?php if (isset($city)) : ?><?= $city ?><?php endif; ?>, <?php if (isset($country)) : ?><?= $country ?><?php endif; ?></span>
				</p>
				
				<p class="professionalTitle"><?php if (isset($professional_title)) : ?><?= $professional_title ?><?php endif; ?></p>
				<br>
				
				<div class="tabs">
					<ul class="tab-links">
						<li>
							<a href="#tab1"><span><i class="fa fa-eye"></i></span>Bio</a>
						</li>
						<li class="active">
							<a href="#tab2"><span><i class="fa fa-user"></i></span>Overview</a>
						</li>
						<li>
							<a href="#tab3"><span><i class="fa fa-comments"></i></span>Reviews</a>
						</li>
					</ul>

					<div class="tab-contents">
						<div id="tab1" class="tab bioTab">
							<p class="bioText"><?php if (isset($bio)) : ?><?= $bio ?><?php endif; ?></p>
						</div>

						<div id="tab2" class="tab overviewTab active">

							<p class="muted">Worker Information</p>

							<p class="dataLine">
								<span class="dataTitle">Name:</span>
								<span class="dataContent"><?php if (isset($name)) : ?><?= $name ?><?php endif; ?></span>
							</p>
							<p class="dataLine">
								<span class="dataTitle">Qualifications:</span>
								<span class="dataContent"><?php if (isset($qualification)) : ?><?= $qualification ?><?php endif; ?></span>
							</p>
							<p class="dataLine">
								<span class="dataTitle">Skills:</span>
								<span class="dataContent">
									<?php if (isset($skills)) : ?>

										<?php

										$MyArray = explode(',', $skills);

										foreach($MyArray as $skill) {

											echo "<span class=\"tag\">$skill</span>";

										}

										?>

									<?php endif; ?>
                                </span>
							</p>

							<p class="dataLine">
								<span class="dataTitle">Certifications:</span>
                                <?php if(($certification_1 == "") && ($certification_2 == "") && ($certification_3 == "")) : ?><?= '<span class="dataContent">No certificates!</span>' ?><?php endif; ?>
                                
                                <div class="dataContent row">
                                   
                                    <?php if($certification_1 != "") : ?><?= '<div class="certificateFile col col-4 col-sm-3 col-md-3  col-lg-2 offset-lg-2 offset-md-3 text-center" id="pdfFile1" onclick="downloadPdf(this.id);" style="cursor: pointer;"><i class="fa fa-file-text-o" style="display: block; font-size: 5rem; padding-bottom: 0.5rem; color: #2185d5;"></i><span style="display: block; font-size: 0.8rem; padding-bottom: 0.8rem; color: #757575; font-weight: 500;">'?><?= $certification_1 ?><?= '</span></div>' ?><?php endif; ?>
                                    
                                    <?php if($certification_2 != "") : ?><?= '<div class="certificateFile col col-4 col-sm-3 col-md-3 col-lg-2 text-center" id="pdfFile2" onclick="downloadPdf(this.id);" style="cursor: pointer;"><i class="fa fa-file-text-o" style="display: block; font-size: 5rem; padding-bottom: 0.5rem; color: #2185d5;"></i><span style="display: block; font-size: 0.8rem; padding-bottom: 0.8rem; color: #757575; font-weight: 500;">'?><?= $certification_2 ?><?= '</span></div>' ?><?php endif; ?>
                                    
                                    <?php if($certification_3 != "") : ?><?= '<div class="certificateFile col col-4 col-sm-3 col-md-3 col-lg-2 text-center" id="pdfFile3" onclick="downloadPdf(this.id);" style="cursor: pointer;"><i class="fa fa-file-text-o" style="display: block; font-size: 5rem; padding-bottom: 0.5rem; color: #2185d5;"></i><span style="display: block; font-size: 0.8rem; padding-bottom: 0.8rem; color: #757575; font-weight: 500;">'?><?= $certification_3 ?><?= '</span></div>' ?><?php endif; ?>
                                    
                                </div>
                            </p>

							<p class="muted">Personal Information</p>

							<p class="dataLine">
								<span class="dataTitle">Gender:</span>
								<span class="dataContent"><?php if (isset($gender)) : ?><?= $gender ?><?php endif; ?></span>
							</p>
                            
							<div class="row">
                                <?php
                                    if( ($portfolio_link1 != "") || ($portfolio_link2 != "") || ($portfolio_link3 != "") || ($portfolio_link4 != "") )
                                        echo '<p id="portfolioTitle">Portfolio</p>';
                                ?>
                                
                                <?php if ( $portfolio_link1 != "") : ?><?= '<div class="imageCard col col-12 col-md-6 col-lg-3"><div class="card text-center"><img class="card-img-top" src="' ?><?= base_url().$portfolio_pic1; ?><?= '" alt="Portfolio Image" onError="showErrorImage(this);"><a class="card-body" target="_blank" href="http://' ?><?= $portfolio_link1 ?><?= '"><p class="card-text">' ?><?= $portfolio_link1 ?><?= '</p></a><div class="card-footer text-center"><small>' ?><?= $portfolio_desc1 ?><?= '</small></div></div></div>' ?><?php endif; ?>
                                
                                <?php if ( $portfolio_link2 != "") : ?><?= '<div class="imageCard col col-12 col-md-6 col-lg-3"><div class="card text-center"><img class="card-img-top" src="' ?><?= base_url().$portfolio_pic2; ?><?= '" alt="Portfolio Image" onError="showErrorImage(this);"><a class="card-body" target="_blank" href="http://' ?><?= $portfolio_link2 ?><?= '"><p class="card-text">' ?><?= $portfolio_link2 ?><?= '</p></a><div class="card-footer text-center"><small>' ?><?= $portfolio_desc2 ?><?= '</small></div></div></div>' ?><?php endif; ?>
                                
                                <?php if ( $portfolio_link3 != "") : ?><?= '<div class="imageCard col col-12 col-md-6 col-lg-3"><div class="card text-center"><img class="card-img-top" src="' ?><?= base_url().$portfolio_pic3; ?><?= '" alt="Portfolio Image" onError="showErrorImage(this);"><a class="card-body" target="_blank" href="http://' ?><?= $portfolio_link2 ?><?= '"><p class="card-text">' ?><?= $portfolio_link3 ?><?= '</p></a><div class="card-footer text-center"><small>' ?><?= $portfolio_desc3 ?><?= '</small></div></div></div>' ?><?php endif; ?>
                                
                                <?php if ( $portfolio_link4 != "") : ?><?= '<div class="imageCard col col-12 col-md-6 col-lg-3"><div class="card text-center"><img class="card-img-top" src="' ?><?= base_url().$portfolio_pic4; ?><?= '" alt="Portfolio Image" onError="showErrorImage(this);"><a class="card-body" target="_blank" href="http://' ?><?= $portfolio_link4 ?><?= '"><p class="card-text">' ?><?= $portfolio_link4 ?><?= '</p></a><div class="card-footer text-center"><small>' ?><?= $portfolio_desc4 ?><?= '</small></div></div></div>' ?><?php endif; ?>
							    
							</div>
                            
						</div>
						
						<div id="tab3" class="tab reviewsTab">
                            
                            <!--  Div for reviews  -->
						    <div class="row" style="margin-top: 40px;"></div>
						    
						    <?php if( $pages > 1 ) : ?><?= '<ul class="pagination" id="pagination">'; ?>
                                <?php
                                    for($i=0; $i<$pages; $i++) {
                                        echo '<li class="page-item';
                                        if( $i == 0)
                                            echo ' active';
                                        echo '"><a id="pageLink'.($i+1).'" class="page-link" onclick="loadReview('.($i*4).')">'.($i+1).'</a></li>';
                                    }
                                ?>
                            <?= '</ul>' ?>
                            <?php endif; ?>
                            
                            <?php
                                if( $reviews == "0" ) {
                                    echo "<span style='font-weight: 600; color: #757575;'>No reviews posted yet!</span>";
                                }
                            ?>
						</div>
					</div>
				</div>

			</div>

		</div>

	</div>
</div>

<script src="<?php echo base_url(); ?>assets/assets/web/assets/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/assets/dropdown/js/script.min.js"></script>

<script>

	$(document).ready(function() {
		$('.tabs .tab-links a').on('click', function(e)  {
			var currentAttrValue = jQuery(this).attr('href');

			// Show/Hide Tabs
			$('.tabs ' + currentAttrValue).fadeIn(400).siblings().hide();

			// Change/remove current tab to active
			$(this).parent('li').addClass('active').siblings().removeClass('active');

			e.preventDefault();
		});
        
        // ---- Show average rating --- //
        <?php if ( $average != "" ) : ?><?= 'var average = parseFloat(' ?><?= $average ?><?= ');average = Math.round(average*2)/2;if( average % 1 == 0 ) {$(".rating #star" + average).attr("checked", "checked");$(".rating .ratingData").html(average + ".0");}else {$(".rating #star" + parseInt(average) + "half").attr("checked", "checked");$(".rating .ratingData").html(average);}' ?><?php endif; ?>
	});
	$(document).ready(function () {
		var pass = 0;
		var username = $('#username').text();
		$.ajax ({
			url: "<?php echo base_url(); ?>profile/loadReviews",
			method: "post",
			data: {offset: pass, username: username},
			success: function (data) {
				$("#tab3 .row").html("");
                $("#tab3 .row").html(data);
			}
		});
	});

    // ------------- Downloads the pdf on clicking pdf icon ----------- //
    function downloadPdf(id) {
        if( id == "pdfFile1") {
            window.open('<?php if (isset($certification_1_pdf)) : ?><?= base_url().$certification_1_pdf ?><?php endif; ?>');
        } else if( id == "pdfFile2" ) {
            window.open('<?php if (isset($certification_2_pdf)) : ?><?= base_url().$certification_2_pdf ?><?php endif; ?>');
        } else if( id == "pdfFile3" ) {
            window.open('<?php if (isset($certification_3_pdf)) : ?><?= base_url().$certification_3_pdf ?><?php endif; ?>');
        }
    }
    
	// ------------- Load Reviews Ajax ----------- //
    function loadReview(offset) {
        var toActivate = (parseInt(offset))/4 + 1;
        if( $("#pagination #pageLink" + toActivate).parent().hasClass("active") )
            return false;
        $("#pagination li").removeClass("active");
        $("#pagination #pageLink" + toActivate).parent().addClass("active");
        
    	var pass = parseInt(offset);
    	var username = $('#username').text();
		$.ajax ({
			url: "<?php echo base_url(); ?>profile/loadReviews",
			method: "post",
			data: {offset: pass, username: username},
			success: function (data) {
				$("#tab3 .row").html("");
				$("#tab3 .row").html(data);
			}
		});
	}
    
    // ------------- Shows image not found error on portfolii ----------- //
    function showErrorImage(source) {
        source.src = '<?= base_url(); ?>assets/Images/NoPicAvailable.jpg';
        source.onerror = "";
        return true;
    }
</script>
</body>

</html>
