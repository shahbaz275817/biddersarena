<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Bidder's Arena | Profile Setting</title>

	<!-- Bulma files -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.1/css/bulma.min.css">
	<!-- Fonts -->
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Josefin+Slab,Arvo,Open+Sans">
	<!-- Datepicker -->
	<link href="http://t1m0n.name/air-datepicker/dist/css/datepicker.min.css" rel="stylesheet" type="text/css">

	<style>
		body {
			overflow-x: hidden;
			user-select: none;
		}
        ::-webkit-scrollbar {
            width: 0px;
            background: transparent;
        }
		.navbar-brand a {
			font-family: "Josefin Slab", "Open Sans", sans-serif !important;
			font-size: 1.5em;
			font-weight: bold;
			padding-left: 20px;
			padding-right: 20px;
		}

		.columns:last-child {
			margin-bottom: 0px;
		}
		.hero-foot .footerCol{
			background: rgba(21, 101, 192, 0.8);
		}

		.userImage {
			left: 50%;
			transform: translateX(-50%);
			border-radius: 50%;
			overflow: hidden;
			cursor: pointer;
		}
		.userImage a {
			position: absolute;
			bottom: 0px;
			background: rgba(0, 0, 0, 0.6);
			display: block;
			opacity: 0;
			width: 96px;
			height: 96px;
			line-height: 96px;
			text-align: center;
			color: white !important;
			font-size: 1.5rem;
			transition: all 0.3s ease-in;
		}
		.userImage a:hover {
			opacity: 1;
		}

		.settingList {
			font-family: "Open Sans", sans-serif;
			text-align: center;
			letter-spacing: 1px;
			padding-left: 10%;
			padding-right: 10%;
		}
		.settingList .optionsDivider {
			margin-bottom: 20px;
			margin-top: 20px;
			width: 80%;
			margin-left: 10%;
		}
		.settingList .options {
			border-radius: 5px;
			height: 30px;
			line-height: 30px;
			transition: font-size 0.2s ease;
			cursor: pointer;
		}
		.settingList .options:hover {
			background: #209cee;
			color: white;
			font-weight: 500;
			text-transform: uppercase;
		}

		/* For Firefox */
		input[type='number'] {
			-moz-appearance:textfield;
		}
		/* Webkit browsers like Safari and Chrome */
		input[type=number]::-webkit-inner-spin-button,
		input[type=number]::-webkit-outer-spin-button {
			-webkit-appearance: none;
			margin: 0;
		}

		.field-label label,
		.label label {
			font-family: "Open Sans", sans-serif;
			font-weight: 700;
			font-size: 0.9rem !important;
			color: #4a4a4a;
		}
		.control input,
		.control textarea,
		.control select {
			font-family: "Arvo";
			letter-spacing: 1px;
			height: 35px;
			font-size: 0.9rem;
			color: #4a4a4a;
		}
		.control .non-resizable {
			resize: none;
			overflow: hidden;
		}
		.control button {
			float: right;
		}
		.clearfix {
			clear: both;
		}

		.skill {
			background: #209cee;
			color: white;
			padding: 4px 15px;
			border-radius: 20px;
			width: 80%;
			margin: 10px 10%;
			text-transform: capitalize;
		}

		#overview form .is-gayab,
		#qualifications form .is-gayab,
		#bank form .is-gayab {
			display: none;
		}

        /* For notification */
		.alertMsg {
			position: absolute;
			z-index: 100;
			height: 52px;
            width: auto;
			text-align: center;
			background: #4CAF50;
			font-size: 16px;
            padding: 0px 20px;
            left: 50%;
            transform: translateX(-50%);
            display: table;
            border-radius: 0px 0px 5px 5px;
            
            -webkit-box-shadow: 0 2px 3px rgba(10,10,10,.1), 0 0 0 1px rgba(10,10,10,.1);
            box-shadow: 0 2px 3px rgba(10,10,10,.1), 0 0 0 1px rgba(10,10,10,.1);
            color: #4a4a4a;

			animation: fadeOutThreeSec 3000ms ease-out forwards;
		}
        @media screen and (max-width: 526px) {
            .alertMsg {
                border-radius: 0px;
            }
        }
        .alertMsg span {
			color: #F5F5F5;
            display: table-cell;
            vertical-align: middle;
        }
		@keyframes fadeOutThreeSec {
			0%{
                top: 0px;
			}
			75%{
                top: 0px;
			}
			100%{
                top: -62px;
			}
		}
        
        .modal .uploadImg {
            background: white;
            padding: 20px;
            font-family: "Open Sans", sans-serif;
        }
        .modal .uploadImg form .dpText {
            text-align: center;
            font-size: 0.8em;
        }

	</style>
</head>

<body>

<!-- For notification -->
<?php if ($this->session->flashdata('msg')) : ?>
    <div class="alertMsg">
        <?= '<span>'.$this->session->flashdata('msg').'</span>' ?>
    </div>
<?php endif; ?>

<!-- Modal for uploading profile picture -->
<div class="modal">
    <div class="modal-background"></div>
        <div class="modal-content">
            <div class="uploadImg">
                <p class="is-size-5 has-text-centered has-text-weight-semibold">Upload Image</p><br>
                
                <form method="POST" action="user/profile/imageUpload" enctype="multipart/form-data" name="frmFile" id="frmFile" onSubmit="return validate();">
                   
                    <div class="imgCont">
                        <img id="imgOutput" alt="No file chosen!" style="position: relative; left: 50%; transform: translateX(-50%); max-width: 80%; max-height: 306px; margin-bottom: 10px; margin-top: -5px;"/>
                    </div>
                    
                    <div class="field">
                        <div class="file is-centered is-info">
                            <label class="file-label">
                               
                                <input class="file-input demoInputBox" type="file" name="myimage" accept="image/*" onchange="loadFile(event)" id="file">
                                
                                <span class="file-cta">
                                    <span class="file-icon">
                                        <i class="fa fa-upload"></i>
                                    </span>
                                    <span class="file-label">
                                        Choose Image
                                    </span>
                                </span>
                                
                            </label>
                        </div>
                    </div>
                    
                    <div class="control">
                        <input type="submit" name="submit_image" value="Upload Image" class="button is-info is-outlined" style="left: 50%; transform: translateX(-50%);" id="btnSubmit" disabled>
                    </div>
                     <span id="file_error"></span>
                </form>
                
            </div>
        </div>
    <button class="modal-close is-large" aria-label="close" onclick="hideModal();"></button>
</div>
<script>
    var loadFile = function(event) {
        var output = document.getElementById('imgOutput');
        output.src = URL.createObjectURL(event.target.files[0]);
    };
</script>

<section class="hero is-info is-fullheight">
	<div class="hero-head">
		<nav class="navbar is-transparent">
			<div class="navbar-brand">

				<a class="navbar-item" href="<?php echo base_url();?>">
					Bidder's Arena
				</a>
				<div class="navbar-burger burger" data-target="navbarDropDown">
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>

			<div id="navbarDropDown" class="navbar-menu">
				<div class="navbar-end">
					<a class="navbar-item" href="<?php echo base_url();?>">Home</a>
					<a class="navbar-item" href="<?php echo base_url();?>dashboard">Dashboard</a>

					<a class="navbar-item" href="<?php echo base_url();?>profile">Profile</a>
					<a class="navbar-item" href="#">Contact Us</a>
					<div class="navbar-item has-dropdown is-hoverable" style="color: white;">
						<a class="navbar-link" href="#">
							Categories
						</a>

						<div class="navbar-dropdown is-boxed" style="color: #343434">
							<a class="navbar-item" href="#">Notifications</a>
							<a class="navbar-item" href="#">Settings</a>
							<hr class="navbar-divider">
							<a class="navbar-item is-active" href="<?php echo base_url();?>logout">Logout</a>
						</div>
					</div>
				</div>
			</div>
		</nav>
	</div>

	<div class="hero-body">
		<div class="container">

			<div class="columns profileBg">
				<div class="column is-10 is-offset-1">

					<div class="tile is-ancestor">
						<div class="tile">

							<div class="tile is-4">
								<div class="tile is-vertical">

                                    <div class="tile is-parent" style="max-height: 100px;">
                                        <div class="tile is-child box">

                                            <div class="photo">
                                               <figure class="image userImage is-96x96">
                                                    <?php if ($photo == NULL) : ?><img src="<?php echo base_url(); ?>assets/Images/default-profile.jpg"><?php endif; ?>
													<?php if ($photo != NULL) : ?><img src="data:image/jpeg;base64,<?php echo base64_encode($photo); ?>"><?php endif; ?>

                                                    <a href="#" onclick="showModal();"><i class="fa fa-cog"></i></a>
                                                </figure>
                                            </div>

                                            <div class="username has-text-centered is-size-5 has-text-weight-semibold" style="padding-top: 10px;"><?php if (isset($username)) : ?><?= $username ?><?php endif; ?></div>

                                        </div>
                                    </div>

									<div class="tile is-parent">
										<div class="tile is-child box settingList">
											<p id="overview" class="options" onclick="showThis(this.id);">Overview</p>

											<p id="skills" class="options" onclick="showThis(this.id);">Skills</p>

											<p id="qualifications" class="options" onclick="showThis(this.id);">Qualifications</p>

											<p id="bank" class="options" onclick="showThis(this.id);">Bank Details</p>

											<p id="projects" class="options" onclick="showThis(this.id);">Projects</p>

											<hr class="optionsDivider">

											<p class="options"><a href="#">Post Project</a></p>
											<p class="options"><a href="#">Browse Projects</a></p>
										</div>
									</div>
								</div>
							</div>

							<div class="tile is-parent infoTiles">
								<div class="tile is-child box" id="overview">

									<form action="<?php echo base_url();?>profile/updateOverviewProfile" method="post">

										<div class="field is-horizontal">
											<div class="field-label is-normal">
												<label class="label">Name</label>
											</div>
											<div class="field-body">
												<div class="field">
													<div class="control">
														<input class="editable input is-info" type="text" name="name" placeholder="Enter your name" value="<?php if (isset($name)) : ?><?= $name ?><?php endif; ?>" disabled>
													</div>
												</div>
											</div>
										</div>

										<div class="field is-horizontal">
											<div class="field-label is-normal">
												<label class="label">Username</label>
											</div>
											<div class="field-body">
												<div class="field">
													<div class="control">
														<input class="input is-info" type="text" name="username" placeholder="Enter your username" value="<?php if (isset($username)) : ?><?= $username ?><?php endif; ?>" disabled>
													</div>
												</div>
											</div>
										</div>

										<div class="field is-horizontal">
											<div class="field-label is-normal">
												<label class="label">Email</label>
											</div>
											<div class="field-body">
												<div class="field">
													<div class="control">
														<input class="input is-info" type="text" name="email" placeholder="Enter your email" value="<?php if (isset($email)) : ?><?= $email ?><?php endif; ?>" disabled>
													</div>
												</div>
											</div>
										</div>

										<div class="field is-horizontal">
											<div class="field-label is-normal">
												<label class="label">Gender/DOB</label>
											</div>
											<div class="field-body">
												<div class="field">
													<div class="control">
														<input class="input is-info" type="text" name="gender" placeholder="Enter your Gender" value="<?php if (isset($gender)) : ?><?= $gender ?><?php endif; ?>" disabled>
													</div>
												</div>

												<div class="field">
													<div class="control guide">
														<input value="<?php if (isset($dob)) : ?><?php $date = strtotime($dob);
															$date = date('d/m/Y', $date); echo $date; ?><?php endif; ?>" type='text' placeholder="dd/mm/yyyy" class="editable input is-info datepicker-here" data-language="en" data-date-format="dd/mm/yyyy" data-view="years" name="dob" disabled>
													</div>
												</div>
											</div>
										</div>

										<div class="field is-horizontal">
											<div class="field-label is-normal">
												<label class="label">Phone</label>
											</div>
											<div class="field-body">
												<div class="field">
													<div class="control guide">
														<input type='number' placeholder="Enter your phone number" class="editable input is-info" name="phone" pattern="\d*" maxlength="10" min="0000000000" max="9999999999" value="<?php if (isset($phone)) : ?><?= $phone ?><?php endif; ?>" disabled>
													</div>
												</div>
											</div>
										</div>

										<div class="field is-horizontal">
											<div class="field-label is-normal">
												<label class="label">Address</label>
											</div>
											<div class="field-body">
												<div class="field">
													<div class="control">
														<textarea class="editable textarea is-info non-resizable" name="add1" placeholder="Enter your address" rows="1" disabled><?php if (isset($address1)) : ?><?= $address1 ?><?php endif; ?></textarea>
													</div>
												</div>
											</div>
										</div>

										<div class="field is-horizontal">
											<div class="field-label is-normal">
												<label class="label">Locality</label>
											</div>
											<div class="field-body">
												<div class="field">
													<div class="control">
														<textarea class="editable textarea is-info non-resizable" name="add2" placeholder="Enter your locality" rows="1" disabled><?php if (isset($address2)) : ?><?= $address2 ?><?php endif; ?></textarea>
													</div>
												</div>
											</div>
										</div>

										<div class="field is-horizontal">
											<div class="field-label is-normal">
												<label class="label">City/State</label>
											</div>
											<div class="field-body">
												<div class="field">
													<div class="control">
														<input class="editable input is-info" type="text" name="city" placeholder="Enter your city" value="<?php if (isset($city)) : ?><?= $city ?><?php endif; ?>" disabled>
													</div>
												</div>

												<div class="field">
													<div class="control">
														<input class="editable input is-info" type="text" name="state" placeholder="Enter your state" value="<?php if (isset($state)) : ?><?= $state ?><?php endif; ?>" disabled>
													</div>
												</div>
											</div>
										</div>

										<div class="field is-horizontal">
											<div class="field-label is-normal">
												<label class="label">Country</label>
											</div>
											<div class="field-body">
												<div class="field">
													<div class="control">
														<input class="editable input is-info" type="text" name="country" placeholder="Enter your country" value="<?php if (isset($country)) : ?><?= $country ?><?php endif; ?>" disabled>
													</div>
												</div>
											</div>
										</div>

										<!-- Buttons -->

										<div class="control edit">
											<button type="button" class="button is-danger" onclick="edit('overview');">
                                                <span class="icon">
                                                    <i class="fa fa-pencil"></i>
                                                </span>
												<span>Edit</span>
											</button>
										</div>

										<!-- page reload on save -->
										<div class="control is-gayab save">
											<button type="submit" name="overviewSave" class="button is-info">
                                               <span class="icon">
                                                    <i class="fa fa-floppy-o"></i>
                                                </span>
												<span>Save</span>
											</button>
										</div>

										<div class="control is-gayab cancel" style="margin-right: 90px;">
											<button type="button" class="button is-danger" onclick="cancel('overview');">
                                                <span class="icon">
                                                    <i class="fa fa-ban"></i>
                                                </span>
												<span>Cancel</span>
											</button>
										</div>
										<div class="clearfix"></div>

									</form>
								</div>

								<div class="tile is-child box" id="skills">

									<div class="checks has-text-centered">
										<p class="is-size-6 has-text-weight-semibold">You have chosen the following skills</p>

										<!-- Skills to be added from the database -->

										<?php if (isset($skills)) : ?>

											<?php

											$MyArray = explode(',', $skills);

											foreach($MyArray as $skill) {

												echo "<div class=\"skill\">$skill</div>";

											}

											?>

										<?php endif; ?>

									</div>

								</div>

								<div class="tile is-child box" id="qualifications">
                                    
                                    <!-- https://www.w3schools.com/php/php_file_upload.asp -->
									<form action="user/profile/uploadcertificates" method="post" enctype="multipart/form-data">
                                       
                                        <div class="field">
                                            <label class="label">Professional Title</label>
                                            <div class="control">
                                                <div class="select is-info" style="width: 100%;">
                                                    <select name="professionalTitle" style="width: 100%;">
                                                        <option value='-1'>Professional Title</option>
                                                        <option value='Mr'>Mr.</option>
                                                        <option value='Mrs'>Mrs.</option>
                                                        <option value='Ms'>Ms.</option>
                                                        <option value='Master'>Master.</option>
                                                        <option value='Hon'>Hon.</option>
                                                        <option value='Dr'>Dr.</option>
                                                        <option value='Prof'>Prof.</option>
                                                        <option value='Er'>Er.</option>
                                                        <option value='Advocate'>Advocate</option>
                                                        <option value='Justice'>Justice</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                       
                                        <div class="field">
                                            <label class="label">Qualifications</label>
                                            <div class="control">
                                                <div class="select is-info" style="width: 100%;">
                                                    <select name="qualifications" style="width: 100%;">
                                                        <option value='-1'>Qualifications</option>
                                                        <option value='10th (High School)'>10th (High School)</option>
                                                        <option value='12th (Intermediate)'>12th (Intermediate)</option>
                                                        <option value='Air Crew'>Air Crew</option>
                                                        <option value='Air Hostess'>Air Hostess</option>
                                                        <option value='Animation and Multimedia'>Animation and Multimedia</option>
                                                        <option value='Animation Film Making'>Animation Film Making</option>
                                                        <option value='Application Software Development – DASD'>Application Software Development – DASD</option>
                                                        <option value='B.Arch'>B.Arch</option>
                                                        <option value='B.Com'>B.Com</option>
                                                        <option value='B.Des'>B.Des</option>
                                                        <option value='B.Sc. Dairy Technology'>B.Sc. Dairy Technology</option>
                                                        <option value='B.Sc. Degree'>B.Sc. Degree</option>
                                                        <option value='B.Sc. Home Science'>B.Sc. Home Science</option>
                                                        <option value='B.Sc. Nursing'>B.Sc. Nursing</option>
                                                        <option value='BA'>BA</option>
                                                        <option value='Bachelor of Naturopathy &amp; Yogic Science (BNYS)'>Bachelor of Naturopathy &amp; Yogic Science (BNYS)</option>
                                                        <option value='Bachelor of Pharmacy'>Bachelor of Pharmacy</option>
                                                        <option value='Bachelor of Physiotherapy'>Bachelor of Physiotherapy</option>
                                                        <option value='Bachelor of Veterinary Science &amp; Animal Husbandry (B.VSc AH)'>Bachelor of Veterinary Science &amp; Animal Husbandry (B.VSc AH)</option>
                                                        <option value='BAMS (Ayurvedic)'>BAMS (Ayurvedic)</option>
                                                        <option value='BCA'>BCA</option>
                                                        <option value='BDS'>BDS</option>
                                                        <option value='BHMS (Homoeopathy)'>BHMS (Homoeopathy)</option>
                                                        <option value='Biotechnology'>Biotechnology</option>
                                                        <option value='BMLT (Medical Lab Technology)'>BMLT (Medical Lab Technology)</option>
                                                        <option value='BOT (Occupational Therapy)'>BOT (Occupational Therapy)</option>
                                                        <option value='BUMS (Unani)'>BUMS (Unani)</option>
                                                        <option value='CA Program'>CA Program</option>
                                                        <option value='Computer Courses'>Computer Courses</option>
                                                        <option value='Computer Hardware'>Computer Hardware</option>
                                                        <option value='CS Program'>CS Program</option>
                                                        <option value='Cutting and Tailoring'>Cutting and Tailoring</option>
                                                        <option value='Defence (Navy, Army, Air force)'>Defence (Navy, Army, Air force)</option>
                                                        <option value='Designing Courses'>Designing Courses</option>
                                                        <option value='Diploma in Beauty Culture &amp; Hair Dressing'>Diploma in Beauty Culture &amp; Hair Dressing</option>
                                                        <option value='Drawing and Painting'>Drawing and Painting</option>
                                                        <option value='Dress Designing – DDD'>Dress Designing – DDD</option>
                                                        <option value='Education/ Teaching'>Education/ Teaching</option>
                                                        <option value='Engineering (B.E/ B.Tech)'>Engineering (B.E/ B.Tech)</option>
                                                        <option value='Environmental Science'>Environmental Science</option>
                                                        <option value='Event Management'>Event Management</option>
                                                        <option value='Fashion Designing – DFD'>Fashion Designing – DFD</option>
                                                        <option value='Fashion Technology'>Fashion Technology</option>
                                                        <option value='Film Arts &amp; A/V Editing'>Film Arts &amp; A/V Editing</option>
                                                        <option value='Film Making &amp; Digital Video Production'>Film Making &amp; Digital Video Production</option>
                                                        <option value='Film/ Television Courses'>Film/ Television Courses</option>
                                                        <option value='Foreign Language Courses'>Foreign Language Courses</option>
                                                        <option value='General Nursing'>General Nursing</option>
                                                        <option value='Graphic Designing'>Graphic Designing</option>
                                                        <option value='Hospital &amp; Health Care Management'>Hospital &amp; Health Care Management</option>
                                                        <option value='Hotel Management'>Hotel Management</option>
                                                        <option value='HR Training'>HR Training</option>
                                                        <option value='ICWA Program'>ICWA Program</option>
                                                        <option value='Information Technology'>Information Technology</option>
                                                        <option value='Integrated M.Sc'>Integrated M.Sc</option>
                                                        <option value='LLB (Bachelor of Law)'>LLB (Bachelor of Law)</option>
                                                        <option value='Mass Communication'>Mass Communication</option>
                                                        <option value='Mass Media and Creative Writing'>Mass Media and Creative Writing</option>
                                                        <option value='MBBS'>MBBS</option>
                                                        <option value='Media/ Journalism Courses'>Media/ Journalism Courses</option>
                                                        <option value='Paramedical Courses'>Paramedical Courses</option>
                                                        <option value='Physical Medicine and Rehabilitation'>Physical Medicine and Rehabilitation</option>
                                                        <option value='Print Media Journalism &amp; Communications'>Print Media Journalism &amp; Communications</option>
                                                        <option value='Textile Designing – DTD'>Textile Designing – DTD</option>
                                                        <option value='Travel &amp; Tourism Courses'>Travel &amp; Tourism Courses</option>
                                                        <option value='Web Designing'>Web Designing</option>
                                                        <option value=''></option>                           
                                                   </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="field certificate-field">
                                            <div class="label">
                                                <label class="label">Certificates</label>
                                            </div>
                                            <div class="field-body">
                                                <div class="field is-grouped is-grouped-multiline">

                                                   <div class="control">
                                                        <div class="file is-info">
                                                            <label class="file-label">
                                                                <input class="file-input" type="file" name="certificateFile1" accept="file_extension|.pdf">
                                                                <span class="file-cta">
                                                                    <span class="file-icon">
                                                                        <i class="fa fa-upload"></i>
                                                                    </span>
                                                                    <span class="file-label">
                                                                        Upload certificate
                                                                    </span>
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="control is-expanded">
                                                        <input class="input is-info" type="text" name="certificate1" placeholder="Describe certificate">
                                                    </div>

                                                    <div class="control addButton">
                                                        <a class="button is-outlined is-info" onclick="addCertificateInput();">
                                                            <span><i class="fa fa-plus"></i></span>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

										<!-- Buttons -->

										<div class="control edit" style="padding-top: 15px;">
											<button type="button" class="button is-danger" onclick="edit('qualifications');">
                                                <span class="icon">
                                                    <i class="fa fa-pencil"></i>
                                                </span>
												<span>Edit</span>
											</button>
										</div>

										<!-- page reload on save -->
										<div class="control is-gayab save" style="padding-top: 15px;">
											<button type="submit" name="certificateSubmit" value="upload" class="button is-info">
                                               <span class="icon">
                                                    <i class="fa fa-floppy-o"></i>
                                                </span>
												<span>Save</span>
											</button>
										</div>

										<div class="control is-gayab cancel" style="margin-right: 90px;">
											<button type="button" class="button is-danger" onclick="cancel('qualifications');">
                                                <span class="icon">
                                                    <i class="fa fa-ban"></i>
                                                </span>
												<span>Cancel</span>
											</button>
										</div>
										<div class="clearfix"></div>

									</form>

								</div>

								<!-- KYC Card -->
								<div class="tile is-child box" id="bank">
									<form action="<?php echo base_url();?>profile/updateKYCProfile" method="post">

										<div class="field">
											<div class="label">
												<label class="label">Account Name</label>
											</div>
											<div class="field-body">
												<div class="field">
													<div class="control">
														<input class="editable input is-info" type="text" name="accountName" placeholder="Enter bank account name" value="<?php if (isset($bank_account_name)) : ?><?= $bank_account_name ?><?php endif; ?>" disabled>
													</div>
												</div>
											</div>
										</div>

										<div class="field">
											<div class="label">
												<label class="label">Account Number</label>
											</div>
											<div class="field-body">
												<div class="field">
													<div class="control">
														<input class="editable input is-info" type="text" name="accountNumber" placeholder="Enter bank account number" value="<?php if (isset($account_number)) : ?><?= $account_number ?><?php endif; ?>" disabled>
													</div>
												</div>
											</div>
										</div>

										<div class="field">
											<div class="label">
												<label class="label">Bank Name</label>
											</div>
											<div class="field-body">
												<div class="field">
													<div class="control">
														<input class="editable input is-info" type="text" name="bankName" placeholder="Enter bank name" value="<?php if (isset($bank_name)) : ?><?= $bank_name ?><?php endif; ?>" disabled>
													</div>
												</div>
											</div>
										</div>

										<div class="field">
											<div class="label">
												<label class="label">IFSC Code</label>
											</div>
											<div class="field-body">
												<div class="field">
													<div class="control guide">
														<input type='text' placeholder="Enter bank's IFSC code" class="editable input is-info" name="ifsc" maxlength="11" value="<?php if (isset($ifsc)) : ?><?= $ifsc ?><?php endif; ?>" disabled>
													</div>
												</div>
											</div>
										</div>

										<div class="field">
											<div class="label">
												<label class="label">Address</label>
											</div>
											<div class="field-body">
												<div class="field">
													<div class="control">
														<textarea class="editable textarea is-info non-resizable" name="bankAdd" rows="3" placeholder="Enter bank address" disabled><?php if (isset($bank_address)) : ?><?= $bank_address ?><?php endif; ?></textarea>
													</div>
												</div>
											</div>
										</div>

										<!-- Buttons -->

										<div class="control edit">
											<button type="button" class="button is-danger" onclick="edit('bank');">
                                                <span class="icon">
                                                    <i class="fa fa-pencil"></i>
                                                </span>
												<span>Edit</span>
											</button>
										</div>

										<!-- page reload on save -->
										<div class="control is-gayab save">
											<button type="submit" name="bankSubmit" class="button is-info">
                                               <span class="icon">
                                                    <i class="fa fa-floppy-o"></i>
                                                </span>
												<span>Save</span>
											</button>
										</div>

										<div class="control is-gayab cancel" style="margin-right: 90px;">
											<button type="button" class="button is-danger" onclick="cancel('bank');">
                                                <span class="icon">
                                                    <i class="fa fa-ban"></i>
                                                </span>
												<span>Cancel</span>
											</button>
										</div>
										<div class="clearfix"></div>

									</form>
								</div>

								<div class="tile is-child box" id="projects">
									projects
								</div>
							</div>

						</div>
					</div>

				</div>
			</div>

		</div>
	</div>

	<div class="hero-foot">
		<div class="footerCol columns is-mobile has-text-centered">
			<div class="column">
				<p>
					<strong>© </strong><a href="<?php echo base_url();?>">Bidder's Arena</a>.
				</p>
			</div>
			<div class="column">
				<p>
					by <a href="#">ChamberOfDevelopers</a>
				</p>
			</div>
		</div>
	</div>
</section>

<!-- JQuery -->
<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
<!-- Datepicker (not required) -->
<script src="http://t1m0n.name/air-datepicker/dist/js/datepicker.min.js"></script>
<script src="http://t1m0n.name/air-datepicker/dist/js/i18n/datepicker.en.js"></script>

<!-- Script to make hamburger menu working -->
<script>
	document.addEventListener('DOMContentLoaded', function () {
		// Get all "navbar-burger" elements
		var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

		// Check if there are any navbar burgers
		if ($navbarBurgers.length > 0) {

			// Add a click event on each of them
			$navbarBurgers.forEach(function ($el) {
				$el.addEventListener('click', function () {

					// Get the target from the "data-target" attribute
					var target = $el.dataset.target;
					var $target = document.getElementById(target);

					// Toggle the class on both the "navbar-burger" and the "navbar-menu"
					$el.classList.toggle('is-active');
					$target.classList.toggle('is-active');
				});
			});
		}

	});
</script>

<script>
	var tileArray = ['#qualifications', '#bank', '#overview', '#projects', '#skills'];

	var count = 0;

	$(document).ready(function() {
		//var url = $(location).attr('href');
		//url = url.substring(url.lastIndexOf('/')+1);

		$(".infoTiles .tile").hide();
//		if(url == "updateKYCProfile")
//			$(".infoTiles #bank").show();
//		else if(url == "updateOverviewProfile")
//			$(".infoTiles #overview").show();
//		else
        $(".infoTiles #overview").show();
        
        $('input[name="myimage"]').change(function(){
            var fullpath = $(this).val();
            var backslash=fullpath.lastIndexOf("\\");
            var filename = fullpath.substr(backslash+1);
            $(".dpText").html(filename);
            
            $('input[name="submit_image"]').removeAttr('disabled');
        });
	});
	function showThis(id) {
		for(var i = 0; i < 6; i++) {
			if( tileArray[i] == "#"+id) {
				$(".infoTiles " + tileArray[i]).show();
			}else {
				$(".infoTiles " + tileArray[i]).hide();
			}
		}
	}
	function addCertificateInput() {
		if (count < 2) {
			$(".addButton").remove();

			$(".certificate-field").append('<div class="field-body" style="margin-top: 15px;"><div class="field is-grouped is-grouped-multiline"><div class="control"><div class="file is-info"><label class="file-label"><input class="file-input" type="file" name="certificateFile'+(count+2)+'" accept="file_extension|.pdf"><span class="file-cta"><span class="file-icon"><i class="fa fa-upload"></i></span><span class="file-label">Upload certificate</span></span></label></div></div><div class="control is-expanded"><input class="input is-info" type="text" name="certificate'+(count+2)+'" placeholder="Describe certificate"></div><div class="control addButton"><a class="button is-outlined is-info" onclick="addCertificateInput();"><span><i class="fa fa-plus"></i></span></a></div></div></div>');

			count++;
		}
		if (count == 2) {
			$(".addButton").remove();
		}
	}
	function edit(id) {
		$("#"+id+" .edit button").toggleClass("is-loading");

		setTimeout(function(){
			$("#"+id+" .edit").addClass("is-gayab");
			$("#"+id+" .save").removeClass("is-gayab");
			$("#"+id+" .cancel").removeClass("is-gayab");
			$("#"+id+" .edit button").toggleClass("is-loading");

			$('#'+id+' form .field .editable').removeAttr('disabled');
		}, 500);
	}
	function cancel(id) {
		$("#"+id+" .cancel button").toggleClass("is-loading");

		setTimeout(function(){
			$("#"+id+" .edit").removeClass("is-gayab");
			$("#"+id+" .save").addClass("is-gayab");
			$("#"+id+" .cancel").addClass("is-gayab");
			$("#"+id+" .cancel button").toggleClass("is-loading");

			$('#'+id+' form .field .editable').attr('disabled', 'disabled');
		}, 500);
	}
    function showModal() {
        $(".modal").addClass("is-active");
    }
    function hideModal() {
        $(".modal").removeClass("is-active");
        
        var output = document.getElementById('imgOutput');
        output.src = "";
    }
    
    function validate() {
        $("#file_error").html("");
        $(".demoInputBox").css("border-color","#F0F0F0");
        var file_size = $('#file')[0].files[0].size;
        if(file_size>2097152) {
            $("#file_error").html("File size is greater than 2MB");
            $(".demoInputBox").css("border-color","#FF0000");
            return false;
        } 
        return true;
    }
</script>
</body>

</html>
