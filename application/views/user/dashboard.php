<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <meta name="description" content="">
    <title>Bidder's Arena | Dashboard</title>
    
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/web/assets/mobirise-icons/mobirise-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/dropdown/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/mobirise/css/mbr-additional.css" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- Fonts -->
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Josefin+Slab,Arvo,Open+Sans">
   
    <!--  Loading Overlay  -->
    <style>
        .loadingOverlay {
            height: 100vh;
            width: 100vw;
            position: fixed;
            z-index: 99999;
            background: #6997DB;
        }
        #loader {
            position: absolute;
            top: calc(50% - 20px);
            left: calc(50% - 20px);
        }
        #box {
            width: 50px;
            height: 50px;
            background: #fff;
            animation: animate .5s linear infinite;
            position: absolute;
            top: 0;
            left: 0;
            border-radius: 3px;
        }
        @keyframes animate {
            17% {
                border-bottom-right-radius: 3px;
            }
            25% {
                transform: translateY(9px) rotate(22.5deg);
            }
            50% {
                transform: translateY(18px) scale(1,.9) rotate(45deg) ;
                border-bottom-right-radius: 40px;
            }
            75% {
                transform: translateY(9px) rotate(67.5deg);
            }
            100% {
                transform: translateY(0) rotate(90deg);
            }
        } 
        #shadow { 
            width: 50px;
            height: 5px;
            background: #000;
            opacity: 0.1;
            position: absolute;
            top: 59px;
            left: 0;
            border-radius: 50%;
            animation: shadow .5s linear infinite;
        }
        @keyframes shadow {
            50% {
                transform: scale(1.2, 1);
            }
        }
        #loading { 
            position: absolute;
            top: 80px;
            left: -12px;
            color: white;
            font-family: "Open Sans", sans-serif;
            letter-spacing: 0.1rem;
            animation: textAnimation 1.5s ease-in-out infinite;
        }
        @keyframes textAnimation {
            0%, 100% {
                opacity: 1;
            }
            50% {
                opacity: 0.5;
            }
        }
    </style>
    
    <!--  Custom  -->
    <style>
		body {
            user-select: none;
			background: #efefef;
		}
		.alertMsg {
			position: absolute;
			top: 0px;
			z-index: 1035;
			height: 60.8px;
			width: 100vw;
			text-align: center;
			background: #66BB6A;
			color: #FFF;
			font-size: 18px;
			line-height: 60.8px;

			animation: fadeOutTwoSec 500ms ease-out forwards 3500ms;
		}

		.yoloMsg {
			position: relative;
			z-index: 1035;
			width: 100vw;
			text-align: center;
			color: #FFF;
            border: 1.5px solid #0c2461 ;
		}
        .yoloMsg .alertWarning,
        .yoloMsg .alertDanger {
            border: 0;
            font-size: .875rem;
            line-height: 1.5rem;
            padding: 1rem;
        }
        .yoloMsg .alertWarning {
            background: #0c2461  !important;
        }
        .yoloMsg .alertDanger {
            background: #0c2461  !important;
        }
        .yoloMsg .alert-link {
            color: #feca57;
            text-transform: uppercase;
        }
        
		@keyframes fadeOutTwoSec {
			0%{
				top: 0;
			}
			100%{
				top: -65px;
			}
		}
		.mbr-iconfont {
			font-size: 1.3rem !important;
		}
		.profileIconNav {
			height: 35px;
			width: 35px;

			background: url("<?php if ($this->session->userdata('photo') == NULL) : ?><?php echo base_url(); ?>assets/Images/default-profile.jpg<?php endif; ?><?php if ($this->session->userdata('photo') != NULL) : ?>data:image/jpeg;base64,<?php echo base64_encode($this->session->userdata('photo')); ?><?php endif; ?>");

			margin-right: 10px;
			overflow: hidden;
			background-size: cover;
			background-position: center top;
		}

		.dropdown-submenu {
			left: 0 !important;
			transform: translateX(-100%) !important;
		}
		.dropdown-menu .dropdown-toggle[data-toggle="dropdown-submenu"]::after {
			left: 1.1538em !important;
			transform: rotate(180deg) !important;
		}
		@media (max-width: 991px) {
			.dropdown-submenu {
				left: 100% !important;
			}
			.dropdown-menu .dropdown-toggle[data-toggle="dropdown-submenu"]::after {
				right: 1.1538em !important;
				transform: rotate(0deg) !important;
				border-bottom: 0em solid transparent !important;
			}
		}
        
        /*  Card  */
		.wrapper {
			padding-top: 60.1px;
			padding-bottom: 40px;
            color: #212121;
			box-sizing: border-box;
		}
        @media (max-width: 768px){
            .is-card {
                box-shadow: 0px 0px 20px -5px rgba(0, 0, 0, 0) !important;
            }
            .wrapper {
                padding-top: 0px;
                padding-bottom: 0px;
            }
            body {
                background: white;
            }
        }
        @media (min-width: 768px){
            .is-card {
                box-shadow: 0 0 4px 0 rgba(0,0,0,.08), 0 2px 4px 0 rgba(0,0,0,.12) !important;
            }
            .wrapper {
                padding-top: 60.1px;
                padding-bottom: 40px;
            }
            body {
                background: #efefef;
            }
        }
		.is-card {
			min-height: 200px;
			background: white;
			padding: 20px 30px;
            box-shadow: 0 0 4px 0 rgba(0,0,0,.08), 0 2px 4px 0 rgba(0,0,0,.12);
		}
		.is-card p {
            margin: 0;
            padding: 0;
		}
		.is-card a {
            text-decoration: none;
            outline: 0;
		}
        
        .is-card .H1 {
            font-family: "Josefin Slab", "Arvo", "Open Sans", sans-serif;
            font-size: 2.5rem;
            font-weight: bold;
            margin-bottom: 1.25rem;
            color: #424242;
        }
        .is-card .H2 {
            font-family: "Rubik", sans-serif;
            font-size: 1.5rem;
            margin-bottom: 0.5rem;
            font-weight: 500;
        }
        .is-card .H3 {
            font-family: "Open Sans", sans-serif;
            font-size: 1rem;
            margin-bottom: 0.25rem;
        }
        .is-card .text-hint {
            font-size: 0.9rem;
            color: #616161;
            padding: 18px 0;
        }
        
        /*  Projects  */
        .is-card .project {
            background: #F5F5F5;
            padding: 15px 0px;
            border-radius: 3px;
            border: 1.5px solid #E0E0E0;
        }
        .is-card .project .projectTitle {
            font-family: "Josefin Slab", "Arvo", monospace;
            font-weight: bold;
            font-size: 1.5rem;
            color: #424242;
            word-wrap: break-word;
        }
        .is-card .project .projectDesc {
            font-family: "Open Sans", sans-serif;
            font-size: 0.95rem;
            color: #616161;
            word-wrap: break-word;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-box-orient: vertical;
            -webkit-line-clamp: 4;
            line-height: 1.3rem;
            max-height: 5.2rem;
            overflow: hidden;
        }
        .is-card .project .projectSubTitle {
            text-transform: uppercase;
            font-weight: bold;
            color: #616161;
        }
        .is-card .project .projectSubTitle {
            text-transform: uppercase;
            font-weight: 600;
            letter-spacing: 0.1rem;
            color: #616161;
            font-family: "Arvo", monospace;
        }
        @media (min-width: 768px){
            .is-card .project .bid .btn {
                position: absolute;
                top: 50%;
                transform: translateY(-50%);
                right: 20px;
            }
        }
    </style>
    
	<!--  Tabs  -->
	<style>
		/*----- Tabs -----*/
		.tabs {
			width:100%;
			display:inline-block;
		}
		/*----- Tab Links -----*/
		/* Clearfix */
		.tab-links:after {
			display:block;
			clear:both;
			content:'';
		}
		.tab-links {
			padding-left: 0;
			margin-bottom: 15px;
		}
		.tab-links li {
			margin: 0px 25px 0px 0px;
			float:left;
			list-style:none;
			background:#fff;
			border-bottom: 1.5px solid transparent;
		}
		.tab-links i {
			padding-right: 10px;
			font-size: 0.8em;
		}
		.tab-links a {
			padding:8px 2px 8px;
			display:inline-block;
			font-size:16px;
			font-weight:600;
			color:#4c4c4c;
			transition:all linear 0.15s;
		}
		.tab-links a:hover {
			text-decoration:none;
		}
		li.active a, li.active a:hover {
			color:#4c4c4c;
			border-bottom: 1.5px solid #2185d5;
		}
		.tab {
			display:none;
		}
		.tab.active {
			display:block;
		}
        
        .tab .imageCard {
            padding-top: 20px;
        }
        
        .tab .imageCard .card-body {
            padding: 0.8rem;
            background: #2185d5;
        }
        .tab .imageCard .card-body > p {
            font-size: 0.9rem;
            color: #fff;
        }
        
        .tab .imageCard .card-footer {
            padding: 1rem;
            background: white;
            font-size: 0.95rem;
            color: #757575;
        }
        .tab .imageCard .card-footer small {
            font-size: 0.95rem;
            color: #757575;
        }
        
        .tab #portfolioTitle {
            border-bottom: 1px solid #757575;
            display: block;
            width: 100%;
            margin: 30px 15px 0;
            text-align: center;
            font-size: 2rem;
            color: #757575;
            font-weight: 600;
            padding-bottom: 10px;
        }
	</style>
	
    <!--  Buttons  -->
    <style>
        .btn {
			padding: 10px 10px;
			width: 180px;
			max-width: 80%;
            margin: 0;
        }
        .btn-warning {
			background-color: rgb(247, 237, 74);
			color: rgb(63, 60, 3);
			border-color: rgb(247, 237, 74);
		}
		.btn-warning:hover,
		.btn-warning:focus {
			background-color: rgb(234, 221, 10);
			border-color: rgb(234, 221, 10);
			color: rgb(63, 60, 3);
		}
		.btn-danger {
			background-color: #ff3366;
			border-color: #ff3366;
			color: #ffffff;
		}
		.btn-danger:hover,
		.btn-danger:focus {
			background-color: #e50039;
			border-color: #e50039;
		}
		.btn-info {
			background-color: #149dcc;
			border-color: #149dcc;
			color: #ffffff;
		}
		.btn-info:hover,
		.btn-info:focus {
			background-color: #0d6786;
			border-color: #0d6786;
		}
        .btn a {
            text-decoration: none;
            color: white;
        }
        .btn-warning a {
			color: rgb(63, 60, 3);
        }
    </style>

</head>

<body>

<!--  Loading Overlay  -->
<div class="loadingOverlay">
    <div id="loader">
        <div id="shadow"></div>
        <div id="box"></div>
        <div id="loading">Loading...</div>
    </div>
</div>
    
<?php if ($this->session->flashdata('msg')) : ?>
    <div class="alertMsg">
        <?= $this->session->flashdata('msg') ?>
    </div>
<?php endif; ?>

<?php if (validation_errors()) : ?>
    <div class="yoloMsg">
        <div class="alertDanger"><?= validation_errors(); ?></div>
    </div>
<?php endif; ?>

<section class="menu cid-qH62TEfJyl" once="menu" id="menu1-e">
	<nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-toggleable-sm" style="position: relative;">

		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<div class="hamburger">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
		</button>

		<div class="menu-logo">
			<div class="navbar-brand">

                    <span class="navbar-caption-wrap">
                        <a class="navbar-caption text-white display-5" href="<?php echo base_url(); ?>">Bidder's Arena</a>
                    </span>
			</div>
		</div>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">

				<li class="nav-item"><a class="nav-link link text-white display-4" href="#"><span class="mbri-cash mbr-iconfont mbr-iconfont-btn"></span>My Wallet<br></a></li>

				<li class="nav-item"><a class="nav-link link text-white display-4" href="<?php echo base_url();?>myprojects"><span class="mbri-bookmark mbr-iconfont mbr-iconfont-btn"></span>My Projects<br></a></li>

				<li class="nav-item"><a class="nav-link link text-white display-4" href="<?php echo base_url();?>profile"><span class="mbri-user mbr-iconfont mbr-iconfont-btn"></span>My Profile<br></a></li>

				<li class="nav-item dropdown">
					<a class="nav-link link dropdown-toggle text-white display-4" href="#" data-toggle="dropdown-submenu" aria-expanded="false">
						<span class="profileIconNav"></span><?= $this->session->userdata('username') ?><br></a>

					<div class="dropdown-menu">

						<div class="dropdown">
							<a class="dropdown-item text-white dropdown-toggle display-4" data-toggle="dropdown-submenu" aria-expanded="false">Help</a>

							<div class="dropdown-menu dropdown-submenu">
								<a class="dropdown-item text-white display-4" href="#" aria-expanded="false">FAQ</a>

								<a class="dropdown-item text-white display-4" href="#">Contact Us</a>

								<a class="dropdown-item text-white display-4" href="#" aria-expanded="false">Our Charges</a>
							</div>
						</div>

						<a class="dropdown-item text-white display-4" href="<?php echo base_url();?>logout" aria-expanded="false">Logout</a>

					</div>
				</li>
			</ul>
		</div>
	</nav>
</section>

<div class="container wrapper">
	<div class="row">
        <div class="col col-12 is-card">
          
           <div class="tabs">
               
                <ul class="tab-links">
                    <li class="active">
                        <a href="#tab1"><span><i class="fa fa-newspaper-o"></i></span>Dashboard</a>
                    </li>

                    <li class="">
                        <a href="#tab2"><span><i class="fa fa-comments"></i></span>Inbox</a>
                    </li>
                </ul>

                <div class="tab-contents">
                    <div id="tab1" class="tab active">
                        
                        <p class="H2">Want to get something done?</p>
                        
                        <a class="btn btn-danger" href="<?php echo base_url(); ?>post-project">Post Project</a>
                        
                        <div class="row mt-4 mb-4">
                            <div class="col-7 col-md-9 col-lg-9">
                                <p class="H2">Want to work? <span class="text-hint">(Here are some projects according to your skills)</span></p>
                            </div>
                            
                            <div class="col-5 col-md-3 col-lg-3 text-right">
                                <p class="H2"><span class="text-hint pr-2">Bids left:
									</span>
										<?php if (isset($bids_remaining)) : ?>
											<?= $bids_remaining ?>
										<?php endif; ?>
									<span class="text-hint">
										/<?php if (isset($total_bids)) : ?>
											<?= $total_bids ?>
										<?php endif; ?>
									</span>
								</p>
                            </div>
                        </div>
                        
                        <div id="projectSection" class="row projectWrapper pl-3 pr-3" style="max-height: 70vh; overflow-y: scroll;">
                        </div>
                        
                    </div>
                    
                    <div id="tab2" class="tab">
                        tab2
                    </div>
               </div>
            
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/assets/web/assets/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/assets/dropdown/js/script.min.js"></script>

<script src="<?php echo base_url();?>assets/js/tagsinput.js"></script>
<script src="<?php echo base_url();?>assets/js/typeahead.bundle.js"></script>

<script>
    
	$(document).ready(function() {
		$('.tabs .tab-links a').on('click', function(e)  {
			var currentAttrValue = jQuery(this).attr('href');

			// Show/Hide Tabs
			$('.tabs ' + currentAttrValue).fadeIn(400).siblings().hide();

			// Change/remove current tab to active
            $(this).parent('li').addClass('active').siblings().removeClass('active');

			e.preventDefault();
		});
	});

	$(document).ready(function () {
		$.ajax ({
			url: "<?php echo base_url(); ?>dashboard/showProjects",
			method: "post",
			success: function (data) {
				if(data == "No skills") {
					$("body").prepend('<div class="yoloMsg"><div class="alertWarning"><strong>No projects matching your skills found. Try altering your skillset ! </strong><a href="<?php echo base_url();?>profile" class="alert-link">Proceed to profile</a></div></div>');
				} else {
					$("#projectSection").html("");
					$("#projectSection").html(data);
				}
                $(".loadingOverlay #loader").fadeOut(500);
                $(".loadingOverlay").fadeOut(1000);
			}
		});
	});

</script>

</body>

</html>
