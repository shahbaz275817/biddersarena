<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Bidder's Arena | Login</title>

	<!-- Bulma files -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.1/css/bulma.min.css">
	<!-- Fonts -->
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Josefin+Slab,Arvo,Open+Sans">

	<style>
		body {
			overflow: hidden;
		}
        ::-webkit-scrollbar {
            width: 0px;
            background: transparent;
        }
		.navbar-brand a {
			font-family: "Josefin Slab", "Open Sans", sans-serif !important;
			font-size: 1.5em;
			font-weight: bold;
			padding-left: 20px;
			padding-right: 20px;
		}
		form {
			background: white;
			box-shadow: 0px 5px 10px rgba(66, 66, 66, 0.8);
			padding: 20px 10% 25px;
		}
		form .labelLarge {
			font-family: "Arvo";
			font-weight: bold;
			margin-bottom: 20px;
		}
		form input {
			font-family: "Arvo";
			letter-spacing: 1px;
		}
		form .addMarginTop {
			margin-top: 10px;
		}
		form .button {
			min-width: 100%;
		}
		.columns:last-child {
			margin-bottom: 0px;
		}
		.hero-foot .footerCol{
			background: rgba(21, 101, 192, 0.8);
		}
        
        /* For notification */
		.alertMsg {
			position: absolute;
			z-index: 100;
			height: 52px;
            width: auto;
			text-align: center;
			background: #F44336;
			font-size: 16px;
            padding: 0px 20px;
            left: 50%;
            transform: translateX(-50%);
            display: table;
            border-radius: 0px 0px 5px 5px;
            
            -webkit-box-shadow: 0 2px 3px rgba(10,10,10,.1), 0 0 0 1px rgba(10,10,10,.1);
            box-shadow: 0 2px 3px rgba(10,10,10,.1), 0 0 0 1px rgba(10,10,10,.1);
            color: #4a4a4a;

			animation: fadeOutThreeSec 3000ms ease-out forwards 1000ms;
		}
        @media screen and (max-width: 526px) {
            .alertMsg {
                border-radius: 0px;
            }
        }
        .alertMsg span {
			color: #F5F5F5;
            display: table-cell;
            vertical-align: middle;
        }
		@keyframes fadeOutThreeSec {
			0%{
                top: 0px;
			}
			75%{
                top: 0px;
			}
			100%{
                top: -62px;
			}
		}
	</style>
</head>

<body>

<?php if (validation_errors()) : ?>
    <div class="alertMsg">
        <span><?= validation_errors(); ?></span>
    </div>
<?php endif; ?>

<!-- For notification -->
<?php if ($this->session->flashdata('msg')) : ?>
    <div class="alertMsg">
        <?= '<span>'.$this->session->flashdata('msg').'</span>' ?>
    </div>
<?php endif; ?>

<section class="hero is-info is-fullheight">
	<div class="hero-head">
		<nav class="navbar is-transparent">
			<div class="navbar-brand">

				<!-- Can also use img tag for logo -->
				<a class="navbar-item" href="<?php echo base_url();?>">
					Bidder's Arena
				</a>
				<div class="navbar-burger burger" data-target="navbarDropDown">
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>

			<div id="navbarDropDown" class="navbar-menu">
				<div class="navbar-end has-text-centered">
					<a class="navbar-item" href="<?php echo base_url();?>">Home</a>
					<a class="navbar-item" href="<?php echo base_url();?>register">Sign Up</a>
					<a class="navbar-item" href="#">Contact Us</a>
					<a class="navbar-item" href="#">Our Services</a>
				</div>
			</div>
		</nav>
	</div>

	<div class="hero-body">
		<div class="container">

			<div class="columns is-centered">
				<div class="column is-two-thirds-phone is-two-thirds-tablet is-8-desktop is-6-widescreen">
					<form action="<?php echo base_url(); ?>login" method="post">

						<p class="labelLarge has-text-info has-text-centered is-size-2">Login to your account</p>

						<div class="field">
							<label class="label has-text-grey-dark">Email</label>
							<div class="control has-icons-left">
								<input class="input is-info is-medium" type="email" placeholder="Enter email" name="email" required>
								<span class="icon is-small is-left"><i class="fa fa-at"></i></span>
							</div>
							<?php if (isset($error)) : ?>
								<p class="help is-danger"><?= $error; ?></p>
							<?php endif; ?>
						</div>

						<div class="field">
							<label class="label has-text-grey-dark">Password</label>
							<div class="control has-icons-left">
								<input class="input is-info is-medium" type="password" placeholder="Enter password" name="password" required>
								<span class="icon is-small is-left"><i class="fa fa-lock"></i></span>
							</div>
						</div>

						<div class="field">
							<div class="control addMarginTop columns is-variable is-2 has-text-centered">

								<div class="column">
									<button type="submit" name="login" class="button is-medium is-info">Login</button>
								</div>

								<div class="column">
									<div class="columns is-variable is-2 is-mobile">
										<div class="column has-text-grey-dark">
											<a href="<?php echo base_url();?>register" class="button is-medium is-success is-outlined">Register</a>
										</div>
										<div class="column has-text-grey-dark">
											<a href="<?php echo base_url();?>forgotpassword" class="button is-medium is-danger is-outlined">Forgot?</a>
										</div>

									</div>
								</div>
							</div>
						</div>

					</form>
				</div>
			</div>

		</div>
	</div>

	<div class="hero-foot">
		<div class="footerCol columns is-mobile has-text-centered">
			<div class="column">
				<p>
					<strong>© </strong><a href="<?php echo base_url();?>">Bidder's Arena</a>
				</p>
			</div>
			<div class="column">
				<p>
					by <a href="#">ChamberOfDevelopers</a>
				</p>
			</div>
		</div>
	</div>
</section>

<!-- JQuery -->
<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>

<!-- Script to make hamburger menu working -->
<script>
	document.addEventListener('DOMContentLoaded', function () {
		// Get all "navbar-burger" elements
		var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

		// Check if there are any navbar burgers
		if ($navbarBurgers.length > 0) {

			// Add a click event on each of them
			$navbarBurgers.forEach(function ($el) {
				$el.addEventListener('click', function () {

					// Get the target from the "data-target" attribute
					var target = $el.dataset.target;
					var $target = document.getElementById(target);

					// Toggle the class on both the "navbar-burger" and the "navbar-menu"
					$el.classList.toggle('is-active');
					$target.classList.toggle('is-active');
				});
			});
		}

	});
</script>
</body>

</html>
