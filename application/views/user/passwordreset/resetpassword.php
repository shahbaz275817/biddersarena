<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Forgot password</title>

	<!-- Bulma files -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.1/css/bulma.min.css">
	<!-- Fonts -->
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Josefin+Slab,Arvo,Open+Sans">

	<style>
		body {
			overflow: hidden;
		}
		.navbar-brand a {
			font-family: "Josefin Slab", "Open Sans", sans-serif !important;
			font-size: 1.5em;
			font-weight: bold;
			padding-left: 20px;
			padding-right: 20px;
		}
		form {
			background: white;
			box-shadow: 0px 5px 10px rgba(66, 66, 66, 0.8);
			padding: 20px 10% 25px;
		}
		form .labelLarge {
			font-family: "Arvo";
			font-weight: bold;
			margin-bottom: 15px;
		}
		form .labelSmall {
			margin-bottom: 20px;
			font-family: "Open Sans", sans-serif;
		}
		form input {
			font-family: "Arvo";
			letter-spacing: 1px;
			text-align: center;
		}
		form .button {
			min-width: 40%;
			margin-top: 25px;
		}
		.columns:last-child {
			margin-bottom: 0px;
		}
		.hero-foot .footerCol{
			background: rgba(21, 101, 192, 0.8);
		}
	</style>
</head>

<body>
<section class="hero is-info is-fullheight">
	<div class="hero-head">
		<nav class="navbar is-transparent">
			<div class="navbar-brand">

				<!-- Can also use img tag for logo -->
				<a class="navbar-item" href="<?php echo base_url();?>">
					Bidder's Arena
				</a>
				<div class="navbar-burger burger" data-target="navbarDropDown">
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>

			<div id="navbarDropDown" class="navbar-menu">
				<div class="navbar-end has-text-centered">
					<a class="navbar-item" href="#">Home</a>
					<a class="navbar-item" href="#">About Us</a>
					<a class="navbar-item" href="#">Contact Us</a>
					<a class="navbar-item" href="#">Our Services</a>
				</div>
			</div>
		</nav>
	</div>

	<div class="hero-body">
		<div class="container">

			<div class="columns is-centered">
				<div class="column is-10-phone is-10-tablet is-8-desktop is-6-widescreen">
					<form action="<?php echo base_url();?>user/passresetreq" method="post">

						<p class="labelLarge has-text-info has-text-centered is-size-2">Forgot Password</p>

						<p class="labelSmall has-text-grey-dark has-text-centered is-size-6">Enter your registered e-mail of your account. A password reset link will be sent to it. You can reset the password by clicking on it.</p>

						<div class="field">
							<div class="control">
								<input id="email" class="input is-info is-medium" type="email" placeholder="Enter e-mail address" name="email">
							</div>
						</div>
						<div class="field">
							<div class="control columns is-mobile">
								<div class="column"></div>
								<button type="submit" class="button is-medium is-info">Reset Password</button>
								<div class="column"></div>
							</div>
						</div>

					</form>
				</div>
			</div>

		</div>
	</div>

	<div class="hero-foot">
		<div class="footerCol columns is-mobile has-text-centered">
			<div class="column">
				<p>
					<strong>© </strong><a href="<?php echo base_url();?>">Bidder's Arena</a>.
				</p>
			</div>
			<div class="column">
				<p>
					by <a href="#">ChamberOfDevelopers</a>
				</p>
			</div>
		</div>
	</div>
</section>

<!-- JQuery -->
<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>

<!-- Script to make hamburger menu working -->
<script>
	document.addEventListener('DOMContentLoaded', function () {
		// Get all "navbar-burger" elements
		var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

		// Check if there are any navbar burgers
		if ($navbarBurgers.length > 0) {

			// Add a click event on each of them
			$navbarBurgers.forEach(function ($el) {
				$el.addEventListener('click', function () {

					// Get the target from the "data-target" attribute
					var target = $el.dataset.target;
					var $target = document.getElementById(target);

					// Toggle the class on both the "navbar-burger" and the "navbar-menu"
					$el.classList.toggle('is-active');
					$target.classList.toggle('is-active');
				});
			});
		}

	});
</script>
</body>

</html>


</body>
