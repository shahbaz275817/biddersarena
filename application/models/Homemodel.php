<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homemodel extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->library('email');
	}

	// -------------- User Verification Module ------------------ //

	function verifyEmailAddress($verification_code){
		$value =  $this->no_rows($verification_code);
		if($value == 0) {
			return 0;
		} elseif ($value == 1) {
			$sql = "UPDATE password SET email_verified = 1 WHERE email_hash = ?";
			$this->db->query($sql, array($verification_code));
			return 1;
		}
	}

	function no_rows($verification_code){
		$sql = "SELECT count(*) FROM password WHERE email_hash = ?";
		$query = $this->db->query($sql, array($verification_code));
		return $query->num_rows();
	}

	function loadProjects($username, $user_id) {
		$abc = $this->getSkills($username);
		if($abc == NULL) {
			return NULL;
		} else {
			$temp = array();

			$skills = explode(',', $abc[0]->skills);
			$user_skills = array();
			foreach($skills as $skill) {
				array_push($user_skills, $skill);
			}

			$sql = "SELECT count(project_id) FROM projects WHERE posted_by != ?";
			$query = $this->db->query($sql, array($user_id));
			$count = $query->result();
			$a = $count[0];
			foreach ($a as $b) {
				$c = $b;
			}

			$sql = "SELECT * FROM projects WHERE posted_by != ?";
			$query = $this->db->query($sql, array($user_id));
			$result = $query->result();
			$counter = 0;

			for($i = 0; $i < $c; $i++) {
				$skills = explode(',', $result[$i]->skills_required);
				$req_skills = array();
				foreach($skills as $skill) {
					array_push($req_skills, $skill);
				}
				if(count(array_intersect($user_skills, $req_skills)) > 0) {
					array_push($temp, $result[$i]);
					$counter++;
				}
				if($counter == 14) {
					break;
				}
			}
		}
		return $temp;
	}

	function getSkills($username) {
		$sql = "SELECT skills FROM user WHERE username = ?";
		$query = $this->db->query($sql, array($username));
		$result = $query->result();

		return $result;
	}

	function userSubscription($user_id) {
		$sql = "SELECT * FROM user_subscription WHERE user_id = ?";
		$query = $this->db->query($sql, array($user_id));
		$result = $query->result();

		return $result;
	}

	// -------------- End - User Verification Module ------------------ //

}
