<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projectmodel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->library('session');
	}

	public function postProject($user_id, $project_name, $description, $sampleLink, $file1,$file2, $file3, $file4,$file5, $category, $skills_required, $budget_range, $budget, $additional_packs, $deadline) {

		$previous_project_id = $this->get_previous_project_id();
		if($previous_project_id == NULL) {
			$new_project_id = 1;
		} else {
			$new_project_id = ($previous_project_id + 1);
		}

		$result1 = false; $result2 = false; $result3 = false; $result4 = false;

		foreach ($additional_packs as $packs) {
			if($packs == 'Recruiter') {
				$result1 = true;
			} elseif ($packs == 'Urgent') {
				$result2 = true;
			} elseif ($packs == 'Featured') {
				$result3 = true;
			} elseif ($packs == 'NDA') {
				$result4 = true;
			}
		}

		if($result2 == true || $result3 == true || $result4 == true) {
			$approved = 1;
		} else {
			$approved = 0;
		}

		if($result2 == true) {
			$dLine = $deadline;
		} else {
			$dLine = NULL;
		}

		$project_data = array(
			'project_id'   => $new_project_id,
			'title'   => $project_name,
			'description'  => $description,
			'sampleLink' => $sampleLink,
			'photo1'    => $file1,
            'photo2'    => $file2,
            'photo3'    => $file3,
            'photo4'    => $file4,
            'photo5'    => $file5,
			'is_active'  => 1,
			'posted_by'  => $user_id,
			'category'   => $category,
			'is_approved'    => $approved,
			'skills_required' => $skills_required,
			'budgetRange' => $budget_range,
			'budget' => $budget,
			'recruiter' => $result1,
			'urgent' => $result2,
			'deadline' => $dLine,
			'featured' => $result3,
			'nda' => $result4
			//'created_on' => date('Y-m-j H:i:s')
		);

		$query = $this->db->insert('projects', $project_data);
		return $query;
	}



	private function get_previous_project_id() {

		$this->db->select('*');
		$this->db->from('projects');
		$this->db->order_by("project_id","desc");
		return $this->db->get()->row('project_id');

	}

	public function loadProject($project_id) {
		$sql = "SELECT * FROM projects WHERE project_id = ?";
		$query = $this->db->query($sql, array($project_id));
		$result = $query->result();
		return $result[0];
	}

	public function load_detailed_MyProjects($project_id) {
		$sql = "SELECT * FROM projects WHERE project_id = ?";
		$query = $this->db->query($sql, array($project_id));
		$result = $query->result();
		return $result[0];
	}

	public function load_main_myProjects($user_id) {
		$sql = "SELECT project_id, title, description, skills_required, budgetRange FROM projects WHERE posted_by = ? ORDER BY project_id DESC";
		$query = $this->db->query($sql, array($user_id));
		$result = $query->result();

		return $result;
	}

	public function getProjectDetails($project_id) {
		$sql = "SELECT project_id, posted_by FROM projects WHERE project_id = ?";
		$query = $this->db->query($sql, array($project_id));
		$result = $query->result();
		return $result;
	}

	public function check_bids_remaining($user_id) {
		$sql = "SELECT user_id, bids_remaining FROM user_subscription WHERE user_id = ?";
		$query = $this->db->query($sql, array($user_id));
		$result = $query->result();
		return $result;
	}







}
